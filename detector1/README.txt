README for "detector"
----------------------
Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
Date:    19.2.2015
Project: Effective detection of network anomaly using DNS data (Master thesis).

"detector" is tool providing detection of DNS tunneling using various techniques. Program 
supports input data (packets) in pcap file/device capture mode, netflow format (CSV or nfcapd binary format)
and IPFIX format (CSV).

--------- INSTALLATION

To run this program, you need to match following criteria:
- compiler (g++) with "--std=c++0x" option
- installed libraries: 
  - libpcap-dev

  This program dynamically links library "libnfdump" (http://sourceforge.net/projects/libnfdump/files/?source=navbar),
which is in this case distributed in libnfdump.a file. If there is a problem with linking during complilation, please 
install library from specified website.

Authors of libnfdump:
Vaclav Bartos <bartos@censet.cz>
[code based on nfdump by Peter Haag <peter.haag@switch.ch>]

For making main detector, use "make" command. Automatically optimized version (-O3) of detector can be compiled by "make optim".
For making pcap to IPFIX CSV convertor, use "make convertor".

---------- IMPORTANT !!!

  When working with flow statistics files (-t <file>), statistic period time duration specified in config file must match 
period duration with which were statistics data captured (with -s param)!

---------- TESTING

  For testing purposes, you can convert pcap file with attack to IPFIX CSV using convertor (make convertor) and then
merge attack traffic with normal traffic using merger (merge.py). Then, you can use merged traffic as input for
detector.

Example: 
First, convert pcap to IPFIX CSV:
	./pcaptoipfixcsv -i attack.pcap -o attack.csv
Next, merge normal traffic and attack traffic. Specify attack start time (attack traffic is merged to start at specified time).
	python merger.py --ipfix -i normal.csv -m attack.csv -o merged.csv -t 14:25:00
Finally, you can use merged.csv as input for detector...
	./detector -x -i merged.csv ...

  If you want to test NetFlow detector with pcap file, you must convert pcap to NetFlow first. You can convert pcap to NetFlow
using available software-based NetFlow exporters which support pcap input, for example "FlowTraq Flow Exporter" 
(http://www.flowtraq.com/corporate/product/flow-exporter/) or "softflowd" (http://www.mindrot.org/projects/softflowd/). 
These are only exporters ("softflowd" isn't), so you must capture exported NetFlow traffic with "nfcapd".
  After getting nfcapd binary file with captured NetFlow information, you can export it to NetFlow CSV file ("nfdump" style).
Finally, you can use merger to merge nfdump CSV with normal traffic with CSV containing attack traffic.

Example (using "FlowTraq Flow Exporter"):
First, download "FlowTraq Flow Exporter" from http://www.flowtraq.com/corporate/product/flow-exporter/ (needs email registration).
Next, use flow exporter and nfcapd to capture NetFlow data:
	sudo nfcapd -p 2055 -b 127.0.0.1 -l ./
	./flowexport_linux_i386.bin -f attack.pcap
Then, use nfdump to convert binary data to CSV (we need only first 15 columns of CSV):
	nfdump -r nfcapd.201504151550 -o csv | cut -d, -f1-15 > attack.csv
Now you can merge attack traffic with normal traffic (at specified attack start time).
	python merger.py --netflow -i normal.csv -m attack.csv -o merged.csv -t 14:25:00
Finally, use merged.csv as input for detector...
	./detector -n -i merged.csv ...


---------- SPECIFICS

  For working with netflow data (for export to desired CSV format, or capture flow data) use package "nfdump" version 1.6.1X (at least).
You can gather netflow data using "nfcapd -p port -b local_ip -l dir_to_store_files".
  After data has been gathered, you can convert them to CSV using "nfdump -r nfcapdfile -o csv | cut -d, -f1-15 > file.csv".
Notice that we need only first 15 columns from CSV, so you don't have to waste your HDD space and cut it from result.
CSV header must be "ts,te,td,sa,da,sp,dp,pr,flg,fwd,stos,ipkt,ibyt,opkt,obyt".
  You can also use binary files created by nfcapd directly (specified by -d parameter of detector).

  Input IPFIX files are CSV files created by "DNS plugin" for INVEA-TECH's Flowmon Exporter, which you can get from
https://www.liberouter.org/technologies/dns-plugin/
  CSV header must be "DST_IP,SRC_IP,BYTES,TIME_FIRST,TIME_LAST,DNS_RR_TTL,PACKETS,DNS_ANSWERS,DNS_QTYPE,DNS_RLENGTH,DST_PORT,SRC_PORT,DNS_RCODE,DNS_NAME,DNS_RDATA".

---------- PARAMETERS

  Available options for detection are specified in configuration file. If configuration file
is missing, you can generate it by "./detector -m <filename>".
  Configuration file is provided to program by parameter "-c <filename>". If no configuration
file is specified, program uses default detection setting, which can be seen in generated
config file...


Program options:
	-i <input_file> - input file or interface name
	-r <inputs_folder> - folder with input files
	-o <output_file> - output file
	-p - input is pcap file
	-e - capture and analyze live packets on interface specified by -i <interface_name>. Program must run under root (e.g. sudo ./detector ...)!
	-n - input is NetFlow csv file(s) (nfdump -o csv)
	-d - input is NetFlow binary file(s) (captured by nfcapd)
	-x - input is IPFIX csv file(s) (created by DNS-plugin for INVEA-TECH's Flowmon Exporter).
	-t <stats_file> - daily NetFlow/IPFIX statistics file created with -s.
	-s - create daily statistics for NetFlow/IPFIX detector (from input data). Flow type (-n/-d/-x), inputs (-i/-r) and output (-o) must be specified!
	-c <config_file> - configuration file with parameters for detection. If not specified, default configuration is used (see -m parameter).
	-m <config_file> - create new config file with default configuration.
    -w <whitelist_file> - whitelist file with domains which will not be detected as tunnels.
    -l <whitelist_file> - create new whitelist file with default configuration.
	-D <integer> - debug level (prints packet info to stdout). Higher level means more info.
		-D 1 prints information about bins (creation, detection, ...)
		-D 2 prints packet/flow information - IPs, ports, ...
		-D 3 prints packet/flow extended info - DNS queries etc.
		-D 4 prints extended info & bin data just before bin switch
	-h - prints help message
 
---------- TYPICAL USAGE

Firstly, you need configuration file. If configuration file (config.txt) is missing, you can create it with:
	./detector -m config.txt
Next, you should change configuration for your specific environment (based on volume of traffic which flows through your detection point).

For netflow/IPFIX detection are needed typical daily traffic statistics. You can make them from pcap/IPFIX files like this 
(example is with nfcapd binary file):
	./detector -s -n -i dailyTraffic.nfcapd -o statistics.txt -c config.txt
Or from folder with daily flow files:
	./detector -s -n -r traffic_folder -o statistics.txt -c config.txt

Case 1: detect from live traffic captured from interface
	sudo ./detector -e -i eth0 -o detections.txt -c config.txt -w whitelist.txt

Case 2: detect in pcap file
	./detector -p -i attack.pcap -o detections.txt -c config.txt -w whitelist.txt

Case 3: detect in nfdump binary file
	./detector -d -i flows.nfcapd -o detections.txt -c config.txt -t statistics.txt

Case 4: detect in IPFIX CSV file:
	./detector -x -i flows.csv -o detections.txt -c config.txt -t statistics.txt -w whitelist.txt


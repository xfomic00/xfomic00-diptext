/**
 * libnfdump.h
 * This is header file for "libnfdump" (dynamically linked with libnfdump.a).
 *
 * libnfdump can be obtained here:
 * http://sourceforge.net/projects/libnfdump/files/?source=navbar
 *
 * Authors:
 * Vaclav Bartos <bartos@censet.cz>
 * [code based on nfdump by Peter Haag <peter.haag@switch.ch>]
 */

#ifndef _LIBNFDUMP_H_
#define _LIBNFDUMP_H_

//#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-pedantic"

#ifdef __cplusplus
extern "C" {
#endif

// TODO: include only what is really needed

// Incude the same header files as in nfdump.c
//#include "config.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//#ifdef HAVE_STDINT_H
#include <stdint.h>
//#endif

/*********************** Structures copied from nffile.h **********************/

#ifndef _IN_LIBNFDUMP_C_ // this is not needed if included from libnfdump.c

// single IP addr for next hop and bgp next hop
typedef struct nfdump_ip_addr_s {
	union {
		struct {
#ifdef WORDS_BIGENDIAN
			uint32_t	fill[3];
			uint32_t	_v4;
#else
			uint32_t	fill1[2];
			uint32_t	_v4;
			uint32_t	fill2;
#endif
		};
		uint64_t		_v6[2];
	} ip_union;
} nfdump_ip_addr_t;


typedef struct nfdump_record_header_s {
 	// record header
 	uint16_t	type;
 	uint16_t	size;
} nfdump_record_header_t;

typedef struct exporter_info_record_s {
	nfdump_record_header_t	header;

	// exporter version
	uint32_t 	version;
#define SFLOW_VERSION  9999

	// IP address
	nfdump_ip_addr_t	ip;
	uint16_t	sa_family;

	// internal assigned ID
	uint16_t	sysid;

	// exporter ID/Domain ID/Observation Domain ID assigned by the device
	uint32_t	id;

} nfdump_exporter_info_record_t;

typedef struct nfdump_extension_map_s {
 	// record head
 	uint16_t	type;	// is ExtensionMapType
 	uint16_t	size;	// size of full map incl. header

	// map data
#define INIT_ID 0xFFFF
	uint16_t	map_id;			// identifies this map
 	uint16_t	extension_size; // size of all extensions
	uint16_t	ex_id[1];		// extension id array
} nfdump_extension_map_t;

typedef struct nfdump_stat_record_s {
	// overall stat
	uint64_t	numflows;
	uint64_t	numbytes;
	uint64_t	numpackets;
	// flow stat
	uint64_t	numflows_tcp;
	uint64_t	numflows_udp;
	uint64_t	numflows_icmp;
	uint64_t	numflows_other;
	// bytes stat
	uint64_t	numbytes_tcp;
	uint64_t	numbytes_udp;
	uint64_t	numbytes_icmp;
	uint64_t	numbytes_other;
	// packet stat
	uint64_t	numpackets_tcp;
	uint64_t	numpackets_udp;
	uint64_t	numpackets_icmp;
	uint64_t	numpackets_other;
	// time window
	uint32_t	first_seen;
	uint32_t	last_seen;
	uint16_t	msec_first;
	uint16_t	msec_last;
	// other
	uint32_t	sequence_failure;
} nfdump_stat_record_t;

/* the master record contains all possible records unpacked */
typedef struct master_record_s {
	// common information from all netflow versions
	// 							// interpreted as uint64_t[]
	// 							#ifdef WORDS_BIGENDIAN

	uint16_t	type;			// index 0  0xffff 0000 0000 0000
	uint16_t	size;			// index 0	0x0000'ffff'0000 0000
	uint16_t	flags;			// index 0	0x0000'0000'ffff'0000
	uint16_t	ext_map;		// index 0	0x0000'0000'0000'ffff
#	define OffsetRecordFlags 	0
#ifdef WORDS_BIGENDIAN
#	define MaskRecordFlags  	0x00000000ff000000LL
#	define ShiftRecordFlags 	24
#else
#	define MaskRecordFlags  	0x000000ff00000000LL
#	define ShiftRecordFlags 	32
#endif

	//
	uint16_t	msec_first;		// index 1	0xffff'0000'0000'0000
	uint16_t	msec_last;		// index 1	0x0000'ffff'0000'0000

	// 12 bytes offset in master record to first
#define BYTE_OFFSET_first	12

	uint32_t	first;			// index 1	0x0000'0000'ffff'ffff

	//
	uint32_t	last;			// index 2	0xffff'ffff'0000'0000
	uint8_t		fwd_status;		// index 2	0x0000'0000'ff00'0000
	uint8_t		tcp_flags;		// index 2  0x0000'0000'00ff'0000
	uint8_t		prot;			// index 2  0x0000'0000'0000'ff00
	uint8_t		tos;			// index 2  0x0000'0000'0000'00ff
#ifdef WORDS_BIGENDIAN
#	define OffsetStatus 		2
#	define MaskStatus  			0x00000000ff000000LL
#	define ShiftStatus  		24

#	define OffsetFlags 			2
#	define MaskFlags   			0x0000000000ff0000LL
	#define ShiftFlags  		16

#	define OffsetProto 			2
#	define MaskProto   			0x000000000000ff00LL
#	define ShiftProto  			8

#	define OffsetTos			2
#	define MaskTos	   			0x00000000000000ffLL
#	define ShiftTos  			0

#else
#	define OffsetStatus 		2
#	define MaskStatus  			0x000000ff00000000LL
#	define ShiftStatus  		32

#	define OffsetFlags 			2
#	define MaskFlags   			0x0000ff0000000000LL
#	define ShiftFlags  			40

#	define OffsetProto 			2
#	define MaskProto   			0x00ff000000000000LL
#	define ShiftProto  			48

#	define OffsetTos			2
#	define MaskTos	   			0xff00000000000000LL
#	define ShiftTos  			56
#endif

	uint16_t	srcport;		// index 3	0xffff'0000'0000'0000
	uint16_t	dstport;		// index 3  0x0000'ffff'0000'0000
	uint16_t	exporter_sysid; // index 3	0x0000'0000'ffff'0000

	union {
		struct {
#ifdef WORDS_BIGENDIAN
			uint8_t		icmp_type;	// index 3  0x0000'0000'0000'ff00
			uint8_t		icmp_code;	// index 3  0x0000'0000'0000'00ff
#else
			// little endian confusion ...
			uint8_t		icmp_code;
			uint8_t		icmp_type;
#endif
		};
		uint16_t icmp;
	};

#ifdef WORDS_BIGENDIAN
#	define OffsetPort 			3
#	define MaskSrcPort			0xffff000000000000LL
#	define ShiftSrcPort			48

#	define MaskDstPort			0x0000ffff00000000LL
#	define ShiftDstPort 		32

#	define MaskExporterSysID  	0x00000000ffff0000LL
#	define ShiftExporterSysID 	16

#	define MaskICMPtype			0x000000000000ff00LL
#	define ShiftICMPtype 		8
#	define MaskICMPcode			0x00000000000000ffLL
#	define ShiftICMPcode 		0

#else
#	define OffsetPort 			3
#	define OffsetExporterSysID	3
#	define MaskSrcPort			0x000000000000ffffLL
#	define ShiftSrcPort			0

#	define MaskDstPort			0x00000000ffff0000LL
#	define ShiftDstPort 		16

#	define MaskExporterSysID  	0x0000ffff00000000LL
#	define ShiftExporterSysID 	32

#	define MaskICMPtype			0xff00000000000000LL
#	define ShiftICMPtype 		56
#	define MaskICMPcode			0x00ff000000000000LL
#	define ShiftICMPcode 		48
#endif

	// extension 4 / 5
	uint32_t	input;			// index 4	0xffff'ffff'0000'0000
	uint32_t	output;			// index 4	0x0000'0000'ffff'ffff
#ifdef WORDS_BIGENDIAN
#	define OffsetInOut     		4
#	define MaskInput       		0xffffffff00000000LL
#	define ShiftInput      		32
#	define MaskOutput      		0x00000000ffffffffLL
#	define ShiftOutput     		0

#else
#	define OffsetInOut     		4
#	define MaskInput      		0x00000000ffffffffLL
#	define ShiftInput      		0
#	define MaskOutput       	0xffffffff00000000LL
#	define ShiftOutput     		32
#endif

	// extension 6 / 7
	uint32_t	srcas;			// index 5	0xffff'ffff'0000'0000
	uint32_t	dstas;			// index 5	0x0000'0000'ffff'ffff
#ifdef WORDS_BIGENDIAN
#	define OffsetAS 			5
#	define MaskSrcAS 			0xffffffff00000000LL
#	define ShiftSrcAS 			32
#	define MaskDstAS 			0x00000000ffffffffLL
#	define ShiftDstAS 			0

#else
#	define OffsetAS 			5
#	define MaskSrcAS 			0x00000000ffffffffLL
#	define ShiftSrcAS 			0
#	define MaskDstAS 			0xffffffff00000000LL
#	define ShiftDstAS 			32
#endif


	// IP address block
	union {
		struct _ipv4_s {
#ifdef WORDS_BIGENDIAN
			uint32_t	fill1[3];	// <empty>		index 6	0xffff'ffff'ffff'ffff
									// <empty>		index 7 0xffff'ffff'0000'0000
			uint32_t	srcaddr;	// srcaddr      index 7 0x0000'0000'ffff'ffff
			uint32_t	fill2[3];	// <empty>		index 8	0xffff'ffff'ffff'ffff
									// <empty>		index 9	0xffff'ffff'0000'0000
			uint32_t	dstaddr;	// dstaddr      index 9 0x0000'0000'ffff'ffff
#else
			uint32_t	fill1[2];	// <empty>		index 6	0xffff'ffff'ffff'ffff
			uint32_t	srcaddr;	// srcaddr      index 7 0xffff'ffff'0000'0000
			uint32_t	fill2;		// <empty>		index 7 0x0000'0000'ffff'ffff
			uint32_t	fill3[2];	// <empty>		index 8 0xffff'ffff'ffff'ffff
			uint32_t	dstaddr;	// dstaddr      index 9 0xffff'ffff'0000'0000
			uint32_t	fill4;		// <empty>		index 9 0xffff'ffff'0000'0000
#endif
		} _v4;
		struct _ipv6_s {
			uint64_t	srcaddr[2];	// srcaddr[0-1] index 6 0xffff'ffff'ffff'ffff
									// srcaddr[2-3] index 7 0xffff'ffff'ffff'ffff
			uint64_t	dstaddr[2];	// dstaddr[0-1] index 8 0xffff'ffff'ffff'ffff
									// dstaddr[2-3] index 9 0xffff'ffff'ffff'ffff
		} _v6;
		struct _ip64_s {
			uint64_t	addr[4];
		} _ip_64;
	} ip_union;

#ifdef WORDS_BIGENDIAN
#	define OffsetSrcIPv4 		7
#	define MaskSrcIPv4  		0x00000000ffffffffLL
#	define ShiftSrcIPv4 		0

#	define OffsetDstIPv4 		9
#	define MaskDstIPv4  		0x00000000ffffffffLL
#	define ShiftDstIPv4  		0

#	define OffsetSrcIPv6a 		6
#	define OffsetSrcIPv6b 		7
#	define OffsetDstIPv6a 		8
#	define OffsetDstIPv6b 		9
#	define MaskIPv6  			0xffffffffffffffffLL
#	define ShiftIPv6 			0

#else
#	define OffsetSrcIPv4 		6
#	define MaskSrcIPv4  		0xffffffff00000000LL
#	define ShiftSrcIPv4 		32

#	define OffsetDstIPv4 		8
#	define MaskDstIPv4  		0xffffffff00000000LL
#	define ShiftDstIPv4  		32

#	define OffsetSrcIPv6a 		6
#	define OffsetSrcIPv6b 		7
#	define OffsetDstIPv6a 		8
#	define OffsetDstIPv6b 		9
#	define MaskIPv6  			0xffffffffffffffffLL
#	define ShiftIPv6 			0
#endif


	// counter block - expanded to 8 bytes
	uint64_t	dPkts;			// index 10	0xffff'ffff'ffff'ffff
#	define OffsetPackets 		10
#	define MaskPackets  		0xffffffffffffffffLL
#	define ShiftPackets 		0

	uint64_t	dOctets;		// index 11 0xffff'ffff'ffff'ffff
#	define OffsetBytes 			11
#	define MaskBytes  			0xffffffffffffffffLL
#	define ShiftBytes 			0

	// extension 9 / 10
	nfdump_ip_addr_t	ip_nexthop;		// ipv4   index 13 0x0000'0000'ffff'ffff
								// ipv6	  index 12 0xffff'ffff'ffff'ffff
								// ipv6	  index 13 0xffff'ffff'ffff'ffff

#ifdef WORDS_BIGENDIAN
#	define OffsetNexthopv4 		13
#	define MaskNexthopv4  		0x00000000ffffffffLL
#	define ShiftNexthopv4 		0

#	define OffsetNexthopv6a		12
#	define OffsetNexthopv6b		13
// MaskIPv6 and ShiftIPv6 already defined

#else
#	define OffsetNexthopv4 		13
#	define MaskNexthopv4  		0xffffffff00000000LL
#	define ShiftNexthopv4 		0

#	define OffsetNexthopv6a		12
#	define OffsetNexthopv6b		13
#endif

	// extension 11 / 12
	nfdump_ip_addr_t	bgp_nexthop;	// ipv4   index 15 0x0000'0000'ffff'ffff
								// ipv6	  index 14 0xffff'ffff'ffff'ffff
								// ipv6	  index 15 0xffff'ffff'ffff'ffff

#ifdef WORDS_BIGENDIAN
#	define OffsetBGPNexthopv4 	15
#	define MaskBGPNexthopv4  	0x00000000ffffffffLL
#	define ShiftBGPNexthopv4 	0

#	define OffsetBGPNexthopv6a	14
#	define OffsetBGPNexthopv6b	15
// MaskIPv6 and ShiftIPv6 already defined

#else
#	define OffsetBGPNexthopv4 	15
#	define MaskBGPNexthopv4  	0xffffffff00000000LL
#	define ShiftBGPNexthopv4 	0

#	define OffsetBGPNexthopv6a	14
#	define OffsetBGPNexthopv6b	15
#endif

	// extension 8
	union {
		struct {
			uint8_t	dst_tos;	// index 16 0xff00'0000'0000'0000
			uint8_t	dir;		// index 16 0x00ff'0000'0000'0000
			uint8_t	src_mask;	// index 16 0x0000'ff00'0000'0000
			uint8_t	dst_mask;	// index 16 0x0000'00ff'0000'0000
		};
		uint32_t	any;
	};

	// extension 13
	uint16_t	src_vlan;		// index 16 0x0000'0000'ffff'0000
	uint16_t	dst_vlan;		// index 16 0x0000'0000'0000'ffff

#ifdef WORDS_BIGENDIAN
#	define OffsetDstTos			16
#	define MaskDstTos			0xff00000000000000LL
#	define ShiftDstTos  		56

#	define OffsetDir			16
#	define MaskDir				0x00ff000000000000LL
#	define ShiftDir  			48

#	define OffsetMask			16
#	define MaskSrcMask			0x0000ff0000000000LL
#	define ShiftSrcMask  		40

#	define MaskDstMask			0x000000ff00000000LL
#	define ShiftDstMask 		32

#	define OffsetVlan 			16
#	define MaskSrcVlan  		0x00000000ffff0000LL
#	define ShiftSrcVlan 		16

#	define MaskDstVlan  		0x000000000000ffffLL
#	define ShiftDstVlan 		0

#else
#	define OffsetDstTos			16
#	define MaskDstTos			0x00000000000000ffLL
#	define ShiftDstTos  		0

#	define OffsetDir			16
#	define MaskDir				0x000000000000ff00LL
#	define ShiftDir  			8

#	define OffsetMask			16
#	define MaskSrcMask			0x0000000000ff0000LL
#	define ShiftSrcMask  		16

#	define MaskDstMask			0x00000000ff000000LL
#	define ShiftDstMask 		24

#	define OffsetVlan 			16
#	define MaskSrcVlan  		0x0000ffff00000000LL
#	define ShiftSrcVlan 		32

#	define MaskDstVlan  		0xffff000000000000LL
#	define ShiftDstVlan 		48

#endif

	// extension 14 / 15
	uint64_t	out_pkts;		// index 17	0xffff'ffff'ffff'ffff
#	define OffsetOutPackets 	17
// MaskPackets and ShiftPackets already defined

	// extension 16 / 17
	uint64_t	out_bytes;		// index 18 0xffff'ffff'ffff'ffff
#	define OffsetOutBytes 		18

	// extension 18 / 19
	uint64_t	aggr_flows;		// index 19 0xffff'ffff'ffff'ffff
#	define OffsetAggrFlows 		19
#	define MaskFlows 	 		0xffffffffffffffffLL

	// extension 20
	uint64_t	in_src_mac;		// index 20 0xffff'ffff'ffff'ffff
#	define OffsetInSrcMAC 		20
#	define MaskMac 	 			0xffffffffffffffffLL

	// extension 20
	uint64_t	out_dst_mac;	// index 21 0xffff'ffff'ffff'ffff
#	define OffsetOutDstMAC 		21

	// extension 21
	uint64_t	in_dst_mac;		// index 22 0xffff'ffff'ffff'ffff
#	define OffsetInDstMAC 		22

	// extension 21
	uint64_t	out_src_mac;	// index 23 0xffff'ffff'ffff'ffff
#	define OffsetOutSrcMAC 		23

	// extension 22
	uint32_t	mpls_label[10];
#	define OffsetMPLS12 		24
#	define OffsetMPLS34 		25
#	define OffsetMPLS56 		26
#	define OffsetMPLS78 		27
#	define OffsetMPLS910 		28

#ifdef WORDS_BIGENDIAN
#	define MaskMPLSlabelOdd  	0x00fffff000000000LL
#	define ShiftMPLSlabelOdd 	36
#	define MaskMPLSexpOdd  		0x0000000e00000000LL
#	define ShiftMPLSexpOdd 		33

#	define MaskMPLSlabelEven  	0x0000000000fffff0LL
#	define ShiftMPLSlabelEven 	4
#	define MaskMPLSexpEven  	0x000000000000000eLL
#	define ShiftMPLSexpEven 	1
#else
#	define MaskMPLSlabelOdd 	0x000000000000fff0LL
#	define ShiftMPLSlabelOdd 	4
#	define MaskMPLSexpOdd  		0x000000000000000eLL
#	define ShiftMPLSexpOdd 		1

#	define MaskMPLSlabelEven 	0x00fffff000000000LL
#	define ShiftMPLSlabelEven 	36
#	define MaskMPLSexpEven 		0x0000000e00000000LL
#	define ShiftMPLSexpEven		33

#endif

	// extension 23 / 24
	nfdump_ip_addr_t	ip_router;		// ipv4   index 30 0x0000'0000'ffff'ffff
								// ipv6	  index 29 0xffff'ffff'ffff'ffff
								// ipv6	  index 30 0xffff'ffff'ffff'ffff

#ifdef WORDS_BIGENDIAN
#	define OffsetRouterv4 		30
#	define MaskRouterv4  		0x00000000ffffffffLL
#	define ShiftRouterv4 		0

#	define OffsetRouterv6a		29
#	define OffsetRouterv6b		30
// MaskIPv6 and ShiftIPv6 already defined

#else
#	define OffsetRouterv4 		30
#	define MaskRouterv4  		0xffffffff00000000LL
#	define ShiftRouterv4 		0

#	define OffsetRouterv6a		29
#	define OffsetRouterv6b		30
#endif

	// extension 25
	uint16_t	fill;			// fill	index 31 0xffff'0000'0000'0000
	uint8_t		engine_type;	// type index 31 0x0000'ff00'0000'0000
	uint8_t		engine_id;		// ID	index 31 0x0000'00ff'0000'0000
	uint32_t	fill2;

#	define OffsetRouterID	31
#ifdef WORDS_BIGENDIAN
#	define MaskEngineType		0x0000FF0000000000LL
#	define ShiftEngineType		40
#	define MaskEngineID			0x000000FF00000000LL
#	define ShiftEngineID		32

#else
#	define MaskEngineType		0x0000000000FF0000LL
#	define ShiftEngineType		16
#	define MaskEngineID			0x00000000FF000000LL
#	define ShiftEngineID		24
#endif

	// IPFIX extensions in v9
	// BGP next/prev AS
	uint32_t	bgpNextAdjacentAS;	// index 32 0xffff'ffff'0000'0000
	uint32_t	bgpPrevAdjacentAS;	// index 32 0x0000'0000'ffff'ffff

// extension 18
#	define OffsetBGPadj	32
#ifdef WORDS_BIGENDIAN
#	define MaskBGPadjNext		0xFFFFFFFF00000000LL
#	define ShiftBGPadjNext		32
#	define MaskBGPadjPrev		0x00000000FFFFFFFFLL
#	define ShiftBGPadjPrev		0

#else
#	define MaskBGPadjNext		0x00000000FFFFFFFFLL
#	define ShiftBGPadjNext		0
#	define MaskBGPadjPrev		0xFFFFFFFF00000000LL
#	define ShiftBGPadjPrev		32
#endif

	// NSEL extensions
#ifdef NSEL
#define NSEL_BASE_OFFSET     (offsetof(master_record_t, conn_id) >> 3)

	// common block
#   define OffsetConnID  NSEL_BASE_OFFSET
#   define OffsetNATevent  NSEL_BASE_OFFSET
	uint32_t	conn_id;			// index OffsetConnID    0xffff'ffff'0000'0000
	uint8_t		event;				// index OffsetConnID    0x0000'0000'ff00'0000
#define FW_EVENT 1
#define NAT_EVENT 2
	uint8_t		event_flag;			// index OffsetConnID    0x0000'0000'00ff'0000
	uint16_t	fw_xevent;			// index OffsetConnID    0x0000'0000'0000'ffff
	uint64_t	event_time;			// index OffsetConnID +1 0x1111'1111'1111'1111
#ifdef WORDS_BIGENDIAN
#	define MaskConnID		0xFFFFFFFF00000000LL
#	define ShiftConnID		32
#	define MaskFWevent		0x00000000FF000000LL
#	define ShiftFWevent		24
#	define MasNATevent		0x00000000FF000000LL
#	define ShiftNATevent	24
#	define MaskFWXevent		0x000000000000FFFFLL
#	define ShiftFWXevent	0
#else
#	define MaskConnID		0x00000000FFFFFFFFLL
#	define ShiftConnID		0
#	define MaskFWevent		0x000000FF00000000LL
#	define ShiftFWevent		32
#	define MasNATevent		0x000000FF00000000LL
#	define ShiftNATevent	32
#	define MaskFWXevent		0xFFFF000000000000LL
#	define ShiftFWXevent	48

#endif

	// xlate ip/port
#   define OffsetXLATEPort NSEL_BASE_OFFSET+2
	uint16_t	xlate_src_port;		// index OffsetXLATEPort 0xffff'0000'0000'0000
	uint16_t	xlate_dst_port;		// index OffsetXLATEPort 0x0000'ffff'0000'0000
	uint32_t	xlate_flags;
#   define OffsetXLATESRCIP NSEL_BASE_OFFSET+3
	nfdump_ip_addr_t	xlate_src_ip;		// ipv4  OffsetXLATESRCIP +1 0x0000'0000'ffff'ffff
									// ipv6	 OffsetXLATESRCIP 	 0xffff'ffff'ffff'ffff
									// ipv6	 OffsetXLATESRCIP	 0xffff'ffff'ffff'ffff

	nfdump_ip_addr_t	xlate_dst_ip;		// ipv4  OffsetXLATEDSTIP +1 0x0000'0000'ffff'ffff
									// ipv6	 OffsetXLATEDSTIP 	 0xffff'ffff'ffff'ffff
									// ipv6	 OffsetXLATEDSTIP 	 0xffff'ffff'ffff'ffff
#ifdef WORDS_BIGENDIAN
#	define MaskXLATESRCPORT	 0xFFFF000000000000LL
#	define ShiftXLATESRCPORT 48
#	define MaskXLATEDSTPORT	 0x0000FFFF00000000LL
#	define ShiftXLATEDSTPORT 32

#	define OffsetXLATESRCv4	 OffsetXLATESRCIP+1
#	define MaskXLATEIPv4  	 0x00000000fFFFFFFFLL
#	define ShiftXLATEIPv4 	 0

#	define OffsetXLATESRCv6a OffsetXLATESRCIP
#	define OffsetXLATESRCv6b OffsetXLATESRCIP+1

#	define OffsetXLATEDSTv6a OffsetXLATESRCIP+2
#	define OffsetXLATEDSTv6b OffsetXLATESRCIP+3

#else
#	define MaskXLATESRCPORT	 0x000000000000FFFFLL
#	define ShiftXLATESRCPORT 0
#	define MaskXLATEDSTPORT	 0x00000000FFFF0000LL
#	define ShiftXLATEDSTPORT 16

#	define OffsetXLATESRCv4	 OffsetXLATESRCIP+1
#	define MaskXLATEIPv4  	 0xFFFFFFFF00000000LL
#	define ShiftXLATEIPv4 	 32

#	define OffsetXLATESRCv6a OffsetXLATESRCIP
#	define OffsetXLATESRCv6b OffsetXLATESRCIP+1

#	define OffsetXLATEDSTv6a OffsetXLATESRCIP+2
#	define OffsetXLATEDSTv6b OffsetXLATESRCIP+3

#endif


	// ingress/egress ACL id
#   define OffsetIngressAclId NSEL_BASE_OFFSET+7
#	define OffsetIngressAceId NSEL_BASE_OFFSET+7
#	define OffsetIngressGrpId NSEL_BASE_OFFSET+8
#	define OffsetEgressAclId  NSEL_BASE_OFFSET+8
#	define OffsetEgressAceId  NSEL_BASE_OFFSET+9
#	define OffsetEgressGrpId  NSEL_BASE_OFFSET+9
	uint32_t ingress_acl_id[3];	// index OffsetIngressAclId   0xffff'ffff'0000'0000
								// index OffsetIngressAceId   0x0000'0000'ffff'ffff
								// index OffsetIngressGrpId   0xffff'ffff'0000'0000
	uint32_t egress_acl_id[3];	// index OffsetEgressAclId	  0x0000'0000'ffff'ffff
								// index OffsetEgressAceId	  0xffff'ffff'0000'0000
								// index OffsetEgressGrpId	  0x0000'0000'ffff'ffff
#ifdef WORDS_BIGENDIAN
#define MaskIngressAclId	0xffffffff00000000LL
#define ShiftIngressAclId	32
#define MaskIngressAceId	0x00000000ffffffffLL
#define ShiftIngressAceId	0
#define MaskIngressGrpId	0xffffffff00000000LL
#define ShiftIngressGrpId	32
#define MaskEgressAclId		0x00000000ffffffffLL
#define ShiftEgressAclId	0
#define MaskEgressAceId		0xffffffff00000000LL
#define ShiftEgressAceId	32
#define MaskEgressGrpId		0x00000000ffffffffLL
#define ShiftEgressGrpId	0
#else
#define MaskIngressAclId	0x00000000ffffffffLL
#define ShiftIngressAclId	0
#define MaskIngressAceId	0xffffffff00000000LL
#define ShiftIngressAceId	32
#define MaskIngressGrpId	0x00000000ffffffffLL
#define ShiftIngressGrpId	0
#define MaskEgressAclId		0xffffffff00000000LL
#define ShiftEgressAclId	32
#define MaskEgressAceId		0x00000000ffffffffLL
#define ShiftEgressAceId	0
#define MaskEgressGrpId		0xffffffff00000000LL
#define ShiftEgressGrpId	32
#endif

	// username
#	define OffsetUsername  NSEL_BASE_OFFSET+10
	char username[72];

	// NAT extensions
	// NAT event is mapped into ASA event
#define NAT_BASE_OFFSET     (offsetof(master_record_t, ingress_vrfid) >> 3)
	// common block
#   define OffsetNELcommon  NEL_BASE_OFFSET
#   define OffsetIVRFID  	NAT_BASE_OFFSET
#   define OffsetEVRFID  	NAT_BASE_OFFSET
#   define OffsetPortBlock	NAT_BASE_OFFSET+1
	uint32_t	ingress_vrfid;	// OffsetIVRFID	   0xffff'ffff'0000'0000
	uint32_t	egress_vrfid;	// OffsetEVRFID	   0x0000'0000'ffff'ffff

	// Port block allocation
	uint16_t	block_start;	// OffsetPortBlock 0xffff'0000'0000'0000
	uint16_t	block_end;		// OffsetPortBlock 0x0000'ffff'0000'0000
	uint16_t	block_step;		// OffsetPortBlock 0x0000'0000'ffff'0000
	uint16_t	block_size;		// OffsetPortBlock 0x0000'0000'0000'ffff

#ifdef WORDS_BIGENDIAN
#	define MaskIVRFID			0xFFFFFFFF00000000LL
#	define ShiftIVRFID			32
#	define MaskEVRFID			0x00000000FFFFFFFFLL
#	define ShiftEVRFID			0
#	define MaskPortBlockStart	0xFFFF000000000000LL
#	define ShiftPortBlockStart	48
#	define MaskPortBlockEnd		0x0000FFFF00000000LL
#	define ShiftPortBlockEnd	32
#	define MaskPortBlockStep	0x00000000FFFF0000LL
#	define ShiftPortBlockStep	16
#	define MaskPortBlockSize	0x000000000000FFFFLL
#	define ShiftPortBlockSize	0
#else
#	define MaskIVRFID			0x00000000FFFFFFFFLL
#	define ShiftIVRFID			0
#	define MaskEVRFID			0xFFFFFFFF00000000LL
#	define ShiftEVRFID			32
#	define MaskPortBlockStart	0x000000000000FFFFLL
#	define ShiftPortBlockStart	0
#	define MaskPortBlockEnd		0x00000000FFFF0000LL
#	define ShiftPortBlockEnd	16
#	define MaskPortBlockStep	0x0000FFFF00000000LL
#	define ShiftPortBlockStep	32
#	define MaskPortBlockSize	0xFFFF000000000000LL
#	define ShiftPortBlockSize	48
#endif

#endif

	// nprobe extensions
	// latency extension
	uint64_t	client_nw_delay_usec;	// index LATENCY_BASE_OFFSET 0xffff'ffff'ffff'ffff
	uint64_t	server_nw_delay_usec;	// index LATENCY_BASE_OFFSET + 1 0xffff'ffff'ffff'ffff
	uint64_t	appl_latency_usec;		// index LATENCY_BASE_OFFSET + 2 0xffff'ffff'ffff'ffff

#define LATENCY_BASE_OFFSET     (offsetof(master_record_t, client_nw_delay_usec) >> 3)
#   define OffsetClientLatency  LATENCY_BASE_OFFSET
#   define OffsetServerLatency  LATENCY_BASE_OFFSET + 1
#   define OffsetAppLatency     LATENCY_BASE_OFFSET + 2
#   define MaskLatency          0xFFFFFFFFFFFFFFFFLL
#   define ShiftLatency         0

	// flow received time in ms
	uint64_t	received;

/* possible user extensions may fit here
 * - Put each extension into its own #ifdef
 * - Define the base offset for the user extension as reference to the first object
 * - Refer to this base offset for each of the values in the master record for the extension
 * - make sure the extension is 64bit aligned
 * - The user extension must be independant of the number of user extensions already defined
 * - the extension map must be updated accordingly
 */

#ifdef USER_EXTENSION_1
	uint64_t	u64_1;
#	define Offset_BASE_U1	offsetof(master_record_t, u64_1)
#	define OffsetUser1_u64	Offset_BASE_U1

	uint32_t	u32_1;
	uint32_t	u32_2;
#	define OffsetUser1_u32_1	Offset_BASE_U1 + 8
#	define MaskUser1_u32_1 		0xffffffff00000000LL
#	define MaskUser1_u32_2 		0x00000000ffffffffLL

#endif

	// reference to exporter
	nfdump_exporter_info_record_t	*exp_ref;

	// last entry in master record
#	define Offset_MR_LAST	offsetof(master_record_t, map_ref)
	nfdump_extension_map_t	*map_ref;
} master_record_t;

#endif //_IN_LIBNFDUMP_C_

/*****************************************************************************/

// Error codes
#define NFDUMP_E_OK 0
#define NFDUMP_E_BAD_PARAMS 1
#define NFDUMP_E_INVALID_FILTER 2
#define NFDUMP_E_INIT_FAILED 3
#define NFDUMP_E_EMPTY_FILE_LIST 4
#define NFDUMP_E_CORRUPTED_FILE 5
#define NFDUMP_E_CANT_OPEN_FILE 6
#define NFDUMP_E_OTHER 254
#define NFDUMP_E_MEMORY 255
#define NFDUMP_EOF -1

/********** Common API *************/

/** Get statistics from a file.
 * Get statistics from a header of given file and store it into stats structure.
 * \param[in] filename Name of nfdump file.
 * \param[in] stats Stracture which should be filled with data from the file.
 * \return Error code, 0 on success.   
 */
// Not implemented yet
//int nfdump_get_stats(char *filename, stat_record_t *stats);

/** \brief Return a string with details about the last error.
 */
const char *nfdump_get_last_error(void);

/** \brief Limit flow processing on flows within a given time window.
 * Flows with end timestamp lower than twin_start or start timestamp greater
 * than twin_end will be skipped during any flow processing after call of this
 * function. To disble this filtering, set twin_start to zero.
 * \param[in] twin_start Start time of the window (UNIX timestamp).
 * \param[in] twin_start End time of the window (UNIX timestamp).
 * \return Always 0.
 */   
int nfdump_set_time_window(time_t twin_start, time_t twin_end);

/** \brief Limit flow processing on flows within a given time window.
 * The same as nfdump_set_time_window, but the time window is given as a text.
 * \param[in] time_win_spec Time window in the same format as the '-t' option of
 *          nfdump uses (e.g. "YYYY/MM/dd.hh:mm:ss-YYYY/MM/dd.hh:mm:ss",
 *          but there are other possibilities, see "man nfdump").
 *          \note Relative time specification (e.g. "-10") is not supported yet. 
 * \return Zero on success or NFDUMP_E_BAD_PARAMS when invalid string is passed.
 */
int nfdump_set_time_window_str(const char *time_win_spec);


/********** Simple iterator API ***********/

#ifndef _IN_LIBNFDUMP_C_ // libnfdump.c uses a little different definition

/** \brief Nfdump file iterator.
 * Structure used as a handle for iterating over records in a file
 * or a sequence of files.
 */
typedef struct {
   void/*common_record_t*/ *flow_record; // Pointer to current record
   void/*extension_map_list_t*/ *extension_map_list;
   void/*FilterEngine_data_t*/ *filter_data;
   void/*nffile_t*/ *nffile_r;
   #ifdef COMPAT15
   int v1_map_done;
   #endif
   time_t twin_start;
   time_t twin_end;
   unsigned char opened;
   unsigned int  remaining_records;
   unsigned int  file_errors;     // Number of files skipped due to read error or file corruption
   unsigned int  unknown_blocks;  // Number of blocks skipped due to unknown type
   unsigned int  unknown_records; // Number of records skipped due to unknown type
} nfdump_iter_t; 

#endif //_IN_LIBNFDUMP_C_

/** \brief Begin iteration over flows in given file.
   Open given nfdump file and prepare to read flows. Floww are then read one by
   one by calling nfdump_iter_next(). After the reading from file is completed,
   nfdump_iter_end() should be called.
   If time range was set by nfdump_set_time_window or nfdump_set_time_window_str,
   only flow records within this time window will be iterated on.
   \param[in,out] iter Iterator object (caller allocates it, function fills it).
   \param[in] filename Path to a file to read. If NULL, read from stdin.
   \param[in] filter NfDump filter to use. Only flow records matching the filter
                     will be iterated on.
   \return Error code, 0 on success.
*/
int nfdump_iter_start(nfdump_iter_t* iter, const char *filename, const char *filter);

/** \brief Begin iteration over flows in given sequence of files.
 * Open given nfdump files and prepare to read flows. Flow are then read one by
 * one by calling nfdump_iter_next(). After reading from the files is completed,
 * nfdump_iter_end() should be called.
 * If time range was set by nfdump_set_time_window or nfdump_set_time_window_str,
 * only flow records within this time window will be iterated on.
 * 
 * The file sequence is given by the same way as by nfdump options -M, -r and 
 * -R. See nfdump documentation for details.
 * If Mdirs is specified either rfile or Rfile must be specified as well.
 * Exactly one of rfile and Rfile must be specified.
 * 
 * \param[in,out] iter Iterator object (caller allocates it, function fills it).
 * \param[in] Mdirs The same meaning as -M option of nfdump, set to NULL to not use it.
 * \param[in] rfile The same meaning as -r option of nfdump, set to NULL to not use it.
 * \param[in] Rfiles The same meaning as -R option of nfdump, set to NULL to not use it.
 * \param[in] filter NfDump filter to use. Only flow records matching the filter
 *                   will be iterated on.
 * \return Error code, 0 on success.
 */
int nfdump_iter_start_multi(nfdump_iter_t* iter, const char *Mdirs, 
                            const char *rfile, const char* Rfiles,
                            const char *filter);

/** \brief Read next flow record.
 * Read next flow record from intput file(s) and return pointer to it.
 * If there are no more records, NFDUMP_EOF is returned.
 * \param[in,out] iter Iterator object.
 * \param[out] master_record Pointer to flow record as master_record_t structure.
 * \return 0 when record is successfully read, NFDUMP_EOF if end of file 
 *         sequence is reached, or error code when error is encoutered.
 */
int nfdump_iter_next(nfdump_iter_t *iter, master_record_t **master_record);

/** \breif Finish iteration.
 * Close opened file and free buffers. This function should be called when
 * NFDUMP_EOF is returned from nfdump_iter_next or if iteration should be
 * stopped earlier.
 * \param[in,out] iter Iterator object.
 * \reutrn Always 0.
 */
int nfdump_iter_end(nfdump_iter_t *iter);

/********** Callback API ***********/

/** \brief Callback function type.
 * Functions used in nfdump_add_callback must of this type.
 */
typedef void (*callback_func_t)(const master_record_t *, void *);

/** \brief Initialize library.
 * Initialize (or reinitialize) library's internal structures. This function
 * must be called before any other function (except functions of iterator API). 
 * If the library was already initialized, internal structures are cleaned up
 * and reinitialized again.
 * \return Always zero. 
 */ 
int nfdump_init(void);

/** \brief Register a callback function. 
 * Register a callback function which is then called on every record matching 
 * given filter.
 * \param[in] cb_func Pointer to the callback function.
 * \param[in] cb_param A parameter which is passed to the callback function.  
 *    every time it gets called due to the given filter. This allows to use the
 *    same function for different filters.
 * \param[in] filter The filter in nfdump format. 
 * \param[in] limitflows Maximal number of matched flows, any further flows are
 *    ignored (0 = unlimited).
 * \return Error code, 0 on success. 
 */
int nfdump_add_callback(callback_func_t cb_func, void *cb_param,
                        const char *filter, uint64_t limitflows);

/** \brief Process a single file.
 * Read given nfdump file, try to match each record with all registered filters 
 * and pass the record to corresponding callback function for any matching 
 * filter. At least one callback function must be registered before this  
 * function is called. The processing continues until all records in the file
 * are read or until nfdump_stop is called from within a callback function. 
 * \param[in] filename Name of nfdump file to read.
 * \return Error code, 0 on success.
 */ 
int nfdump_process_file(const char *filename);

/** \brief Process a sequence of files.
 * The same as nfdump_process_files but for sequence of files.
 * The sequence is given by parameters with the same semantics as nfdump options
 * -M, -r and -R. See nfdump documentation for details.
 * 
 * If Mdirs is specified either rfile or Rfile must be specified as well.
 * Exactly one of rfile and Rfile must be specified.
 *
 * \param[in] Mdirs The same meaning as -M option of nfdump, set to NULL to not use it.
 * \param[in] rfile The same meaning as -r option of nfdump, set to NULL to not use it.
 * \param[in] Rfiles The same meaning as -R option of nfdump, set to NULL to not use it. 
 * \return Error code, 0 on success.
 */
int nfdump_process_files(const char *M, const char *r, const char* R);

/** \brief Stop processing.
 * Stop reading of records in file(s) initiated by nfdump_process_file or
 * nfdump_process_files. This can be called from within a callback function in 
 * order to stop any further calls of all callbacks. The nfdump_process_file
 * function returns immediately after the current record is processed by all 
 * callback functions.
 * \return Always 0. 
 */
int nfdump_stop(void);

/** \brief Clean up internal structures.
 * This function should be called after the library is not needed any more,
 * typically before the application exits.
 * No library function (except nfdump_init) should be called after the cleanup
 * was performed.  
 * \return Always 0.
 */   
int nfdump_cleanup(void);

#ifdef __cplusplus
} // extern "C"
#endif

//#pragma GCC diagnostic pop

#endif

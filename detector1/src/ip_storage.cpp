/**
 * ip_storage.cpp
 * This file implements methods of class ip_storage, which is used for storing IP addresses and their data.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    18.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <iterator>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>


#include "ip_storage.h"
#include "common.h"
#include "shared.h"

using namespace std;


///////////////////////////////////////////////////////////////// STORAGE_DATA METHODS

/**
 * Constructor
 */
storage_data::storage_data()
{
    packets = 0;
    bytes = 0;
    rdlen_total = 0;
    namelen_total = 0;
    queries = 0;
    bytes_only_queries = 0;
    occurences = 0;
    most_used_domain_counter = 0;
    most_used_qtype_counter = 0;
    firstTime.is_valid = false;
    lastTime.is_valid = false;
    maxDuration = 0;
}

/**
 * Destructor
 */
storage_data::~storage_data()
{
}

/**
 * Add aggregation data into existing storage data.
 */
bool storage_data::add(const aggreg_data &data)
{
    //search data and add new/increment counter

    //domain
    if (data.domain.length() > 0)
    {
        string smalldomain = extract_domain_name_to_nth_level(data.domain, shared_parameters.dns_extract_domain_level); //do not store whole query
        for (unsigned int i=0; i<shared_parameters.whitelist_domains.size(); i++) //check whitelist domains
        {
            if (smalldomain.compare(shared_parameters.whitelist_domains.at(i)) == 0 || smalldomain.compare("\"\"") == 0)    //"" domain is root domain
                return false; //do not detect whitelisted domain queries
        }

        domains_map[smalldomain]++;
        namelen_total += data.domain.length();

        if (domains_map[smalldomain] > most_used_domain_counter)    //new most used domain
            most_used_domain_counter = domains_map[smalldomain];
    }

    //src port
    if (data.SRCport != 0)
        srcports_map[data.SRCport]++;

    //dst port
    if (data.DSTport != 0)
        dstports_map[data.DSTport]++;

    //time
    if (data.ftime.is_valid)
    {
        if (! firstTime.is_valid)
            firstTime = data.ftime;
        else if (firstTime.compare_with(data.ftime) > 0)
            firstTime = data.ftime;

        if (! lastTime.is_valid)
            lastTime = data.ftime;
        else if (lastTime.compare_with(data.ftime) < 0)
            lastTime = data.ftime;

        if (data.ftime.duration > maxDuration)
            maxDuration = data.ftime.duration;
    }

    if (data.qType)
    {
        qtypes_map[data.qType]++;

        if (qtypes_map[data.qType] > most_used_qtype_counter)    //new most used qType
            most_used_qtype_counter = qtypes_map[data.qType];
    }

    if (data.rdLen > 0)
        rdlen_total += data.rdLen;

    if (data.packets > 0)
        packets += data.packets;

    if (data.bytes > 0)
        bytes += data.bytes;

    if (data.is_query)
    {
        queries += data.packets;
        bytes_only_queries += data.bytes;
    }

    occurences++;

    return true;

}

/**
 * Return counter from specific port
 */
u_int32_t storage_data::get_domain_counter(string domain)
{
    return domains_map[domain];
}

/**
 * Return counter for specific DNS query type
 */
u_int32_t storage_data::get_qtype_counter(u_int16_t qtype)
{
    return qtypes_map[qtype];
}

/**
 * Return average packet size for this IP
 */
double storage_data::get_avg_packet_size()
{
    if (packets > 0)
        return ((double)bytes/(double)packets);
    else
        return 0;
}

/**
 * Return average DNS RDLEN size for this IP
 */
double storage_data::get_avg_rdlen_size()
{
    if (packets > 0)
        return (double)rdlen_total/(double)packets;
    else
        return 0;
}

/**
 * Return average DNS domain name length for this IP
 */
double storage_data::get_avg_namelen_size()
{
    if (packets > 0)
        return (double)namelen_total/(double)packets;
    else
        return 0;
}

///////////////////////////////////////////////////////////////// IP_STORAGE METHODS

/**
 * Constructor
 */
ip_storage::ip_storage()
{
}

/**
 * Destructor
 */
ip_storage::~ip_storage()
{
}

/**
 * Aggregate useful IPv4 data into table.
 */
bool ip_storage::add_flow_data(u_int32_t srcIP, u_int32_t dstIP, aggreg_data data)
{
    flow_key k(srcIP, dstIP);
    return table[k].add(data);
}

/**
 * Aggregate useful IPv6 data into table.
 */
bool ip_storage::add_flow_data(in6_addr srcIP, in6_addr dstIP, aggreg_data data)
{
    flow_key k(srcIP, dstIP);
    return table[k].add(data);
}

/**
 * Aggregate statistical data about this bin.
 */
void ip_storage::add_statistics_data(u_int32_t flows, u_int16_t packets, u_int32_t bytes)
{
    summary_data.add(flows, packets, bytes);
}

/**
 * Add parsed pcap packet data into local vector.
 */
void ip_storage::add_pkt(pkt p)
{
    pkts.push_back(p);
}

/**
 * Add parsed IPFIX flow data into local vector.
 */
void ip_storage::add_ipfix_flow(ipfix_flow f)
{
    xflows.push_back(f);
}

/**
 * Add parsed NetFlow flow data into local vector.
 */
void ip_storage::add_netflow_flow(flow f)
{
    nflows.push_back(f);
}

/**
 * Get summary statistical data about this bin
 */
bin_data *ip_storage::get_stats_data()
{
    return &summary_data;
}

/**
 * Get pointer to table with all data
 */
unordered_map<flow_key, storage_data, flow_key_hasher> *ip_storage::get_map()
{
    return &table;
}

/**
 * Get pointer to vector of parsed packets.
 */
vector<pkt> *ip_storage::get_pkts_vector()
{
    return &pkts;
}

/**
 * Get pointer to vector of parsed IPFIX flows.
 */
vector<ipfix_flow> *ip_storage::get_ipfix_flows_vector()
{
    return &xflows;
}

/**
 * Get pointer to vector of parsed NetFlow flows.
 */
vector<flow> *ip_storage::get_netflow_flows_vector()
{
    return &nflows;
}

/**
 * Get count of stored IP addresses...
 */
int ip_storage::get_addr_count()
{
    return table.size();
}

/**
 * Clear table (delete actual data).
 */
void ip_storage::clear_all()
{
    table.clear();
    summary_data.clear();
    pkts.clear();
    xflows.clear();
    nflows.clear();
}

/**
 * Print stored data.
 */
void ip_storage::print_data()
{
    cout << "Bin start: " << start_time.get_date_time_str() << ", end: " << end_time.get_date_time_str() << endl;
    cout << "Bin bytes: " << summary_data.bytes << ", packets: " << summary_data.packets << ", bpppb: ";
    cout << summary_data.get_bytes_per_packet_per_bin() << ", flows: " << summary_data.flows << endl;
    cout << "Bin contains: " << endl;

    for(unordered_map<flow_key, storage_data, flow_key_hasher>::iterator iter = table.begin(); iter != table.end(); iter++)
    {
        flow_key flwIPs = (flow_key)((*iter).first);
        string srcIP_str, dstIP_str;

        if (flwIPs.ipv6)
        {
            srcIP_str = get_ipv6_str(flwIPs.srcIP.v6);
            dstIP_str = get_ipv6_str(flwIPs.dstIP.v6);

        }
        else
        {
            in_addr srcIP, dstIP;
            srcIP.s_addr = flwIPs.srcIP.v4.s_addr;
            dstIP.s_addr = flwIPs.dstIP.v4.s_addr;
            srcIP_str = get_ipv4_str(srcIP);
            srcIP_str = get_ipv4_str(dstIP);
        }

        storage_data *data;
        data = (storage_data *) &((*iter).second);

        cout << srcIP_str << " -> ";
        cout << dstIP_str << ": packets=" << data->get_packets_count() << ", bytes=" << data->get_bytes_total();
        cout << ", avg=" << data->get_avg_packet_size() << " Bpp" << endl;

        //all ports
        cout << "src ports:" << endl;
        for (map<u_int16_t, u_int32_t>::iterator it = data->get_srcports_map()->begin(); it != data->get_srcports_map()->end(); it++)
            cout << "\t" << it->first << "\t\t" << it->second << endl;

        cout << "dst ports:" << endl;
        for (map<u_int16_t, u_int32_t>::iterator it = data->get_dstports_map()->begin(); it != data->get_dstports_map()->end(); it++)
            cout << "\t" << it->first << "\t\t" << it->second << endl;

        //all domains
        for (map<string, u_int32_t>::iterator it = data->get_domains_map()->begin(); it != data->get_domains_map()->end(); it++)
            cout << "\t" << it->first << "\t\t" << it->second << endl;
    }

    cout << "-----------------------------" << endl;
}

/**
 * Print data to file (formatted output to file)
 */
void ip_storage::print_data_to_file(ofstream *file)
{
    if (file)
    {
        (*file) << "\nBin contains: " << endl;

        for(unordered_map<flow_key, storage_data, flow_key_hasher>::iterator iter = table.begin(); iter != table.end(); iter++)
        {
            flow_key flwIPs = (flow_key)((*iter).first);
            string srcIP_str, dstIP_str;

            if (flwIPs.ipv6)
            {
                srcIP_str = get_ipv6_str(flwIPs.srcIP.v6);
                dstIP_str = get_ipv6_str(flwIPs.dstIP.v6);

            }
            else
            {
                in_addr srcIP, dstIP;
                srcIP.s_addr = flwIPs.srcIP.v4.s_addr;
                dstIP.s_addr = flwIPs.dstIP.v4.s_addr;
                srcIP_str = get_ipv4_str(srcIP);
                srcIP_str = get_ipv4_str(dstIP);
            }

            storage_data *data;
            data = (storage_data *) &((*iter).second);

            (*file) << srcIP_str << " -> ";
            (*file) << dstIP_str << ": packets=" << data->get_packets_count() << ", bytes=" << data->get_bytes_total();
            (*file) << ", avg=" << data->get_avg_packet_size() << " Bpp" << endl;

            //all ports
            (*file) << "src ports:" << endl;
            for (map<u_int16_t, u_int32_t>::iterator it = data->get_srcports_map()->begin(); it != data->get_srcports_map()->end(); it++)
                (*file) << "\t" << it->first << "\t\t" << it->second << endl;

            (*file) << "dst ports:" << endl;
            for (map<u_int16_t, u_int32_t>::iterator it = data->get_dstports_map()->begin(); it != data->get_dstports_map()->end(); it++)
                (*file) << "\t" << it->first << "\t\t" << it->second << endl;

            //all domains
            for (map<string, u_int32_t>::iterator it = data->get_domains_map()->begin(); it != data->get_domains_map()->end(); it++)
                (*file) << "\t" << it->first << "\t\t" << it->second << endl;
        }

        (*file) << "-----------------------------" << endl;
    }
}

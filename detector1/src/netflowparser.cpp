/**
 * netflowparser.cpp
 * This file implements methods of class netflow_parser.
 *
 * Nfdump binary files are parsed with "libnfdump" library, which can be obtained here:
 * http://sourceforge.net/projects/libnfdump/files/?source=navbar
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    25.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>


#include "netflowparser.h"
#include "common.h"
//#include "nfreader.h"
#include "libnfdump.h"
#include "ip_storage.h"
#include "netflowstatistics.h"
#include "netflowstatisticsmaker.h"
#include "shared.h"
#include "binsorter.h"

using namespace std;

////////////////////////////////////////////////////////////// NETFLOW_PARSER METHODS

/**
 * Constructor
 */
netflow_parser::netflow_parser(params *par)
{
    parameters = par;
    first_done = false;
    counter = 0;
    infile_binary_opened = false;
    get_file_list();
}

/**
 * Destructor
 */
netflow_parser::~netflow_parser()
{
}

/**
 * Get list of all input files (fill infiles vector).
 */
void netflow_parser::get_file_list()
{
    if (! parameters->infile.empty())
        infiles.push_back(parameters->infile);
    else if (! parameters->indir.empty())
    {
        //get list of files in specified directory
        string folder(parameters->indir);

        //erase last /
        if (folder.at(folder.length()-1) == '/')
            folder.erase(folder.length()-1);

        //cmd (find folder/ -type f)
        string cmd = "find ";
        cmd += folder;
        cmd += "/ -type f";

        //run cmd and parse output
        string sysout = system_cmd(cmd.c_str());

        //parse output
        size_t pos = 0;
        string line;
        string delim = "\n";

        while ((pos = sysout.find(delim)) != string::npos)
        {
            line = sysout.substr(0, pos);
            infiles.push_back(line);  //add to file list
            sysout.erase(0, pos + delim.length());
        }
    }
}

/**
 * Parse next flow record from input file.
 * If parse_only_statistics switch is true, then only statistically important data will be parsed.
 * Returns pointer to parsed data, or NULL (why data couldnt be parsed is specified in err variable).
 */
flow *netflow_parser::get_next_flow(int *err, bool parse_only_statistics)
{
    if (parameters->filetype == 'n')    //csv format
    {
        bool file_done = false;
        CSVRow row; //flow data

        if(infile_csv >> row)   //load next row
        {
            if (! first_done)   //skip first line (header)
            {
                fill_header(&row);  //get column indexes
                first_done = true;
                *err = RET_NOT_DNS_FLOW;
                return NULL;
            }

            if (row.size() < NFDUMP_CSV_COLS_MAX)   //skip lines with summary
                file_done = true;
            else
            {
                if (parse_flow_csv_fields(&row, &flw, parse_only_statistics))  //parse flow into flow record
                {
                    *err = RET_OK;
                    return &flw;    //only DNS flows here
                }
                else
                    *err = RET_NOT_DNS_FLOW;
            }
        }
        else
            file_done = true;

        if (file_done)
        {
            //open new file
            int ret = open_infile_csv();
            *err = ret;
        }
    }
    else if (parameters->filetype == 'd')   //nfdump binary format
    {
        master_record_t *flowdata;   //flow data, temp store

        int ret = nfdump_iter_next(&infile_binary, &flowdata);    //read next record
        if (ret == NFDUMP_EOF)   //file finished
        {
            //open next file
            int ret = open_infile_binary();
            *err = ret;
        }
        else if (ret != NFDUMP_E_OK)   //error
        {
            if (ret == NFDUMP_E_CORRUPTED_FILE)
                cerr << "Error: Input flow file is corrupted!" << endl;
            else if (ret == NFDUMP_E_CANT_OPEN_FILE)
                cerr << "Error: Cant open NetFlow nfdump file!" << endl;

            *err = RET_ERROR;
            return NULL;
        }
        else    //read ok
        {
            if (parse_flow_binary_fields(flowdata, &flw, parse_only_statistics))   //extract useful data
            {
                *err = RET_OK;
                return &flw;
            }
            else
                *err = RET_NOT_DNS_FLOW;
        }
    }

    return NULL;
}

/**
 * Save column indexes from CSV header
 */
void netflow_parser::fill_header(CSVRow *row)
{
    for (unsigned int i=0; i<row->size(); i++)
    {
        if ((*row)[i].compare("sa") == 0)
            header.SRC_IP = i;
        else if ((*row)[i].compare("da") == 0)
            header.DST_IP = i;
        else if ((*row)[i].compare("ts") == 0)
            header.TIME_START = i;
        else if ((*row)[i].compare("te") == 0)
            header.TIME_END = i;
        else if ((*row)[i].compare("td") == 0)
            header.TIME_DURATION = i;
        else if ((*row)[i].compare("dp") == 0)
            header.DST_PORT = i;
        else if ((*row)[i].compare("sp") == 0)
            header.SRC_PORT = i;
        else if ((*row)[i].compare("pr") == 0)
            header.PROTOCOL = i;
        else if ((*row)[i].compare("ipkt") == 0)
            header.IN_PKTS = i;
        else if ((*row)[i].compare("opkt") == 0)
            header.OUT_PKTS = i;
        else if ((*row)[i].compare("ibyt") == 0)
            header.IN_BYTES = i;
        else if ((*row)[i].compare("obyt") == 0)
            header.OUT_BYTES = i;
    }
}

/**
 * Parse flow fields into "flwdest".
 * If parse_only_statistics switch is true, then only statistically important data will be parsed.
 * Returns true if it is DNS flow, false otherwise.
 */
bool netflow_parser::parse_flow_csv_fields(CSVRow *flowdata, flow *flwdest, bool parse_only_statistics)
{
    counter++;

    //skip not-flow lines
    if (flowdata->size() < NFDUMP_CSV_COLS_MAX)
        return false;

    flwdest->flowNum = counter;

    //time (flow end time)
    flwdest->flowTime.init_flow_time((*flowdata)[header.TIME_END], (*flowdata)[header.TIME_DURATION]);

    //IP header
    if (! parse_only_statistics)
    {
        if (! inet_pton(AF_INET, (*flowdata)[header.SRC_IP].c_str(), &(flwdest->srcIP.v4)))
        {
            if (! inet_pton(AF_INET6, (*flowdata)[header.SRC_IP].c_str(), &(flwdest->srcIP.v6)))
                return false;
            else
                flwdest->ipv6 = true;
        }
        else
            flwdest->ipv6 = false;

        if (! inet_pton(AF_INET, (*flowdata)[header.DST_IP].c_str(), &(flwdest->dstIP.v4)))
        {
            if (! inet_pton(AF_INET6, (*flowdata)[header.DST_IP].c_str(), &(flwdest->dstIP.v6)))
                return false;
        }
    }

    if ((*flowdata)[header.PROTOCOL].compare("UDP") == 0)   //protocol
        flwdest->ipProtocol = IPPROTO_UDP;
    else
        return false;   //we only care about UDP packets

    //UDP header
    bool err = false;
    flwdest->srcPort = (u_int16_t) get_int_from_str((*flowdata)[header.SRC_PORT].c_str(), &err);
    flwdest->dstPort = (u_int16_t) get_int_from_str((*flowdata)[header.DST_PORT].c_str(), &err);

    if (flwdest->srcPort != DNS_PORT && flwdest->dstPort != DNS_PORT)   //not a DNS packet
        return false;

    //other
    int inpkts = get_int_from_str((*flowdata)[header.IN_PKTS].c_str(), &err);
    int inbytes = get_int_from_str((*flowdata)[header.IN_BYTES].c_str(), &err);
    int outpkts = get_int_from_str((*flowdata)[header.OUT_PKTS].c_str(), &err);
    int outbytes = get_int_from_str((*flowdata)[header.OUT_BYTES].c_str(), &err);

    if (err)    //check error during str to int conversions
        return false;

    flwdest->packets = (u_int16_t) inpkts + outpkts;
    flwdest->bytes = (u_int32_t) inbytes + outbytes;

    return true;
}

/**
 * Copy flow fields from master_record_t into flow structure.
 * If parse_only_statistics switch is true, then only statistically important data will be parsed.
 * Returns true if it is DNS flow, false otherwise.
 */
bool netflow_parser::parse_flow_binary_fields(master_record_t *flowdata, flow *flwdest, bool parse_only_statistics)
{
    counter++;

    flwdest->flowNum = counter;

    //time and date (flow end time)
    time_t endt = flowdata->last;
    struct tm *loctime = localtime(&endt);
    flwdest->flowTime.set_date((u_int16_t)(loctime->tm_year + 1900), (u_int8_t)(loctime->tm_mon + 1), (u_int8_t)loctime->tm_mday);
    flwdest->flowTime.set_time((u_int8_t)loctime->tm_hour, (u_int8_t)loctime->tm_min, (u_int8_t)loctime->tm_sec);

    //IP header
    if (! parse_only_statistics)
    {
        if (flowdata->flags & 0x1)  //IPv6
        {
            memcpy(&(flwdest->srcIP.v6), &(flowdata->ip_union._v6.srcaddr), sizeof(in6_addr));
            memcpy(&(flwdest->dstIP.v6), &(flowdata->ip_union._v6.dstaddr), sizeof(in6_addr));
            flwdest->ipv6 = true;
        }
        else    //IPv4
        {
            flwdest->srcIP.v4.s_addr = ntohl(flowdata->ip_union._v4.srcaddr);
            flwdest->dstIP.v4.s_addr = ntohl(flowdata->ip_union._v4.dstaddr);
            flwdest->ipv6 = false;
        }
    }

    flwdest->ipProtocol = flowdata->prot;   //protocol
    if (flwdest->ipProtocol != IPPROTO_UDP)
        return false;   //we only care about UDP packets

    //UDP header
    flwdest->srcPort = flowdata->srcport;
    flwdest->dstPort = flowdata->dstport;

    if (flwdest->srcPort != DNS_PORT && flwdest->dstPort != DNS_PORT)   //not a DNS packet
        return false;

    //other
    flwdest->packets = (u_int16_t) flowdata->dPkts;
    flwdest->bytes = (u_int32_t) flowdata->dOctets;

    return true;
}

/**
 * Open next input file (common function for all types of file).
 * Returns code based on result of done actions.
 */
int netflow_parser::open_infile()
{
    if (parameters->filetype == 'n')
        return open_infile_csv();
    else if (parameters->filetype == 'd')
        return open_infile_binary();

    return RET_OK;
}

/**
 * Open next input csv file.
 * Returns code based on result of done actions.
 */
int netflow_parser::open_infile_csv()
{
    close_infile(); //close previously opened file

    if (infiles.size() == 0)    //all files finished
        return RET_EMPTY;

    infile_csv.open(infiles[0]);
    if (! infile_csv.good())
    {
        cerr << "Couldn't open input file " << infiles[0] << "!" << endl;
        return RET_ERROR;
    }

    opened_stuff_shared.opened_infile = &infile_csv;

    infiles.erase(infiles.begin());   //first file is done
    first_done = false;

    return RET_OK;
}

/**
 * Open next input binary file.
 * Returns code based on result of done actions.
 */
int netflow_parser::open_infile_binary()
{
    close_infile(); //close previously opened file

    if (infiles.size() == 0)    //all files finished
        return RET_EMPTY;

    //open nfdump binary file and prepare to read flows (filter is to read only DNS packets)
    if (nfdump_iter_start(&infile_binary, (char *)(infiles[0].c_str()), "proto udp and ( src port 53 or dst port 53 )") != NFDUMP_E_OK)
    {
        cerr << "Couldn't open input file " << infiles[0] << "!" << endl;
        return RET_ERROR;
    }
    else
        infile_binary_opened = true;

    opened_stuff_shared.opened_nfdump_file = &infile_binary;

    infiles.erase(infiles.begin());   //first file is done
    first_done = false;

    return RET_OK;
}

/**
 * Close opened file(s)
 */
void netflow_parser::close_infile()
{
    if (infile_csv.is_open())
    {
        infile_csv.close();
        opened_stuff_shared.opened_infile = NULL;
    }

    if (infile_binary_opened)
    {
        nfdump_iter_end(&infile_binary);
        opened_stuff_shared.opened_nfdump_file = NULL;
        infile_binary_opened = false;
    }
}


////////////////////////////////////////////////////////////// NETFLOW_BIN_MAKER METHODS

/**
 * Constructor
 */
netflow_bin_maker::netflow_bin_maker(params *pars)
{
    parameters = pars;
    total_bins = 0;
    total_flws_processed = 0;
    total_flws_filtered = 0;
}

/**
 * Destructor
 */
netflow_bin_maker::~netflow_bin_maker()
{
}

/**
 * Parse input file.
 */
void netflow_bin_maker::parse_infile()
{
    int retcode;
    netflow_parser parser(parameters);
    retcode = parser.open_infile(); //open first file, rest is managed by parser itself
    if (retcode != RET_OK)
        err_exit();

    flow *flw;   //flow record

    bin_sorter sorter(parameters);  //for sorting flows by end time

    do  //read flow records
    {
        flw = parser.get_next_flow(&retcode, false);
        if (flw && retcode == RET_OK)   //DNS flow
        {
            total_flws_filtered++;

            //put this flow into correct bin by flow end time
            sorter.sort_into_bin(flw);

            //print packet info if needed
            if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
            {
                mutex_cout.lock();
                print_flow(flw);
                mutex_cout.unlock();
            }
        }
        else if (retcode == RET_ERROR)
            err_exit();

        total_flws_processed++;

        if (stop)
            break;

        //automatically skip all non-DNS flows
    } while (retcode != RET_ERROR && retcode != RET_EMPTY);

    sorter.push_all_to_shared();            //put all waiting bins into shared queue
    shared_bin_pointers.set_finished(true); //all loading is finished

    total_bins = sorter.get_total_bins();
}

/**
 * Print flow information (debug function).
 */
void netflow_bin_maker::print_flow(flow *flw)
{
    if (flw)
    {
        cout << flw->flowNum << ": " << flw->flowTime.get_time_str() << ", bytes=" << flw->bytes << ", " << "pkts=" << flw->packets << ", " << get_srcIP_str(flw);
        cout << " -> " << get_dstIP_str(flw) << ", sp: " << flw->srcPort << ", dp: " << flw->dstPort << endl;
    }
}

/**
 * Write flow information into file.
 */
void netflow_bin_maker::save_to_file(ofstream *file, flow *flw)
{
    if (file && flw)
    {
        (*file) << flw->flowTime.get_time_str() << " " << get_srcIP_str(flw) << " -> ";
        (*file) << get_dstIP_str(flw) << ", sp=" << flw->srcPort << ", dp=";
        (*file) << flw->dstPort << ", pkts=" << flw->packets << ", bytes=" << flw->bytes << endl;
    }
}

/**
 * Save summary msg to shared obj.
 */
void netflow_bin_maker::save_summary()
{
    ostringstream str;
    str << "Total flows processed: " << total_flws_processed << endl;
    str << "Total flows filtered:  " << total_flws_filtered << endl;
    str << "Total bins created:    " << total_bins << endl;

    shared_summary.set_parser_msg(str.str());
}


/**
 * pcapdetector1.cpp
 * This file implements methods of class pcap_detector1.
 *
 * Based on (libpcap part):
 * http://code.google.com/p/pcapsctpspliter/issues/detail?id=6
 * https://github.com/uvic-sdo/DNS-Packet-Parser
 *
 * Based on (DNS part):
 * RFC 1035 (http://tools.ietf.org/html/rfc1035)
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    13.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <ctime>
#include <vector>
#include <unordered_map>
#include <pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>


#include "pcapdetector1.h"
#include "common.h"
#include "ip_storage.h"
#include "shared.h"

using namespace std;

/**
 * Constructor
 */
pcap_detector1::pcap_detector1(params *pars)
{
    parameters = pars;
    bin = NULL;
    total_bins = 0;
    stored_bins = 0;
    total_detections = 0;
    total_pkts_analyzed = 0;
    total_pkts_processed = 0;
    total_pkts_whitelisted = 0;

    for(int i=0; i<PCAP_SINGLE_THRESHOLDS; i++)
        thresh_stats[i] = 0;
}

/**
 * Destructor
 */
pcap_detector1::~pcap_detector1()
{
    delete_finished_bin();
}

/**
 * Process shared queue of bins (detection loop).
 */
void pcap_detector1::process_queue()
{
    ofstream outfile(parameters->outfile);  //open output file
    if (! outfile.is_open())
    {
        cerr << "Couldn't create output file " << parameters->outfile << "!" << endl;
        err_exit();
    }

    opened_stuff_shared.opened_outfile = &outfile;

    //process all bins in shared queue
    //(NULL is returned when queue is empty and producer process has finished)
    while ((bin = shared_bin_pointers.get_data()) != NULL)
    {
        //check aggregated data every N bins
        if (stored_bins == parameters->max_aggregated_bins)
            inspect_aggregated_bins(&outfile);

        //detection
        detect_in_bin(bin);   //bin version

        delete_finished_bin();  //delete finished bin

        total_bins++;
        stored_bins++;

        if (stop)
            break;
    }

    //inspect final aggregated data
    inspect_aggregated_bins(&outfile);

    outfile.close();
    opened_stuff_shared.opened_outfile = NULL;
}

/**
 * Provides detection method for whole bin of packets.
 * Detection is based on: detection schema selected in config file.
 * Returns true if bin is marked as tunnel, false otherwise.
 */
void pcap_detector1::detect_in_bin(ip_storage *bin_to_inspect)
{
    if (bin_to_inspect)
    {
        vector<pkt> *pkts = bin_to_inspect->get_pkts_vector();

        //check each packet from bin
        for (unsigned int i=0; i<pkts->size(); i++)
        {
            //detection based on selected detection schema
            if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_ALL)
            {
                //aggregate all packets in bin
                put_ip_data_in_map(&((*pkts)[i]));
            }
            else if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_SINGLE)
            {
                //aggregate only certain packets
                if (detect(&((*pkts)[i])))
                    put_ip_data_in_map(&((*pkts)[i]));
            }
            else if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES)
            {
                //aggregate only suspicious DNS query types commonly used for DNS tunneling
                u_int16_t qtype = (*pkts)[i].qType;
                if (qtype == QTYPE_MX || qtype == QTYPE_NS || qtype == QTYPE_TXT || qtype == QTYPE_NULL || qtype == QTYPE_CNAME)
                    put_ip_data_in_map(&((*pkts)[i]));
            }
            else if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES_THRESHOLDS)
            {
                //aggregate only suspicious DNS query types where thresholds are met
                u_int16_t qtype = (*pkts)[i].qType;
                if ((qtype == QTYPE_MX || qtype == QTYPE_NS || qtype == QTYPE_TXT || qtype == QTYPE_NULL || qtype == QTYPE_CNAME)
                        && detect(&((*pkts)[i])))
                    put_ip_data_in_map(&((*pkts)[i]));
            }

            if (stop)
                break;
        }

        total_pkts_processed++;
    }
}


/**
 * Provides detection method for each packet.
 * Detection is based on: thresholds specified in config file.
 * Returns true if packet is marked as tunnel, false otherwise.
 */
bool pcap_detector1::detect(pkt *packet)
{
    bool ret = false;

    if (packet)
    {
        if (packet->ipLen > parameters->packet_size_thresh)                         //packet len threshold
        {
            ret = true;
            ++thresh_stats[PCAP_THRESH_PACK_SIZE_POS];
        }
        else if (packet->msgType == DNS_TYPE_RESPONSE && packet->ttl < parameters->packet_ttl_thresh)     //DNS TTL threshold (only on responses)
        {
            ret = true;
            ++thresh_stats[PCAP_THRESH_TTL_POS];
        }
        else if (packet->rdLen > parameters->packet_rdata_size_thresh)                 //DNS RDATA size threshold
        {
            ret = true;
            ++thresh_stats[PCAP_THRESH_RDLEN_POS];
        }
        else if (packet->msgType == DNS_TYPE_QUERY && packet->ipLen > parameters->query_packet_size_thresh) //DNS query packet size threshold
        {
            ret = true;
            ++thresh_stats[PCAP_THRESH_QUERY_SIZE_POS];
        }
        else if (packet->qName.length() > parameters->avg_namelen_map_thresh)          //DNS query domain length
        {
            ret = true;
            ++thresh_stats[PCAP_THRESH_QNAME_LEN_POS];
        }
        else if (subdomains_have_same_len(packet->qName) || subdomains_have_same_len(packet->rData))  //multiple subdomains of same length
        {
            ret = true;
            ++thresh_stats[PCAP_THRESH_SUBDOMAIN_LEN_POS];
        }
        else if (contains_too_much_non_ascii_data(packet->qName) || contains_too_much_non_ascii_data(packet->rData))  //too much non-ascii data in query or rdata
        {
            ret = true;
            ++thresh_stats[PCAP_THRESH_TOO_MUCH_ASCII_POS];
            //if (contains_too_much_non_ascii_data(packet->qName))
                //cerr << packet->pktTime.get_time_str() << " srcport=" << packet->srcPort << ", dstport=" << packet->dstPort << ", isQuery: " << packet->msgType << ", Q: " << packet->qName << endl;
            //else
                //cerr << packet->pktTime.get_time_str() << " srcport=" << packet->srcPort << ", dstport=" << packet->dstPort << ", isQuery: " << packet->msgType << ", R: " << packet->rData << endl;
        }
    }

    return ret;
}

/**
 * Inspect aggregated data and print detections into file.
 */
void pcap_detector1::inspect_aggregated_bins(ofstream *file)
{
    unordered_map<flow_key, storage_data, flow_key_hasher> *table = ipmap.get_map();

    const int TOTAL_CRITERIAS = 8;  //total number of detection criterias

    //iterate through aggregated records
    for(unordered_map<flow_key, storage_data, flow_key_hasher>::iterator iter = table->begin(); iter != table->end(); iter++)
    {
        in_addr srcIP, dstIP;
        in6_addr srcIPv6, dstIPv6;
        flow_key fk = (flow_key)((*iter).first);
        if (fk.ipv6)
        {
            srcIPv6 = fk.srcIP.v6;
            dstIPv6 = fk.dstIP.v6;
        }
        else
        {
            srcIP.s_addr = fk.srcIP.v4.s_addr;
            dstIP.s_addr = fk.dstIP.v4.s_addr;
        }

        storage_data *data;
        data = (storage_data *) &((*iter).second);

        ostringstream out_stream;
        unsigned int conditions_matched = 0;

        //detection
        if (data->get_avg_packet_size() > parameters->avg_size_map_thresh && data->get_packets_count() > parameters->map_minimum_packets) //avg packet size threshold (needs more packets)
        {
            out_stream << "\t" << "avg packet size = " << data->get_avg_packet_size() << " B" << " (detection threshold = " << parameters->avg_size_map_thresh << "), packets: " << data->get_packets_count() << endl;
            ++conditions_matched;
        }
        if (data->get_bytes_total() > parameters->total_bytes_map_thresh) //total bytes threshold
        {
            out_stream << "\t" << "total bytes = " << data->get_bytes_total() << " B (detection threshold = " << parameters->total_bytes_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_occurences_count() > parameters->occurences_in_map_thresh) //occurences in map threshold
        {
            out_stream << "\t" << "occurences in map = " << data->get_occurences_count() << " (detection threshold = " << parameters->occurences_in_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_queries_total() > parameters->total_queries_map_thresh) //number of queries threshold
        {
            out_stream << "\t" << "total number of queries = " << data->get_queries_total() << " (detection threshold = " << parameters->total_queries_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_bytes_only_queries() > parameters->total_bytes_queries_only_map_thresh) //total bytes only on queries
        {
            out_stream << "\t" << "total bytes only on queries = " << data->get_bytes_only_queries() << " B (detection threshold = " << parameters->total_bytes_queries_only_map_thresh << ") , queries: " << data->get_queries_total() << endl;
            ++conditions_matched;
        }
        if (data->get_avg_namelen_size() > parameters->avg_namelen_map_thresh && data->get_packets_count() > parameters->map_minimum_packets) //average DNS query length threshold
        {
            out_stream << "\t" << "avg DNS query len = " << data->get_avg_namelen_size() << " (detection threshold = " << parameters->avg_namelen_map_thresh << "), packets: " << data->get_packets_count() << endl;
            ++conditions_matched;
        }
        if (data->get_most_used_domain_count() > parameters->most_used_domain_count_thresh)
        {
            out_stream << "\t" << "most used domain count = " << data->get_most_used_domain_count() << " (detection threshold = " << parameters->most_used_domain_count_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_most_used_qtype_count() > parameters->most_used_qtype_count_thresh)
        {
            out_stream << "\t" << "most used qType count = " << data->get_most_used_qtype_count() << " (detection threshold = " << parameters->most_used_qtype_count_thresh << ")" << endl;
            ++conditions_matched;
        }

        //print detected IP info
        if (conditions_matched >= parameters->map_minimum_conditions)
        {
            u_int16_t srcPort = (data->get_srcports_count() > 1) ? 0 : data->get_srcports_map()->begin()->first;
            u_int16_t dstPort = (data->get_dstports_count() > 1) ? 0 : data->get_dstports_map()->begin()->first;
            string srcIP_str = (fk.ipv6) ? get_ipv6_str(srcIPv6) : get_ipv4_str(srcIP);
            string dstIP_str = (fk.ipv6) ? get_ipv6_str(dstIPv6) : get_ipv4_str(dstIP);

            //detect only communication from tunnel client to tunnel server
            if (parameters->detect_only_client_communication && srcPort == DNS_PORT)
                continue;

            //print flow time and IPs/ports
            (*file) << data->get_first_time().get_date_time_str() << " - " << data->get_last_time().get_date_time_str() << endl;
            (*file) << srcIP_str << ":";
            if (srcPort == 0)
                (*file) << "XXXXX";
            else
                (*file) << srcPort;
            (*file) << " -> " << dstIP_str << ":";
            if (dstPort == 0)
                (*file) << "XXXXX" << endl;
            else
                (*file) << dstPort << endl;

            //print domains
            (*file) << "Used domains:" << endl;
            for (map<string, u_int32_t>::iterator iter = data->get_domains_map()->begin(); iter != data->get_domains_map()->end(); iter++)
            {
                (*file) << "\t" << iter->first << "\t\t" << iter->second << "x" << endl;
            }

            //print DNS query types
            (*file) << "Used query types:" << endl;
            for (map<u_int16_t, u_int32_t>::iterator iter = data->get_qtypes_map()->begin(); iter != data->get_qtypes_map()->end(); iter++)
            {
                (*file) << "\t" << dns_type_to_text((u_int16_t)(iter->first)) << "\t\t" << iter->second << "x" << endl;
            }

            (*file) << "Detection criteria (matched " << conditions_matched << "/" << TOTAL_CRITERIAS << "):" << endl;
            (*file) << out_stream.str();

            total_detections++;
            (*file) << "----------------------------" << endl;
        }

        if (stop)
            break;
    }

    ipmap.clear_all();
    stored_bins = 0;
}

/**
 * Put packet data into internal aggregation map for further analysis
 */
void pcap_detector1::put_ip_data_in_map(pkt *packet)
{
    if (packet)
    {
        aggreg_data data(packet->srcPort, packet->dstPort, packet->qName, 1, packet->ipLen, packet->pktTime, packet->qType, packet->rdLen, (packet->msgType == DNS_TYPE_QUERY) ? true : false);

        bool added = false;

        if (packet->ipv6)
            added = ipmap.add_flow_data(packet->srcIP.v6, packet->dstIP.v6, data);
        else
            added = ipmap.add_flow_data(packet->srcIP.v4.s_addr, packet->dstIP.v4.s_addr, data);

        total_pkts_analyzed++;
        if (! added)
            total_pkts_whitelisted++;
    }
}


/**
 * Free allocated bin memory.
 */
void pcap_detector1::delete_finished_bin()
{
    if (bin != NULL)
    {
        delete bin;
        bin = NULL;
    }
}

/**
 * Save summary message to shared obj.
 */
void pcap_detector1::save_summary()
{
    ostringstream str;
    str << "Total bins processed:      " << total_bins << endl;
    str << "Total packets processed:   " << total_pkts_processed << endl;
    str << "Total packets analyzed:    " << total_pkts_analyzed << endl;
    str << "Total packets whitelisted: " << total_pkts_whitelisted << endl;
    str << "Total detections:          " << total_detections << endl;
    str << endl;
    str << "Single packet thresholds usage statistics:" << endl;

    //print thresholds usage info
    str << "Single packet size:            " << thresh_stats[PCAP_THRESH_PACK_SIZE_POS] << endl;
    str << "DNS TTL:                       " << thresh_stats[PCAP_THRESH_TTL_POS] << endl;
    str << "DNS RDATA size:                " << thresh_stats[PCAP_THRESH_RDLEN_POS] << endl;
    str << "DNS Query size:                " << thresh_stats[PCAP_THRESH_QUERY_SIZE_POS] << endl;
    str << "DNS domain len:                " << thresh_stats[PCAP_THRESH_QNAME_LEN_POS] << endl;
    str << "Same length subdomains:        " << thresh_stats[PCAP_THRESH_SUBDOMAIN_LEN_POS] << endl;
    str << "Too much non-ASCII characters: " << thresh_stats[PCAP_THRESH_TOO_MUCH_ASCII_POS] << endl;

    shared_summary.set_detector1_msg(str.str());
}

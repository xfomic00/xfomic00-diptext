/**
 * pcapparser.cpp
 * This file implements methods of class pcap_parser.
 *
 * Based on (libpcap part):
 * http://code.google.com/p/pcapsctpspliter/issues/detail?id=6
 * https://github.com/uvic-sdo/DNS-Packet-Parser
 *
 * Based on (DNS part):
 * RFC 1035 (http://tools.ietf.org/html/rfc1035)
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    15.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <ctime>
#include <pcap.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>


#include "pcapparser.h"
#include "common.h"
#include "ip_storage.h"
#include "shared.h"

using namespace std;

/**
 * Constructor
 */
pcap_parser::pcap_parser(params *pars)
{
    if (pars)
    {
        parameters = pars;
        counter = 0;
        total_bins = 0;
        total_pkts_processed = 0;
        total_pkts_filtered = 0;
        bin_packets = 0;
        alloc_new_bin();

        //we wont use time to change bins
        if (parameters->bin_packets != 0)
            bin_end.set_time(100,100,100);
    }
    else
        bin = NULL;
}

/**
 * Destructor
 */
pcap_parser::~pcap_parser()
{
    if (bin)
        delete bin;
}

/**
 * Process input pcap file.
 * Parse packet headers and make detection...
 */
void pcap_parser::parse_infile()
{
    struct pcap_pkthdr pktheader;       //packet pcap header
    const u_char *packet;               //packet data
    pcap_t *pcapfile_opened;            //opened pcap struct
    char error_buff[PCAP_ERRBUF_SIZE];  //buffer for err msgs
    pkt pack;   //captured DNS packet structure

    bool was_first = false;

    ///open input pcap file
    pcapfile_opened = pcap_open_offline(parameters->infile.c_str(), error_buff);
    if (pcapfile_opened == NULL)
    {
        cerr << "Couldn't open input file " << parameters->infile << ": " << error_buff << endl;
        err_exit();
    }

    ///process input file
    while ((packet = pcap_next(pcapfile_opened, &pktheader)) != NULL) //get next packet
    {
        total_pkts_processed++;

        //parse packet fields
        if (parse_packet_fields(packet, &pack))
        {
            //only DNS packets here

            //parse packet capture time
            extract_pkt_time(&pktheader, &pack);

            if (parameters->bin_packets == 0 && ! was_first)  //first packet, determine first bin end time
            {
                bin_end = pack.pktTime;
                bin_end.add_time(parameters->bin_size_hrs, parameters->bin_size_mins, parameters->bin_size_secs);

                bin->set_start_time(pack.pktTime); //save bin times
                bin->set_end_time(bin_end);

                if (parameters->debug_level > DEBUG_LEVEL_NONE)
                {
                    mutex_cout.lock();
                    cout << "set bin end = " << bin_end.get_date_time_str() << endl;
                    mutex_cout.unlock();
                }

                total_bins++;
            }
            else //bin end time is known, check if bin switch is needed and do it (if needed)
                switch_bin(&(pack.pktTime));

            store_data_into_active_bin(&pack);    //save useful data into bin
            was_first = true;

            //print packet info if needed
            if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
            {
                mutex_cout.lock();
                print_pkt(&pack);
                mutex_cout.unlock();
            }
        }

        if (stop)
            break;
    }

    //print bin data if needed
    if (parameters->debug_level > DEBUG_LEVEL_EXTRA_INFO)
    {
        mutex_cout.lock();
        bin->print_data();
        mutex_cout.unlock();
    }

    pcap_close(pcapfile_opened);        //close input file

    push_finished_bin_to_shared();
    shared_bin_pointers.set_finished(true); //all loading finished
}

/**
 * Get next packet from opened pcap file.
 * Return true if packet was extracted, false otherwise.
 */
int pcap_parser::get_next_packet(pkt *pack, pcap_t *pcapfile_opened, struct pcap_pkthdr *pktheader)
{
    if (! pack || ! pcapfile_opened || ! pktheader)   //check params
        return false;

    const u_char *packet;               //packet data

    if ((packet = pcap_next(pcapfile_opened, pktheader)) != NULL) //get next packet
    {
        //parse packet fields
        if (parse_packet_fields(packet, pack))
        {
            //only DNS packets here

            //parse packet capture time
            extract_pkt_time(pktheader, pack);

            return RET_OK;
        }

        return RET_NOT_DNS_FLOW;
    }

    return RET_EMPTY;
}

/**
 * Start live capture of packets on specified interface.
 */
void pcap_parser::live_capture()
{
    pcap_t *handle;                 //interface handle for pcap functions
    char errmsg[PCAP_ERRBUF_SIZE];  //err msg buffer

    //open device
    handle = pcap_open_live(parameters->infile.c_str(), MAX_PKT_CAPTURE_SIZE, 0, 10, errmsg);
    if (handle == NULL)
    {
        cerr << "Error: couldn't open device " << parameters->infile << ": " << errmsg << endl;
        err_exit();
    }

    if (pcap_datalink(handle) != DLT_EN10MB)  //only ethernet headers (packets)
    {
        cerr << "Error: device " << parameters->infile << " doesn't provide Ethernet headers - not supported!" << endl;
        err_exit();
    }

    //DNS packets only - filter
    struct bpf_program fp;              //compiled filter expression
    char filter_str[] = "udp port 53";  //filter expression
    bpf_u_int32 mask = 0x0;             //netmask of sniffing device (= 0.0.0.0)
    //bpf_u_int32 net; //IP of sniffing device

    if (pcap_compile(handle, &fp, filter_str, 0, mask) != 0)   //compile filter
    {
        fprintf(stderr, "Error: couldn't parse filter %s: %s\n", filter_str, pcap_geterr(handle));
        err_exit();
    }
    if (pcap_setfilter(handle, &fp) != 0)  //set filter to capture session
    {
        fprintf(stderr, "Error: couldn't install filter %s: %s\n", filter_str, pcap_geterr(handle));
        err_exit();
    }

    pcap_freecode(&fp);

    live_arg_wrapper wrp;   //helper struct to pass params for parse_live_wrapper
    wrp.obj = this;

    opened_stuff_shared.handle = handle;    //save handle for catching signal and breaking the pcap_loop

    //live capture processing function is parse_live_wrapper, which routes it to parse_live_packet
    if (pcap_loop(handle, -1, pcap_parser::parse_live_wrapper, (u_char *)&wrp) == -1)
        cerr << "Error: error during packet capture!" << endl;

    //print bin data if needed
    if (parameters->debug_level > DEBUG_LEVEL_EXTRA_INFO)
    {
        mutex_cout.lock();
        bin->print_data();
        mutex_cout.unlock();
    }

    push_finished_bin_to_shared();
    shared_bin_pointers.set_finished(true); //all loading is finished
}

/**
 * Parse packet fields.
 * Returns true if it is DNS packet, false otherwise.
 */
bool pcap_parser::parse_packet_fields(const u_char *packet, pkt *pack)
{
    pkt_ptr = (u_char *)packet; //shared ptr to packet

    //parse ethernet header, grabbing the type field
    int eth_type = ((int)(pkt_ptr[12]) << 8) | (int)pkt_ptr[13];

    bool is_ipv6 = false;

    if (eth_type == ETHERNET_TYPE_IP)
        pkt_ptr += ETHERNET_HEADER_SIZE;
    else if (eth_type == ETHERNET_TYPE_IPV6)
    {
        pkt_ptr += ETHERNET_HEADER_SIZE;
        is_ipv6 = true;
    }
    else if (eth_type == ETHERNET_TYPE_DOT1Q)
        pkt_ptr += ETHERNET_DOT1Q_HEADER_SIZE;
    else
    {
        if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
        {
            mutex_cout.lock();

            if (eth_type == ETHERNET_TYPE_ARP)
                cout << dec << total_pkts_processed << ": ARP packet, skipping..." << endl;
            else
                cout << dec << total_pkts_processed << ": Different Ethernet type: " << hex << eth_type << ", skipping..." << dec << endl;

            mutex_cout.unlock();
        }

        return false;
    }

    pack->pktNum = total_pkts_processed; //packet number

    if (is_ipv6)
    {
        //parse IPv6 header
        struct ip6_hdr *ip_hdr = (struct ip6_hdr *)pkt_ptr;
        pack->ipLen = ip_hdr->ip6_ctlun.ip6_un1.ip6_un1_plen;
        pack->ipProtocol = ip_hdr->ip6_ctlun.ip6_un1.ip6_un1_nxt;
        pack->srcIP.v6 = ip_hdr->ip6_src;
        pack->dstIP.v6 = ip_hdr->ip6_dst;
        pack->ipv6 = true;

        pkt_ptr += IPV6_HEADER_SIZE;
    }
    else
    {
        //parse IPv4 header
        struct ip *ip_hdr = (struct ip *)pkt_ptr;
        pack->ipLen = ntohs(ip_hdr->ip_len);
        pack->ipProtocol = ip_hdr->ip_p;
        pack->srcIP.v4 = ip_hdr->ip_src;
        pack->dstIP.v4 = ip_hdr->ip_dst;
        pack->ipv6 = false;

        pkt_ptr += IP_HEADER_SIZE;
    }

    if (pack->ipProtocol != IPPROTO_UDP)    //check if next is UDP
    {
        if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
        {
            mutex_cout.lock();
            cout << dec << total_pkts_processed << ": Not UDP, skipping..." << endl;
            mutex_cout.unlock();
        }
        return false;
    }

    //parse UDP header
    struct udphdr *udp_hdr = (struct udphdr *)pkt_ptr;
    pack->srcPort = ntohs(udp_hdr->source);
    pack->dstPort = ntohs(udp_hdr->dest);

    //check if it is DNS packet (by port)
    if ((pack->srcPort != DNS_PORT) && (pack->dstPort != DNS_PORT))
    {
        if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
        {
            mutex_cout.lock();
            cout << dec << total_pkts_processed << ": Not DNS packet, skipping..." << endl;
            mutex_cout.unlock();
        }

        return false;
    }

    //parse DNS packet
    pkt_ptr += UDP_HEADER_SIZE;
    dns_header *dns_hdr = (dns_header *)pkt_ptr;

    //check if packet is iodine UDP RAW packet
    if (ntohs(dns_hdr->id) == 0x10D1 && ((ntohs(dns_hdr->flags) & 0xFF00) == 0x9E00))    //magic 0x10D19E
    {
        //make sure packet will be detected (fill fake info)
        pack->msgType = true;   //query
        pack->qType = QTYPE_NULL;
        pack->rdLen = pack->ipLen - 30; //cca
        pack->qCount = 1;
        pack->rCount = 1;
        pack->ttl = 1;
        pack->qName = "fakeDomainNameWhichIsLongEnoughToBeDetectedApprox100Characters.MaybeLittleMore.possible.iodineRawUdp.tunnel";
        pack->rData = "";
        return true;
    }

    bool qr = (ntohs(dns_hdr->flags) >> 15) ? false : true;    //query = true, response = false
    u_int8_t rcode = (u_int8_t) (ntohs(dns_hdr->flags) & 0x000F);
    pack->msgType = qr;
    pack->rCode = rcode;
    pack->qCount = ntohs(dns_hdr->qcount);
    pack->rCount = ntohs(dns_hdr->rcount);

    pkt_ptr += DNS_HEADER_SIZE;

    extract_dns_query_info(ntohs(dns_hdr->qcount), pack);   //query information

    if (! qr) //response
        extract_dns_response_info(ntohs(dns_hdr->rcount), pack);    //response information

    return true;
}

/**
 * Parse packet from live capture, provide detection.
 */
//void pcap_parser::parse_live_packet(u_char *arg, const pcap_pkthdr *pkthdr, const u_char *packet)
void pcap_parser::parse_live_packet(const pcap_pkthdr *pkthdr, const u_char *packet)
{
    pkt pack;   //captured DNS packet structure

    total_pkts_processed++;

    //parse packet fields
    if (parse_packet_fields(packet, &pack))
    {
        //parse packet capture time
        extract_pkt_time((pcap_pkthdr *)pkthdr, &pack);

        if (parameters->bin_packets == 0 && pack.pktNum == 1)  //first packet, determine first bin end time
        {
            bin_end = pack.pktTime;
            bin_end.add_time(parameters->bin_size_hrs, parameters->bin_size_mins, parameters->bin_size_secs);

            bin->set_start_time(pack.pktTime); //save bin times
            bin->set_end_time(bin_end);

            if (parameters->debug_level > DEBUG_LEVEL_NONE)
            {
                mutex_cout.lock();
                cout << "set bin end = " << bin_end.get_date_time_str() << endl;
                mutex_cout.unlock();
            }
        }
        else //bin end time is known, check if bin switch is needed and do it (if needed)
            switch_bin(&(pack.pktTime));

        store_data_into_active_bin(&pack);    //save useful data into bin

        //print packet info if needed
        if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
        {
            mutex_cout.lock();
            print_pkt(&pack);
            mutex_cout.unlock();
        }
    }
}

/**
 * Wrapper function for parse_live_packet (because function must be static, which is a problem for parse_live_packet).
 * In arg is passed pointer to object of pcap_detector1, by which is called parse_live_packet.
 */
void pcap_parser::parse_live_wrapper(u_char *arg, const pcap_pkthdr *pkthdr, const u_char *packet)
{
    live_arg_wrapper *wrp = (live_arg_wrapper *)arg;
    //wrp->obj->parse_live_packet(wrp->dumper, pkthdr, packet);
    wrp->obj->parse_live_packet(pkthdr, packet);
}


/**
 * Save flow data into active bin.
 */
void pcap_parser::store_data_into_active_bin(pkt *pack)
{
    if (pack)
    {
        //filter common query types (if configured)
        if (! (parameters->filter_common_qtypes && has_common_qtype(pack)))
        {
            bin->add_pkt(*pack);    //add whole packet
            total_pkts_filtered++;
        }

        bin_packets++;
    }
}

/**
 * Switch bin if needed;
 * Uses bin_end to determine bin ending time.
 * Returns true if bin was switched, false otherwise.
 */
bool pcap_parser::switch_bin(flow_time *flwtime)
{
    if ((parameters->bin_packets > 0 && bin_packets%(parameters->bin_packets) == 0) || (flwtime->compare_with(bin_end) >= 0))
    {
        if (parameters->debug_level > DEBUG_LEVEL_EXTRA_INFO)    //debug print bin data
        {
            mutex_cout.lock();
            bin->print_data();
            mutex_cout.unlock();
        }

        if (parameters->bin_packets > 0)    //set real bin end time
            bin->set_end_time(*flwtime);

        push_finished_bin_to_shared();
        alloc_new_bin();

        bin->set_start_time(bin_end);   //start time of new bin

        //set next bin end time
        if (parameters->bin_packets == 0)
            bin_end.add_time(parameters->bin_size_hrs, parameters->bin_size_mins, parameters->bin_size_secs);
        else
            bin_packets = 0;

        bin->set_end_time(bin_end);

        if (parameters->bin_packets == 0 && parameters->debug_level > DEBUG_LEVEL_NONE)
        {
            mutex_cout.lock();
            cout << "bin switch, next bin end = " << bin_end.get_date_time_str() << endl;
            mutex_cout.unlock();
        }

        total_bins++;

        return true;
    }

    return false;
}

/**
 * Extract packet capture date & time and save into pkt struct.
 */
void pcap_parser::extract_pkt_time(pcap_pkthdr *hdr, pkt *pack)
{
    time_t t = hdr->ts.tv_sec;
    struct tm *time = localtime(&t);

    pack->pktTime.set_date((u_int16_t)(time->tm_year + 1900), (u_int8_t)(time->tm_mon + 1), (u_int8_t)time->tm_mday);
    pack->pktTime.set_time((u_int8_t)(time->tm_hour), (u_int8_t)(time->tm_min), (u_int8_t)(time->tm_sec));
}

/**
 * Extract needed information from DNS query section
 */
void pcap_parser::extract_dns_query_info(int qcount, struct pkt *pack)
{
    for (int i=0; i<qcount; i++)    //move pointer through all queries
    {
        string domain = extract_domain_name_by_labels();

        if (i == 0) //save only first query info
        {
            pack->qName = domain;

            dns_question *dns_quest = (dns_question *)pkt_ptr;  //remaining fields
            pack->qType = ntohs(dns_quest->type);
        }

        pkt_ptr += DNS_QUESTION_SIZE; 
    }

    pack->rData.clear();
    pack->ttl = 0;
    pack->rdLen = 0;
}

/**
 * Extract needed information from DNS response section
 */
void pcap_parser::extract_dns_response_info(int rcount, pkt *pack)
{
    for (int i=0; i<rcount; i++)    //all responses
    {
        //NAME
        //check if name is compressed pointer or labelled name and skip it
        u_int8_t byte = (u_int8_t) *pkt_ptr;
        if (byte & 0xC0)
            pkt_ptr += 2;   //pointer
        else
            skip_name();    //labelled name

        //remaining fields
        dns_rr *dns_respr = (dns_rr *)pkt_ptr;
        pkt_ptr += DNS_RR_SIZE;

        if (i == 0) pack->qType = ntohs(dns_respr->type);
        //int cls = (int) ntohs(dns_respr->cls);
        if (i == 0) pack->ttl = ntohl(dns_respr->ttl);
        if (i == 0) pack->rdLen = ntohs(dns_respr->rdlen);

        //process RR RDATA:
        string respstr = process_rr_rdata(pack->qType, pack->rdLen);
        if (i == 0) pack->rData = respstr;
    }
}

/**
 * Extract domain name from RRs by moving through labels.
 * (helper function)
 */
string pcap_parser::extract_domain_name_by_labels()
{
    /*  RFC 1035:
        A domain name represented as a sequence of labels, where
        each label consists of a length octet followed by that
        number of octets.  The domain name terminates with the
        zero length octet for the null label of the root.  Note
        that this field may be an odd number of octets; no
        padding is used.
    */

    string dname = "";  //final domain name

    //response
    u_int8_t length = (u_int8_t) *pkt_ptr;
    if (length & 0xC0)  //compressed labels dont get processed
        return dname;

    pkt_ptr++;

    while (length > 0)  //move through labels
    {
        for(int j=0; j<length; j++, pkt_ptr++)
        {
            char ch = (char) *pkt_ptr;
            dname += ch;
        }

        length = (u_int8_t) *pkt_ptr;
        if (length & 0xC0)  //compressed labels dont get processed
            return dname;

        pkt_ptr++;

        if (length > 0)
            dname += '.';  //section
    }

    return dname;
}

/**
 * Parse rdata of RR by type.
 */
string pcap_parser::process_rr_rdata(u_int16_t type, u_int16_t datalen)
{
    string ret = "";
    char buff[DNS_RDATA_MAX_SIZE];

    if (datalen >= DNS_RDATA_MAX_SIZE)
        datalen = DNS_RDATA_MAX_SIZE - 1;

    //parse only selected RR types
    switch (type)
    {
        case QTYPE_A:
            ret = inet_ntoa(*((in_addr *)pkt_ptr));
            break;

        case QTYPE_AAAA:
            ret = get_ipv6_str(*((in6_addr *)pkt_ptr));
            break;

        case QTYPE_CNAME:
        case QTYPE_NS:
            ret = extract_domain_name_by_labels();
            break;

        case QTYPE_MX:
            //u_int16_t preference = ntohs(*((u_int16_t *) pkt_ptr));
            pkt_ptr += 2;   //skip PREFERENCE field (16b)
            ret = extract_domain_name_by_labels();
            break;

        case QTYPE_TXT:
            pkt_ptr++;
            datalen--;
        case QTYPE_NULL:
            strncpy(buff, (char *)pkt_ptr, datalen);
            buff[datalen] = 0;
            ret = string(buff);
            break;
    }

    return ret;
}

/**
 * Move shared pointer after labelled name section
 */
void pcap_parser::skip_name()
{
    u_int8_t length = (u_int8_t) *pkt_ptr;
    pkt_ptr++;

    while (length > 0)  //move through labels
    {
        pkt_ptr += length;

        length = (u_int8_t) *pkt_ptr;
        pkt_ptr++;
    }
}

/**
 * Allocate new bin and change internal pointer.
 */
void pcap_parser::alloc_new_bin()
{
    bin = new ip_storage;
}

/**
 * Pass pointer to current bin to global shared queue.
 */
void pcap_parser::push_finished_bin_to_shared()
{
    shared_bin_pointers.add_data(bin);
    bin = NULL;
}

/**
 * Print packet information (debug function).
 */
void pcap_parser::print_pkt(pkt *pack)
{
    if (pack)
    {
        cout << pack->pktNum << ": " << pack->pktTime.get_date_time_str() << ", len=" << pack->ipLen << ", " << get_srcIP_str(pack);
        cout << " -> " << get_dstIP_str(pack) << ", sp: " << pack->srcPort << ", dp: " << pack->dstPort;

        if (parameters->debug_level > DEBUG_LEVEL_EXTENDED)
        {
            cout << ", " << ((pack->msgType) ? "query" : "response");
            //cout << ", questions=" << pack->qCount << ", responses=" << pack->rCount;

            if (pack->qType == DNS_TYPE_QUERY)
                cout << ", qry: " << pack->qName << ", t: " << dns_type_to_text(pack->qType) << endl;
            else
                cout << ", qry: " << pack->qName << ", t: " << dns_type_to_text(pack->qType) << ", resp: " << pack->rData << endl;
        }
        else
            cout << endl;
    }
}

/**
 * Save summary message to shared obj.
 */
void pcap_parser::save_summary()
{
    ostringstream str;
    str << "Total packets processed: " << total_pkts_processed << endl;
    str << "Total packets filtered:  " << total_pkts_filtered << endl;
    str << "Total bins created:      " << total_bins << endl;

    shared_summary.set_parser_msg(str.str());
}


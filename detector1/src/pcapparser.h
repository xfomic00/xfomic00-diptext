/**
 * pcapparser.h
 * This file defines class pcap_parser.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    15.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <pcap.h>
#include <stdlib.h>
#include <string>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include "common.h"
#include "ip_storage.h"

using namespace std;

#ifndef _PCAPPARSER_H_
#define _PCAPPARSER_H_

/////////////////////////////////////////////// CONSTANTS

#define MAX_PKT_CAPTURE_SIZE 2000 //max size of captured packets [B]


/////////////////////////////////////////////// CLASSES

/**
 * pcap_parser - class for parsing pcap files or live capture.
 */
class pcap_parser
{
  public:
    pcap_parser(params *pars);   //constructor
    ~pcap_parser();      //destructor

    void parse_infile();    //process input file
    int get_next_packet(pkt *pack, pcap_t *pcapfile_opened, struct pcap_pkthdr *pktheader);  //get next packet
    void live_capture();    //capture packets from interface
    bool parse_packet_fields(const u_char *packet, pkt *pack);   //parse packet fields (headers)
    void parse_live_packet(const struct pcap_pkthdr* pkthdr, const u_char * packet);
    static void parse_live_wrapper(u_char *arg, const struct pcap_pkthdr* pkthdr, const u_char * packet);

    void store_data_into_active_bin(pkt *pack); //save packet data into bin
    bool switch_bin(flow_time *flwtime);        //switch bin if needed

    void extract_pkt_time(struct pcap_pkthdr *hdr, struct pkt *pack);   //save packet capture time
    void extract_dns_query_info(int qcount, struct pkt *pack);      //save query info
    void extract_dns_response_info(int rcount, struct pkt *pack);   //save response info

    string process_rr_rdata(u_int16_t type, u_int16_t datalen);   //parse rdata TODO
    string extract_domain_name_by_labels(); //get domain name (helper function)

    void alloc_new_bin();   //allocate new bin and change internal pointer to it
    void push_finished_bin_to_shared(); //pass pointer to current bin to shared queue

    void skip_name();       //move pointer after labeled name
    void print_pkt(struct pkt *pack);       //print packet info (debug function)

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments
    long counter;       //packet counter
    unsigned long total_bins;               //total bins created
    unsigned long long total_pkts_processed;//total packets processed
    unsigned long long total_pkts_filtered; //total DNS packets

    u_char *pkt_ptr;    //shared pointer to actual position in packet

    flow_time bin_end;  //actual bin end time
    unsigned int bin_packets; //packets stored in actual bin

    ip_storage *bin;    //active bin pointer
};

//helper struct for parse_live_wrapper
struct live_arg_wrapper
{
    //u_char *dumper;
    pcap_parser *obj;
};

#endif

/**
 * ipfixparser.h
 * This file defines class ipfix_parser.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    18.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
#include "ip_storage.h"


using namespace std;

#ifndef _IPFIXPARSER_H_
#define _IPFIXPARSER_H_

/////////////////////////////////////////////// CONSTANTS

//struct for saving positions of CSV columns
typedef struct
{
    unsigned int DST_IP;
    unsigned int SRC_IP;
    unsigned int BYTES;
    unsigned int TIME_FIRST;
    unsigned int TIME_LAST;
    unsigned int DNS_RR_TTL;
    unsigned int PACKETS;
    unsigned int DNS_ANSWERS;
    unsigned int DNS_QTYPE;
    unsigned int DNS_RLENGTH;
    unsigned int DST_PORT;
    unsigned int SRC_PORT;
    unsigned int DNS_RCODE;
    unsigned int DNS_NAME;
    unsigned int DNS_RDATA;
} ipfix_csv_header;

/////////////////////////////////////////////// CLASSES

/**
 * ipfix_parser - class for parsing IPFIX csv files created by "DNS plugin" for INVEA-TECH's Flowmon Exporter.
 */
class ipfix_parser
{
  public:
    ipfix_parser(params *par);  //constructor
    ~ipfix_parser();

    void get_file_list();   //get list of all input files

    ipfix_flow *get_next_flow(int *err, bool parse_only_statistics);  //read and parse next flow record

    void fill_header(CSVRow *row);    //save column indexes to header struct

    bool parse_ipfix_flow_csv_fields(CSVRow *flowdata, ipfix_flow *flwdest, bool parse_only_statistics);    //parse readed flow data into flow struct
    string strip_quotes(string str);  //strip quote characters from str

    int open_infile();      //open next input file
    void close_infile();    //close opened input file

  private:
    params *parameters; //program arguments
    bool first_done;    //if first line of file has been skipped
    unsigned long long counter;        //flow counter

    ipfix_flow flw;   //flow record

    ipfix_csv_header header;    //CSV header

    vector<string> infiles; //list of all input files
    ifstream infile_csv;    //input file (csv format)
};

/**
 * ipfix_bin_maker - class for organising IPFIX data into bins.
 */
class ipfix_bin_maker
{
  public:
    ipfix_bin_maker(params *pars);   //constructor
    ~ipfix_bin_maker();   //destructor

    void parse_infiles();    //process input files

    static void print_flow(ipfix_flow *flw);       //print flow info (debug function)
    static void save_to_file(ofstream *file, ipfix_flow *flw);   //save flow info into file

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments

    unsigned long total_bins;               //total bins created
    unsigned long long total_flws_processed;//total packets processed
    unsigned long long total_flws_filtered; //total DNS packets
};

#endif

/**
 * main.cpp
 * This file implements main function.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    13.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <pcap.h>
#include <thread>
#include <mutex>

#include "common.h"
#include "pcapdetector1.h"
#include "pcapparser.h"
#include "netflowdetector1.h"
#include "netflowdetector2.h"
#include "netflowparser.h"
#include "netflowstatisticsmaker.h"
#include "ipfixparser.h"
#include "ipfixdetector2.h"
#include "libnfdump.h"
#include "shared.h"

using namespace std;

const char usage_msg[] =
    "detector: Provides DNS tunneling packets detection (selection) from specified file/interface. "
    "Detected tunnels are then saved to file provided by -o argument. Supported files are pcap (-p), netflow csv (-n) (using nfdump -o csv) and "
    "netflow nfcapd binary file (-d). Live capture of packets from interface is also possible (-e).\n\n"
    "Usage:\n"
    "./detector params\n"
    "params:\n"
    "\t-i <input_file> - input file\n"
    "\t-r <inputs_folder> - folder with input files\n"
    "\t-o <output_file> - output file\n"
    "\t-p - input is pcap file\n"
    "\t-e - capture and analyze live packets on interface specified by -i <interface_name>. Program must run under root (e.g. sudo ./detector ...)!\n"
    "\t-n - input is NetFlow csv file(s) (nfdump -o csv)\n"
    "\t-d - input is NetFlow binary file(s) (captured by nfcapd)\n"
    "\t-x - input is IPFIX csv file(s) (created by DNS-plugin for INVEA-TECH's Flowmon Exporter (see README)).\n"
    "\t-t <stats_file> - daily NetFlow/IPFIX statistics file created with -s.\n"
    "\t-s - create daily statistics for NetFlow/IPFIX detector (from input data). Flow type (-n/-d/-x), inputs (-i/-r) and output (-o) must be specified!\n"
    "\t-c <config_file> - configuration file with parameters for detection. If not specified, default configuration is used (see -m parameter).\n"
    "\t-m <config_file> - create new config file with default configuration.\n"
    "\t-w <whitelist_file> - whitelist file with domains which will not be detected as tunnels.\n"
    "\t-l <whitelist_file> - create new whitelist file with default configuration.\n"
    "\t-D <integer> - debug level (prints packet info to stdout). Higher level means more info.\n"
    "\t\t-D 1 prints information about bins (creation, detection, ...)\n"
    "\t\t-D 2 prints packet/flow information - IPs, ports, ...\n"
    "\t\t-D 3 prints packet/flow extended info - DNS queries etc.\n"
    "\t\t-D 4 prints extended info & bin data just before bin switch\n"
    "\t-h - prints this help message\n";

////////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES

//shared var with pointers to opened files for signal handling
opened_stuff opened_stuff_shared;

//shared queue with pointers to bins (for inter-thread data sharing between parser and detector1)
shared_queue_ip_storage shared_bin_pointers;

//bins shared between detector1 and detector2
shared_queue_ip_storage shared_bin_pointers_filtered_by_d1;

//shared processing summary msgs
shared_detectors_summary shared_summary;

//shared program arguments
params shared_parameters;

//shared mutex for locking cout when printing logs
mutex mutex_cout;

//stop all actions after signal catch
bool stop = false;

//////////////////////////////////////////////////////////////////////////

/**
 * Catch signals and free all stuff befor exiting program...
 */
void catch_signal(int sig)
{
    if (sig == SIGINT || sig == SIGTERM)
    {
        stop = true;
        shared_bin_pointers.set_finished(true);
        shared_bin_pointers_filtered_by_d1.set_finished(true);

        if (opened_stuff_shared.handle)
            pcap_breakloop(opened_stuff_shared.handle); //kill packet catching loop only
        /*
		else
        {
            //close opened files

            if (opened_stuff_shared.pcap_opened_file)
                pcap_close(opened_stuff_shared.pcap_opened_file);
            if (opened_stuff_shared.opened_infile && opened_stuff_shared.opened_infile->is_open())
                opened_stuff_shared.opened_infile->close();
            if (opened_stuff_shared.opened_outfile && opened_stuff_shared.opened_outfile->is_open())
                opened_stuff_shared.opened_outfile->close();
            if (opened_stuff_shared.opened_nfdump_file)
                nfdump_iter_end(opened_stuff_shared.opened_nfdump_file);

            exit(EXIT_FAILURE);
        }
        */
    }
}

/**
 * Append summary information from modules into output file.
 */
void write_summary()
{
    ofstream outfile(shared_parameters.outfile, ios_base::app | ios_base::out);  //open output file for append
    if (! outfile.is_open())
    {
        cerr << "Couldn't open output file " << shared_parameters.outfile << "!" << endl;
        err_exit();
    }
    else
    {
        shared_summary.write_msgs_to_file(&outfile);
        outfile.close();
    }
}

/**
 * Parse program arguments.
 */
params parse_params(int argc, char **argv)
{
    params p;

    int param = 0;
    opterr = 0;     //disable implicit error msgs
    bool err = false;
    bool was_m = false;

    if (argc == 2 && (strcmp(argv[1], "-h")==0 || strcmp(argv[1], "--help")==0))
    {
        cerr << usage_msg;
        exit(0);
    }

    //parse args
    while((param = getopt(argc, argv, "i:o:c:D:m:r:t:w:l:pndexs")) != EOF)
    {
        string s = "";
        if (optarg)
            s = string(optarg);


        switch(param)
        {
            case 'i':
                //p.infile(optarg);
                p.infile = s;
                if (p.indir.length() > 0)
                    err_exit_with_msg("Error: cant use both parameters -i, -r!");
                break;
            case 'o':
                p.outfile = s;
                break;
            case 'r':
                p.indir = s;
                if (p.infile.length() > 0)
                    err_exit_with_msg("Error: cant use both parameters -i, -r!");
                break;
            case 't':
                p.stats_file = s;
                break;
            case 'c':
                if (! parse_config_file(optarg, &p))
                    err_exit();
                break;
            case 'w':
                if (! parse_whitelist_file(optarg, &p))
                    err_exit();
                break;
            case 'D':
                p.debug_level = get_int_from_str((const char *)optarg, &err);
                if (err || p.debug_level < 0)
                    err_exit_with_msg("Error: Expected positive int value after param \"-D\".");
                break;
            case 'p':
                if (p.filetype != '-')
                    err_exit_with_msg("Error: You can select only one file format (-p, -d, -n, -e, -x, but not combination of these)!");
                p.filetype = 'p';
                break;
            case 'n':
                if (p.filetype != '-')
                    err_exit_with_msg("Error: You can select only one file format (-p, -d, -n, -e, -x, but not combination of these)!");
                p.filetype = 'n';
                break;
            case 'd':
                if (p.filetype != '-')
                    err_exit_with_msg("Error: You can select only one file format (-p, -d, -n, -e, -x, but not combination of these)!");
                p.filetype = 'd';
                break;
            case 'e':
                if (p.filetype != '-')
                    err_exit_with_msg("Error: You can select only one file format (-p, -d, -n, -e, -x, but not combination of these)!");
                p.filetype = 'e';
                break;
            case 'x':
                if (p.filetype != '-')
                    err_exit_with_msg("Error: You can select only one file format (-p, -d, -n, -e, -x, but not combination of these)!");
                p.filetype = 'x';
                break;
            case 'm':
                create_config_file(optarg);
                was_m = true;
                break;
            case 'l':
                create_whitelist_file(optarg);
                was_m = true;
            case 's':
                p.s_param = true;
                break;

            default:
                err_exit_with_msg("Error: Unknown parameter! Use \"-h\" for help.");
        }
    }

    if (optind < argc)
        err_exit_with_msg("Error: Unknown parameter! Use \"-h\" for help");

    if (! was_m && (p.infile.empty() || p.outfile.empty()))
        err_exit_with_msg("Error: Input or output file not specified! Use -i, -o!");

    if (! was_m && p.filetype == '-')
        err_exit_with_msg("Error: Input file type not specified! Use -p, -e, -n, -d, or -x!");

    if (p.s_param && ((p.infile.empty() && p.indir.empty()) || p.outfile.empty() || (p.filetype != 'n' && p.filetype != 'd' && p.filetype != 'x')))
        err_exit_with_msg("Error: Missing needed parameters!");

    if (p.bin_packets > 0 && (p.filetype == 'n' || p.filetype == 'd' || p.filetype == 'x') && p.d1_detection_schema != D1_DETECTION_SCHEMA_BIN_THRESHOLDS)
        err_exit_with_msg("Error: Can't use bin_packets settings with statistics-based detection schema!");

    return p;
}

////////////////////////////////////////////////////////////////////////// THREAD MAINS

/**
 * Main for netflow parsing and bin creating thread.
 * This thread will parse input netflow files into bins and
 * save them into shared queue.
 */
void thread_run_netflow_loading()
{
    netflow_bin_maker bin_maker(&shared_parameters);
    bin_maker.parse_infile();
    bin_maker.save_summary();

    mutex_cout.lock();
    cerr << "PARSER DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for netflow detector 1 (DNS tunnel detection).
 * This thread will act as detector 1 for netflow based DNS tunnel detection (simple detection).
 * Detector reads bins created by loading thread from shared queue.
 */
void thread_run_netflow_detector1()
{
    netflow_detector1 detector1(&shared_parameters);
    detector1.process_queue();
    detector1.save_summary();

    mutex_cout.lock();
    cerr << "DETECTOR1 DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for netflow detector 2 (DNS tunnel detection).
 * This thread will act as detector 2 for netflow based DNS tunnel detection (advanced detection).
 * Detector reads bins created by netflow detector 1 from shared queue.
 */
void thread_run_netflow_detector2()
{
    netflow_detector2 detector2(&shared_parameters);
    detector2.process_queue();
    detector2.save_summary();

    mutex_cout.lock();
    cerr << "DETECTOR2 DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for pcap parsing and bin creating thread (from live capture or file).
 * This thread will read and parse packets into bins and
 * save them into shared queue.
 */
void thread_run_pcap_loading()
{
    pcap_parser parser(&shared_parameters);
    if (shared_parameters.filetype == 'p')  //pcap file
        parser.parse_infile();
    else
        parser.live_capture();  //live capture

    parser.save_summary();

    mutex_cout.lock();
    cerr << "PARSER DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for pcap detector 1 (DNS tunnel detection).
 * This thread will act as detector 1 for pcap based DNS tunnel detection.
 * Detector reads bins created by loading thread from shared queue.
 */
void thread_run_pcap_detector1()
{
    pcap_detector1 detector(&shared_parameters);
    detector.process_queue();
    detector.save_summary();

    mutex_cout.lock();
    cerr << "DETECTOR1 DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for IPFIX parsing and bin creating thread (from csv file).
 * This thread will read and parse packets into bins and
 * save them into shared queue.
 */
void thread_run_ipfix_loading()
{
    ipfix_bin_maker bin_maker(&shared_parameters);
    bin_maker.parse_infiles();
    bin_maker.save_summary();

    mutex_cout.lock();
    cerr << "PARSER DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for IPFIX detector 1 (DNS tunnel detection).
 * This thread will act as IPFIX detector 2 for DNS tunnel detection (advanced detection methods).
 * Detector reads bins created by detector 1 from shared queue.
 */
void thread_run_ipfix_detector2()
{
    ipfix_detector2 detector2(&shared_parameters);
    detector2.process_queue();
    detector2.save_summary();

    mutex_cout.lock();
    cerr << "DETECTOR2 DONE" << endl;
    mutex_cout.unlock();
}

////////////////////////////////////////////////////////////////////////// PROGRAM MAIN

/**
 * Main - parse args & stuff
 */
int main(int argc, char *argv[])
{
    //check params
    shared_parameters = parse_params(argc, argv);

    //set signal catching function
    signal(SIGINT, catch_signal);

    if (shared_parameters.s_param)    //create daily statistic data for NetFlow/IPFIX detector
    {
        netflow_statistics_maker stats(&shared_parameters);
        if (shared_parameters.filetype == 'n' || shared_parameters.filetype == 'd') //netflow
            stats.create_netflow_statistics();
        else if (shared_parameters.filetype == 'x') //ipfix
            stats.create_ipfix_statistics();

        return 0;
    }
    else if (shared_parameters.filetype == 'p' || shared_parameters.filetype == 'e')    //pcap based detection
    {
        thread t_loading(thread_run_pcap_loading);        //create pcap loading thread
        thread t_detector1(thread_run_pcap_detector1);    //create pcap detector 1 thread
        t_loading.join();
        t_detector1.join();

        //write summary to outfile and close it
        write_summary();
    }
    else if (shared_parameters.filetype == 'n' || shared_parameters.filetype == 'd')   //netflow based detection
    {
        thread t_loading(thread_run_netflow_loading);       //create netflow loading thread
        thread t_detector1(thread_run_netflow_detector1);   //create netflow detector 1 thread
        thread t_detector2(thread_run_netflow_detector2);   //create netflow detector 2 thread
        t_loading.join();
        t_detector1.join();
        t_detector2.join();

        //write summary to outfile and close it
        write_summary();
    }
    else if (shared_parameters.filetype == 'x') //IPFIX based detection
    {
        thread t_loading(thread_run_ipfix_loading);         //create ipfix loading thread
        thread t_detector1(thread_run_netflow_detector1);   //detector 1 thread - we can use netflow detector
        thread t_detector2(thread_run_ipfix_detector2);     //detector 2 thread
        t_loading.join();
        t_detector1.join();
        t_detector2.join();

        //write summary to outfile and close it
        write_summary();
    }

    return 0;
}

/**
 * ipfixparser.cpp
 * This file implements methods of class ipfix_parser.
 *
 * IPFIX csv files are created by "DNS plugin" for INVEA-TECH’s Flowmon Exporter.
 * DNS plugin can be obtained here:
 * https://www.liberouter.org/technologies/dns-plugin/
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    18.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "ipfixparser.h"
#include "common.h"
#include "ip_storage.h"
#include "netflowstatistics.h"
#include "netflowstatisticsmaker.h"
#include "shared.h"
#include "binsorter.h"

using namespace std;

////////////////////////////////////////////////////////////// IPFIX_PARSER METHODS

/**
 * Constructor
 */
ipfix_parser::ipfix_parser(params *par)
{
    parameters = par;
    first_done = false;
    counter = 0;
    get_file_list();
}

/**
 * Destructor
 */
ipfix_parser::~ipfix_parser()
{
}

/**
 * Get list of all input files (fill infiles vector).
 */
void ipfix_parser::get_file_list()
{
    if (! parameters->infile.empty())
        infiles.push_back(parameters->infile);
    else if (! parameters->indir.empty())
    {
        //get list of files in specified directory
        string folder(parameters->indir);

        //erase last /
        if (folder.at(folder.length()-1) == '/')
            folder.erase(folder.length()-1);

        //cmd (find folder/ -type f)
        string cmd = "find ";
        cmd += folder;
        cmd += "/ -type f";

        //run cmd and parse output
        string sysout = system_cmd(cmd.c_str());

        //parse output
        size_t pos = 0;
        string line;
        string delim = "\n";

        while ((pos = sysout.find(delim)) != string::npos)
        {
            line = sysout.substr(0, pos);
            infiles.push_back(line);  //add to file list
            sysout.erase(0, pos + delim.length());
        }
    }
}

/**
 * Parse next flow record from input file.
 * If parse_only_statistics switch is true, then only statistically important data will be parsed.
 * Returns pointer to parsed data, or NULL (why data couldnt be parsed is specified in err variable).
 */
ipfix_flow *ipfix_parser::get_next_flow(int *err, bool parse_only_statistics)
{
    bool file_done = false;
    CSVRow row; //flow data

    if(infile_csv >> row)   //load next row
    {
        if (! first_done)   //skip first line (header)
        {
            fill_header(&row);  //parse header
            first_done = true;
            *err = RET_NOT_DNS_FLOW;
            return NULL;
        }

        if (row.size() < NFDUMP_CSV_COLS_MAX)   //skip lines with summary
            file_done = true;
        else
        {
            if (parse_ipfix_flow_csv_fields(&row, &flw, parse_only_statistics))  //parse flow into flow record
            {
                *err = RET_OK;
                return &flw;    //only DNS flows here
            }
            else
                *err = RET_NOT_DNS_FLOW;
        }
    }
    else
        file_done = true;

    if (file_done)
    {
        //open new file
        int ret = open_infile();
        *err = ret;
    }

    return NULL;
}

/**
 * Save column indexes from CSV header
 */
void ipfix_parser::fill_header(CSVRow *row)
{
    for (unsigned int i=0; i<row->size(); i++)
    {
        if ((*row)[i].compare("DST_IP") == 0)
            header.DST_IP = i;
        else if ((*row)[i].compare("SRC_IP") == 0)
            header.SRC_IP = i;
        else if ((*row)[i].compare("BYTES") == 0)
            header.BYTES = i;
        else if ((*row)[i].compare("TIME_FIRST") == 0)
            header.TIME_FIRST = i;
        else if ((*row)[i].compare("TIME_LAST") == 0)
            header.TIME_LAST = i;
        else if ((*row)[i].compare("DNS_RR_TTL") == 0)
            header.DNS_RR_TTL = i;
        else if ((*row)[i].compare("PACKETS") == 0)
            header.PACKETS = i;
        else if ((*row)[i].compare("DNS_ANSWERS") == 0)
            header.DNS_ANSWERS = i;
        else if ((*row)[i].compare("DNS_QTYPE") == 0)
            header.DNS_QTYPE = i;
        else if ((*row)[i].compare("DNS_RLENGTH") == 0)
            header.DNS_RLENGTH = i;
        else if ((*row)[i].compare("DST_PORT") == 0)
            header.DST_PORT = i;
        else if ((*row)[i].compare("SRC_PORT") == 0)
            header.SRC_PORT = i;
        else if ((*row)[i].compare("DNS_RCODE") == 0)
            header.DNS_RCODE = i;
        else if ((*row)[i].compare("DNS_NAME") == 0)
            header.DNS_NAME = i;
        else if ((*row)[i].compare("DNS_RDATA") == 0)
            header.DNS_RDATA = i;
    }
}

/**
 * Parse flow fields into "flwdest".
 * If parse_only_statistics switch is true, then only statistically important data will be parsed.
 * Returns true if it is DNS flow, false otherwise.
 */
bool ipfix_parser::parse_ipfix_flow_csv_fields(CSVRow *flowdata, ipfix_flow *flwdest, bool parse_only_statistics)
{
    counter++;

    //skip not-flow lines
    if (flowdata->size() < IPFIX_CSV_COLS_MAX)
        return false;

    flwdest->flowNum = counter;

    //time (flow end time)
    flwdest->flowTime.parse_ipfix_time((*flowdata)[header.TIME_LAST]);

    //IP header
    if (! parse_only_statistics)
    {
        if (! inet_pton(AF_INET, (*flowdata)[header.SRC_IP].c_str(), &(flwdest->srcIP.v4)))
        {
            if (! inet_pton(AF_INET6, (*flowdata)[header.SRC_IP].c_str(), &(flwdest->srcIP.v6)))
                return false;
            else
                flwdest->ipv6 = true;
        }
        else
            flwdest->ipv6 = false;

        if (! inet_pton(AF_INET, (*flowdata)[header.DST_IP].c_str(), &(flwdest->dstIP.v4)))
        {
            if (! inet_pton(AF_INET6, (*flowdata)[header.DST_IP].c_str(), &(flwdest->dstIP.v6)))
                return false;
        }
    }

    //UDP header
    bool err = false;
    flwdest->srcPort = (u_int16_t) get_int_from_str((*flowdata)[header.SRC_PORT].c_str(), &err);
    flwdest->dstPort = (u_int16_t) get_int_from_str((*flowdata)[header.DST_PORT].c_str(), &err);

    if (flwdest->srcPort != DNS_PORT && flwdest->dstPort != DNS_PORT)   //not a DNS packet
        return false;

    //other
    int packets = get_int_from_str((*flowdata)[header.PACKETS].c_str(), &err);
    int bytes = get_int_from_str((*flowdata)[header.BYTES].c_str(), &err);

    if (err)    //check error during str to int conversions
        return false;

    flwdest->packets = (u_int16_t) packets;
    flwdest->bytes = (u_int32_t) bytes;

    //DNS data
    if (! parse_only_statistics)
    {
        int rcount = get_int_from_str((*flowdata)[header.DNS_ANSWERS].c_str(), &err);
        int rlen = get_int_from_str((*flowdata)[header.DNS_RLENGTH].c_str(), &err);
        int rcode = get_int_from_str((*flowdata)[header.DNS_RCODE].c_str(), &err);
        int ttl = get_int_from_str((*flowdata)[header.DNS_RR_TTL].c_str(), &err);
        int qtype = get_int_from_str((*flowdata)[header.DNS_QTYPE].c_str(), &err);

        if (err)    //error during conversions
            return false;

        flwdest->rCount = (u_int16_t) rcount;
        flwdest->rdLen = (u_int16_t) rlen;
        flwdest->rCode = (u_int8_t) rcode;
        flwdest->ttl = (u_int32_t) ttl;
        flwdest->qType = (u_int16_t) qtype;

        flwdest->qName = strip_quotes((*flowdata)[header.DNS_NAME]);
        flwdest->rData = strip_quotes((*flowdata)[header.DNS_RDATA]);

        if (flwdest->rData.compare(IPFIX_QUERY_STR)==0)
            flwdest->msgType = DNS_TYPE_QUERY;
        else
            flwdest->msgType = DNS_TYPE_RESPONSE;
    }

    return true;
}

/**
 * Strip quote characters from str (first and last character).
 */
string ipfix_parser::strip_quotes(string str)
{
    try
    {
        if (str.length() > 2)
        {
            str.erase(0, 1);
            str.erase(str.size() - 1);
        }
    }
    catch(const std::out_of_range& e)
    {
        cerr << "in strip_quotes: " << str << endl;
    }

    return str;
}

/**
 * Open next input csv file.
 * Returns code based on result of done actions.
 */
int ipfix_parser::open_infile()
{
    close_infile(); //close previously opened file

    if (infiles.size() == 0)    //all files finished
        return RET_EMPTY;

    infile_csv.open(infiles[0]);
    if (! infile_csv.good())
    {
        cerr << "Couldn't open input file " << infiles[0] << "!" << endl;
        return RET_ERROR;
    }

    opened_stuff_shared.opened_infile = &infile_csv;

    infiles.erase(infiles.begin());   //first file is done
    first_done = false;

    return RET_OK;
}

/**
 * Close opened file.
 */
void ipfix_parser::close_infile()
{
    if (infile_csv.is_open())
    {
        infile_csv.close();
        opened_stuff_shared.opened_infile = NULL;
    }
}

////////////////////////////////////////////////////////////// IPFIX_BIN_MAKER METHODS

/**
 * Constructor
 */
ipfix_bin_maker::ipfix_bin_maker(params *pars)
{
    parameters = pars;
    total_bins = 0;
    total_flws_processed = 0;
    total_flws_filtered = 0;
}

/**
 * Destructor
 */
ipfix_bin_maker::~ipfix_bin_maker()
{
}

/**
 * Parse input file.
 */
void ipfix_bin_maker::parse_infiles()
{
    int retcode;
    ipfix_parser parser(parameters);
    retcode = parser.open_infile(); //open first file, rest is managed by parser itself
    if (retcode != RET_OK)
        err_exit();

    bin_sorter sorter(parameters);  //for sorting flows by end time

    ipfix_flow *flw;   //flow record

    do  //read flow records
    {
        flw = parser.get_next_flow(&retcode, false);
        if (flw && retcode == RET_OK)   //DNS flow
        {
            if (! (parameters->filter_common_qtypes && has_common_qtype(flw)))
                total_flws_filtered++;

            //put this flow into correct bin by flow end time
            sorter.sort_into_bin(flw);

            //print packet info if needed
            if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
            {
                mutex_cout.lock();
                print_flow(flw);
                mutex_cout.unlock();
            }
        }
        else if (retcode == RET_ERROR)
            err_exit();

        if (retcode == RET_OK || retcode == RET_NOT_DNS_FLOW)
            total_flws_processed++;

        if (stop)
            break;

        //automatically skip all non-DNS flows
    } while (retcode != RET_ERROR && retcode != RET_EMPTY);

    total_flws_processed--; //header is counted as NOT_DNS_FLOW

    sorter.push_all_to_shared();            //put all waiting bins into shared queue
    shared_bin_pointers.set_finished(true); //all loading is finished

    total_bins = sorter.get_total_bins();
}

/**
 * Print flow information (debug function).
 */
void ipfix_bin_maker::print_flow(ipfix_flow *flw)
{
    if (flw)
    {
        cout << flw->flowNum << ": " << flw->flowTime.get_date_time_str() << ", bytes=" << flw->bytes << ", " << "pkts=" << flw->packets << ", " << get_srcIP_str(flw);
        cout << " -> " << get_dstIP_str(flw) << ", sp: " << flw->srcPort << ", dp: " << flw->dstPort;

        if (shared_parameters.debug_level > DEBUG_LEVEL_EXTENDED)
        {
            cout << ", " << ((flw->msgType) ? "query" : "response");
            //cout << ", questions=" << pack->qCount << ", responses=" << pack->rCount;

            if (flw->qType == DNS_TYPE_QUERY)
                cout << ", qry: " << flw->qName << ", t: " << dns_type_to_text(flw->qType) << endl;
            else
                cout << ", qry: " << flw->qName << ", t: " << dns_type_to_text(flw->qType) << ", resp: " << flw->rData << endl;
        }
        else
            cout << endl;
    }
}

/**
 * Write flow information into file.
 */
void ipfix_bin_maker::save_to_file(ofstream *file, ipfix_flow *flw)
{
    if (file && flw)
    {
        (*file) << flw->flowTime.get_date_time_str() << " " << get_srcIP_str(flw) << " -> ";
        (*file) << get_dstIP_str(flw) << ", sp=" << flw->srcPort << ", dp=";
        (*file) << flw->dstPort << ", pkts=" << flw->packets << ", bytes=" << flw->bytes << endl;
    }
}

/**
 * Save summary msg to shared obj.
 */
void ipfix_bin_maker::save_summary()
{
    ostringstream str;
    str << "Total flows processed: " << total_flws_processed << endl;
    str << "Total flows filtered:  " << total_flws_filtered << endl;
    str << "Total bins created:    " << total_bins << endl;

    shared_summary.set_parser_msg(str.str());
}


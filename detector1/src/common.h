/**
 * common.h
 * This is header file with prototypes of common functions and constants...
 *
 * DNS structures based on:
 * https://github.com/uvic-sdo/DNS-Packet-Parser
 * RFC 1035 (http://tools.ietf.org/html/rfc1035)
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    13.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/types.h>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <pcap.h>
#include <functional>

#include "detectionsettings.h"

using namespace std;

#ifndef _COMMON_H_
#define _COMMON_H_

//////////////////////////////////////////////////////// CONSTANTS

#define DEBUG true

#define DEBUG_LEVEL_NONE 0          //without debug prints
#define DEBUG_LEVEL_STANDARD 1      //only bin creation/detection/... info
#define DEBUG_LEVEL_EXTENDED 2      //packet/flow information
#define DEBUG_LEVEL_EXTRA_INFO 3    //extended packet/flow information (DNS queries, types, ...)
#define DEBUG_LEVEL_EXTRA_BINS 4    //print bins map

#define CMD_BUFFER_SIZE 1024

#define OK 0
#define ERR 1

//return codes for functions
#define RET_OK 0
#define RET_ERROR 1
#define RET_EMPTY 2
#define RET_NOT_DNS_FLOW 3

//////////////////////////////////// PACKET HEADERS

//ethernet
#define ETHERNET_TYPE_IP (0x0800)
#define ETHERNET_TYPE_IPV6 (0x86DD)
#define ETHERNET_TYPE_DOT1Q (0x8100)
#define ETHERNET_TYPE_ARP (0x0806)

#define ETHERNET_HEADER_SIZE 14
#define ETHERNET_DOT1Q_HEADER_SIZE 18

//other
#define IP_HEADER_SIZE 20
#define IPV6_HEADER_SIZE 40

#define UDP_HEADER_SIZE 8

//union for storing IP addresses
typedef union {
    struct in_addr v4;
    struct in6_addr v6;
} ip46_union;

//////////////////////////////////// DNS

#define DNS_PORT 53

#define DNS_HEADER_SIZE 12
#define DNS_QUESTION_SIZE 4
#define DNS_RR_SIZE 10

#define DNS_QUERIES_MAX_NON_ASCII_RATIO 0.3 //more than this ratio is suspicious

#define CLASS_IN 1  //internet

#define DNS_RDATA_MAX_SIZE 256

//types we need only
#define QTYPE_A 1
#define QTYPE_NS 2
#define QTYPE_CNAME 5
#define QTYPE_NULL 10
#define QTYPE_PTR 12
#define QTYPE_MX 15
#define QTYPE_TXT 16
#define QTYPE_AAAA 28
#define QTYPE_SIG 24
#define QTYPE_KEY 25
#define QTYPE_RRSIG 46
#define QTYPE_DNSKEY 48
#define QTYPE_SRV 33

//////////////////////////////////////////////////////// DNS PACKET STRUCTURES

/* DNS packet (only sections we need):
+---------------------+
|        Header       |
+---------------------+
|       Question      | the question for the name server
+---------------------+
|        Answer       | RRs answering the question
+---------------------+

flags field:
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

*/

//DNS header struct
typedef struct {
    u_int16_t id;           //query ID
    u_int16_t flags;        //flags
    u_int16_t qcount;       //query count
    u_int16_t rcount;       //response count
    u_int16_t nscount;      //autoritative NS responses count
    u_int16_t arcount;      //additional RRs
} dns_header;

//DNS question struct
typedef struct {
    //qname (variable len)
    u_int16_t type; //type of query (A, NS, MX, ...)
    u_int16_t cls;  //class of query
} dns_question;

//DNS RR struct (answer)
typedef struct {
    //name (variable len)
    u_int16_t type;        //type of RR (A, NS, MX, ...)
    u_int16_t cls;         //class code
    u_int32_t ttl;         //TTL of RR
    u_int16_t rdlen;       //length of RDATA
    //rdata (variable len)
} dns_rr;

//////////////////////////////////////////////////////// FLOW STRUCTURE

#define FLOW_TIME_START_POS 11  //pos of time information start in "2015-02-03 22:04:08"
#define TIME_LENGTH 8   //number of expected characters in format "22:04:08"
#define DATE_LENGTH 10  //number of expected characters in format "2015-02-03"

//flow_time - struct for storing and comparing flow record time and duration.
struct flow_time
{
    void init_flow_time(string t, string durat);    //constructor alternative

    bool parse_netflow_time(string t);  //parse netflow time information from str
    bool parse_ipfix_time(string t);    //parse ipfix time information from str
    bool parse_time(string t);          //parse time information from str (different format from parse_netflow_time)
    string get_time_str();      //string representation of time (only time)
    string get_date_time_str(); //string representation of date and time
    string get_date_time_str_ipfix_version();   //date and time in IPFIX csv format

    void set_date(u_int16_t y, u_int8_t m, u_int8_t d); //set date manually
    void set_time(u_int8_t h, u_int8_t m, u_int8_t s);  //set time manually
    void add_time(u_int8_t h, u_int8_t m, u_int8_t s);  //add to this time
    void add_duration_this();   //add duration to this time (to get flow end time)
    void add_hours_to_date(unsigned int hrs);   //add number of hours to date (0-24 hours = +1 day)

    int compare_with(flow_time t);   //compare with other time

    //public
    u_int8_t hours;
    u_int8_t minutes;
    u_int8_t seconds;
    double duration;
    string date;
    bool is_valid;
};

//CSV format for stats file:
//period start, period end, avg bpb, avg fpb, avg bpppb, max bpb, max fpb
//     0             1         2        3         4          5       6
#define NFSTATS_CSV_COLS_MAX 7

#define NFSTATS_CSV_COL_PERSTART 0
#define NFSTATS_CSV_COL_PEREND 1
#define NFSTATS_CSV_COL_AVG_BPB 2
#define NFSTATS_CSV_COL_AVG_FPB 3
#define NFSTATS_CSV_COL_AVG_BPPPB 4
#define NFSTATS_CSV_COL_MAX_BPB 5
#define NFSTATS_CSV_COL_MAX_FPB 6


//parse only these CSV columns (first 15):
//time start, time end, time duration, srcIP, dstIP, srcPort, dstPort, protocol, flags, fwd status, src tos, in pkts, in bytes, out pkts, out bytes,
//    0           1          2            3     4       5        6        7        8        9          10       11       12         13        14
#define NFDUMP_CSV_COLS_MAX 15

/*
#define NFDUMP_CSV_COL_STARTTIME 0
#define NFDUMP_CSV_COL_ENDTIME 1
#define NFDUMP_CSV_COL_DURATION 2
#define NFDUMP_CSV_COL_SRCIP 3
#define NFDUMP_CSV_COL_DSTIP 4
#define NFDUMP_CSV_COL_SRCPORT 5
#define NFDUMP_CSV_COL_DSTPORT 6
#define NFDUMP_CSV_COL_PROTOCOL 7
#define NFDUMP_CSV_COL_INPKTS 11
#define NFDUMP_CSV_COL_INBYTES 12
#define NFDUMP_CSV_COL_OUTPKTS 13
#define NFDUMP_CSV_COL_OUTBYTES 14
*/

//extracted flow fields
struct flow {
    unsigned long flowNum;       //flow number
    flow_time flowTime; //flow end time

    //IP header
    bool ipv6;
    ip46_union srcIP;
    ip46_union dstIP;
    /*
    struct in_addr srcIP;
    struct in_addr dstIP;
    struct in6_addr srcIPv6;
    struct in6_addr dstIPv6;
    */

    u_int8_t ipProtocol;

    //UDP header
    u_int16_t srcPort;
    u_int16_t dstPort;

    //other
    u_int16_t packets;  //flow packets
    u_int32_t bytes;    //flow bytes
};

//////////////////////////////////////////////////////// IPFIX STRUCTURE

//parse only these CSV columns (first 15):
//DST_IP, SRC_IP, BYTES, TIME_FIRST, TIME_LAST, DNS_RR_TTL, PACKETS, DNS_ANSWERS, DNS_QTYPE, DNS_RLENGTH, DST_PORT, SRC_PORT, DNS_RCODE, DNS_NAME, DNS_RDATA
//   0       1      2        3           4           5         6          7           8           9          10        11        12         13        14
#define IPFIX_CSV_COLS_MAX 15

/*
#define IPFIX_CSV_COL_STARTTIME 3
#define IPFIX_CSV_COL_ENDTIME 4
#define IPFIX_CSV_COL_SRCIP 1
#define IPFIX_CSV_COL_DSTIP 0
#define IPFIX_CSV_COL_SRCPORT 11
#define IPFIX_CSV_COL_DSTPORT 10
#define IPFIX_CSV_COL_BYTES 2
#define IPFIX_CSV_COL_PACKETS 6
#define IPFIX_CSV_COL_DNS_TTL 5
#define IPFIX_CSV_COL_DNS_ANSWERS 7
#define IPFIX_CSV_COL_DNS_QTYPE 8
#define IPFIX_CSV_COL_DNS_RLEN 9
#define IPFIX_CSV_COL_DNS_RCODE 12
#define IPFIX_CSV_COL_DNS_QNAME 13
#define IPFIX_CSV_COL_DNS_RDATA 14
*/

#define IPFIX_TIME_START_POS 11  //pos of time information start in "2015-03-17T16:16:50.215"
#define IPFIX_QUERY_STR "(query)"   //str in rdata if packet is query

//extracted ipfix fields
struct ipfix_flow {
    unsigned long flowNum;       //flow number
    flow_time flowTime; //flow end time

    //IP header
    bool ipv6;
    ip46_union srcIP;
    ip46_union dstIP;
    /*
    struct in_addr srcIP;
    struct in_addr dstIP;
    struct in6_addr srcIPv6;
    struct in6_addr dstIPv6;
    */

    //UDP header
    u_int16_t srcPort;
    u_int16_t dstPort;

    //other
    u_int16_t packets;  //flow packets
    u_int32_t bytes;    //flow bytes

    //DNS data
    bool msgType;       //query/response
    u_int16_t rCount;   //response count
    u_int16_t qType;    //type of query (A, NS, MX, ...)
    u_int32_t ttl;      //TTL of RR
    u_int16_t rdLen;    //length of RDATA
    string qName;       //query domain
    string rData;       //response data
    u_int8_t rCode;     //response code
};

//////////////////////////////////////////////////////// PCAP PACKET STRUCTURE

#define DNS_TYPE_QUERY true
#define DNS_TYPE_RESPONSE false
#define DNS_TYPE_MULTIPLE 0

//#define DNS_MAX_DOMAIN_LEVEL 3  //subdomain level of data stored in data bins

//extracted packet fields
struct pkt {
    //default init
    pkt() : ttl(0), rdLen(0), qName(""), rData("") {}

    unsigned long pktNum;//packet number
    flow_time pktTime;   //packet capture time

    //IP header
    bool ipv6;
    ip46_union srcIP;
    ip46_union dstIP;
    /*
    struct in_addr srcIP;
    struct in_addr dstIP;
    struct in6_addr srcIPv6;
    struct in6_addr dstIPv6;
    */

    u_int16_t ipLen;
    u_int8_t ipProtocol;

    //UDP header
    u_int16_t srcPort;
    u_int16_t dstPort;

    //DNS packet
    bool msgType;       //query/response
    u_int16_t qCount;   //query count
    u_int16_t rCount;   //response count
    u_int16_t qType;    //type of query (A, NS, MX, ...)
    u_int32_t ttl;      //TTL of RR
    u_int16_t rdLen;    //length of RDATA
    string qName;       //query domain
    string rData;       //response data
    u_int8_t rCode;     //response code

};


//////////////////////////////////////////////////////// PARAMS STRUCTURE

//program parameters
struct params
{
    //constructor
    params()
    {

        infile = "";
        outfile = "";
        indir = "";
        stats_file = "";

        filetype = '-';
        debug_level = 0;

        s_param = false;

        statistics_period_hrs = DEF_STATISTICS_PERIOD_HRS;
        statistics_period_mins = DEF_STATISTICS_PERIOD_MINS;
        statistics_period_secs = DEF_STATISTICS_PERIOD_SECS;

        bin_size_hrs = DEF_BIN_SIZE_HRS;
        bin_size_mins = DEF_BIN_SIZE_MINS;
        bin_size_secs = DEF_BIN_SIZE_SECS;
        bin_packets = DEF_BIN_SIZE_PACKETS;

        num_of_sorting_bins = DEF_NUM_OF_SORTING_BINS;
        filter_common_qtypes = DEF_FILTER_COMMON_QTYPES;
        max_aggregated_bins = DEF_MAX_AGGREGATED_BINS;
        dns_extract_domain_level = DEF_DNS_EXTRACT_DOMAIN_LEVEL;

        thresh_stat_avg_bpb = DEF_STATISTICS_THRESH_AVG_BPB;
        thresh_stat_avg_fpb = DEF_STATISTICS_THRESH_AVG_FPB;
        thresh_stat_avg_bpppb = DEF_STATISTICS_THRESH_AVG_BPPPB;

        thresh_thresh_bpppb = DEF_THRESHOLD_THRESH_BPPPB;

        d1_detection_schema = DEF_D1_DETECTION_SCHEMA;
        d2_detection_schema = DEF_D2_DETECTION_SCHEMA;

        packet_size_thresh = DEF_PACKET_SIZE_THRESH;
        bpf_thresh = DEF_BYTES_PER_FLOW_THRESH;
        ppf_thresh = DEF_PKTS_PER_FLOW_THRESH;
        packet_ttl_thresh = DEF_PACKET_TTL_THRESH;
        packet_rdata_size_thresh = DEF_PACKET_RDATA_SIZE_THRESH;
        query_packet_size_thresh = DEF_QUERY_PACKET_SIZE_THRESH;
        single_flow_duration = DEF_SINGLE_FLOW_DURATION;

        detect_only_client_communication = (DEF_DETECT_ONLY_CLIENT_COMMUNICATION) ? true : false;
        map_minimum_conditions = DEF_MAP_MINIMUM_CONDITIONS;
        map_minimum_most_used_domain_count = DEF_MAP_MINIMUM_MOST_USED_DOMAIN_COUNT;
        occurences_in_map_thresh = DEF_OCCURENCES_IN_MAP_THRESH;
        map_minimum_packets = DEF_MAP_MINIMUM_PACKETS;
        avg_size_map_thresh = DEF_AVG_SIZE_MAP_THRESH;
        total_bytes_map_thresh = DEF_TOTAL_BYTES_MAP_THRESH;
        total_queries_map_thresh = DEF_TOTAL_QUERIES_MAP_THRESH;
        total_bytes_queries_only_map_thresh = DEF_TOTAL_BYTES_QUERIES_ONLY_MAP_THRESH;
        avg_namelen_map_thresh = DEF_AVG_NAMELEN_MAP_THRESH;
        most_used_domain_count_thresh = DEF_MOST_USED_DOMAIN_COUNT_THRESH;
        most_used_qtype_count_thresh = DEF_MOST_USED_QTYPE_COUNT_THRESH;
    }

    //cmd and config params
    int debug_level;

    string infile;
    string outfile;
    string indir;
    string stats_file;

    char filetype;

    bool s_param;

    //periods
    u_int8_t statistics_period_hrs;
    u_int8_t statistics_period_mins;
    u_int8_t statistics_period_secs;

    u_int8_t bin_size_hrs;
    u_int8_t bin_size_mins;
    u_int8_t bin_size_secs;
    unsigned int bin_packets;

    //misc
    unsigned int num_of_sorting_bins;
    bool filter_common_qtypes;
    unsigned int max_aggregated_bins;
    unsigned int dns_extract_domain_level;

    //detector 1 thresholds
    double thresh_stat_avg_bpb;
    double thresh_stat_avg_bpppb;
    double thresh_stat_avg_fpb;

    double thresh_thresh_bpppb;

    //detection schemas
    int d1_detection_schema;
    int d2_detection_schema;

    //detector 2 single flow/packet
    unsigned int packet_size_thresh;
    unsigned int bpf_thresh;
    unsigned int ppf_thresh;
    unsigned int packet_ttl_thresh;
    unsigned int packet_rdata_size_thresh;
    unsigned int query_packet_size_thresh;
    double single_flow_duration;

    //detector 2 aggregated map
    bool detect_only_client_communication;
    unsigned int map_minimum_conditions;
    unsigned int map_minimum_most_used_domain_count;
    unsigned int occurences_in_map_thresh;
    unsigned int map_minimum_packets;
    unsigned int avg_size_map_thresh;
    unsigned int total_bytes_map_thresh;
    unsigned int total_queries_map_thresh;
    unsigned int total_bytes_queries_only_map_thresh;
    unsigned int avg_namelen_map_thresh;
    unsigned int most_used_domain_count_thresh;
    unsigned int most_used_qtype_count_thresh;

    //whitelist domains
    vector<string> whitelist_domains;
};

//helper struct for storing parsed configuration parameter
struct config_parameter
{
    string name;
    string value;
};

//////////////////////////////////////////////////////// CSVROW CLASS

/**
 * CSVRow - class for easy work with CSV records.
 * Based on:
 * http://stackoverflow.com/questions/1120140/how-can-i-read-and-parse-csv-files-in-c
 */
class CSVRow
{
  public:
    string const& operator[](size_t index) const   //field selection operator
    {
        return fields[index];
    }

    size_t size() const //number of fields
    {
        return fields.size();
    }

    void read_next_row(istream& stream) //read and parse record
    {
        string line;
        getline(stream, line);

        stringstream lineStream(line);
        string cell;

        fields.clear();

        unsigned int columnsmax = NFDUMP_CSV_COLS_MAX;
        if (IPFIX_CSV_COLS_MAX > NFDUMP_CSV_COLS_MAX)
            columnsmax = IPFIX_CSV_COLS_MAX;

        //parse only first N columns from CSV record
        while(getline(lineStream, cell, ',') && fields.size() < columnsmax)
        {
            char c = cell[cell.size() - 1];
            if (c == '\n' || c == '\r')
                cell.erase(cell.size() - 1);

            fields.push_back(cell);
        }
    }

  private:
    vector<string> fields;
};

/**
 * Operator >> for easy work with CSVRow
 */
inline istream& operator>>(istream& str, CSVRow& data)
{
    data.read_next_row(str);
    return str;
}


//////////////////////////////////////////////////////// OTHER

//support struct for removing whitespace chars from string
struct remove_delim
{
    bool operator()(char c)
    {
        return (c =='\r' || c =='\t' || c == ' ' || c == '\n');
    }
};

//hash_combine functions, taken from boost
//(http://www.boost.org/doc/libs/1_35_0/doc/html/boost/hash_combine_id241013.html)
template <class T>
inline void hash_combine(std::size_t & seed, const T & v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

//////////////////////////////////////////////////////// FUNCTIONS
void print_err(const char *error_str);
void err_exit();
void err_exit_with_msg(const char *error_str);

string system_cmd(const char *cmd);                     //run system command and return its output

bool parse_config_file(char *filename, params *p);      //parse detection parameters from configuration file
bool parse_whitelist_file(char *filename, params *p);   //parse whitelist domains from whitelist file
config_parameter parse_config_name(string line);        //parse configuration parameter name from str
void create_config_file(char *filename);                //create default config file
void create_whitelist_file(char *filename);             //create default whitelist file

int get_int_from_str(const char *str, bool *err);       //extract int from char*
double get_double_from_string(string str, bool *err);   //extract double from string

string extract_domain_name_to_nth_level(string domain, unsigned int level); //extract subdomain of nth level from domain name

const char *dns_type_to_text(int type);                 //convert numeric DNS Query type to str

bool contains_too_much_non_ascii_data(string str);      //check if string has too much non-ascii chars
bool subdomains_have_same_len(string domain);           //check if domain name has subdomains of same length

//if flow/packet has common query type (for filtering common qtypes)
bool has_common_qtype(ipfix_flow *flw);
bool has_common_qtype(pkt *pack);

string get_ipv4_str(in_addr ip);    //get string representation of IPv4 address
string get_ipv6_str(in6_addr ip);   //get string representation of IPv6 address

//get string with src IP address (IP/IPv6 selection)
string get_srcIP_str(flow *flw);
string get_srcIP_str(ipfix_flow *flw);
string get_srcIP_str(pkt *pack);

//get string with dst IP address (IP/IPv6 selection)
string get_dstIP_str(flow *flw);
string get_dstIP_str(ipfix_flow *flw);
string get_dstIP_str(pkt *pack);

#endif

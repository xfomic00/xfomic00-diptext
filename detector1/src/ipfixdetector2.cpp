/**
 * ipfixdetector2.cpp
 * This file implements methods of class ipfix_detector2.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    22.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>


#include "ipfixdetector2.h"
#include "ipfixparser.h"
#include "common.h"
#include "shared.h"

using namespace std;

////////////////////////////////////////////////////////////// IPFIX_DETECTOR2 METHODS

/**
 * Constructor
 */
ipfix_detector2::ipfix_detector2(params *pars)
{
    parameters = pars;
    total_bins = 0;
    stored_bins = 0;
    total_detections = 0;
    total_flows_processed = 0;
    total_flows_analyzed = 0;
    total_flows_whitelisted = 0;
    bin = NULL;
    last_bin_end.is_valid = false;

    for(int i=0; i<IPFIX_SINGLE_THRESHOLDS; i++)
        thresh_stats[i] = 0;
}

/**
 * Destructor
 */
ipfix_detector2::~ipfix_detector2()
{
    delete_finished_bin();
}

/**
 * Process shared queue of bins filtered by detector 1.
 */
void ipfix_detector2::process_queue()
{
    ofstream outfile(parameters->outfile);  //open output file
    if (! outfile.is_open())
    {
        cerr << "Couldn't create output file " << parameters->outfile << "!" << endl;
        err_exit();
    }

    opened_stuff_shared.opened_outfile = &outfile;

    //process all bins in shared queue
    //(NULL is returned when queue is empty and producer process has finished)
    while ((bin = shared_bin_pointers_filtered_by_d1.get_data()) != NULL)
    {
        //bin(s) were detected in succession, but now sequence ends -> inspect aggregated data
        if ((last_bin_end.is_valid && last_bin_end.compare_with(bin->get_start_time()) != 0) || (stored_bins == parameters->max_aggregated_bins))
            inspect_aggregated_bins(&outfile);

        //detection of new bin
        detect_in_bin(bin);

        total_bins++;
        stored_bins++;
        last_bin_end = bin->get_end_time();
        last_bin_end.is_valid = true;

        delete_finished_bin();  //delete finished bin

        if (stop)
            break;
    }

    //inspect final aggregated data
    inspect_aggregated_bins(&outfile);

    outfile.close();
    opened_stuff_shared.opened_outfile = NULL;
}

/**
 * Detection method for single IPFIX flow.
 * Returns true if detection was positive, false otherwise.
 */
bool ipfix_detector2::detect(ipfix_flow *flw)
{
    bool ret = false;

    if (flw)
    {
        if (flw->bytes > parameters->bpf_thresh)                                    //bytes per flow threshold
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_BPF_POS];
        }
        else if (flw->packets > parameters->ppf_thresh)                             //packets per flow threshold
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_PPF_POS];
        }
        else if (flw->packets == 1 && flw->bytes > parameters->packet_size_thresh)  //single packet size thresh
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_PACK_SIZE_POS];
        }
        else if (flw->msgType == DNS_TYPE_RESPONSE && flw->ttl < parameters->packet_ttl_thresh)     //DNS TTL threshold (only on responses)
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_TTL_POS];
        }
        else if (flw->rdLen > parameters->packet_rdata_size_thresh)                 //DNS RDATA size threshold
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_RDLEN_POS];
        }
        else if (flw->msgType == DNS_TYPE_QUERY && flw->bytes > parameters->query_packet_size_thresh) //DNS query packet size threshold
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_QUERY_SIZE_POS];
        }
        else if (flw->qName.length() > parameters->avg_namelen_map_thresh)          //DNS query domain length
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_QNAME_LEN_POS];
        }
        else if (subdomains_have_same_len(flw->qName) || subdomains_have_same_len(flw->rData))  //multiple subdomains of same length
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_SUBDOMAIN_LEN_POS];
        }
        else if (contains_too_much_non_ascii_data(flw->qName) || contains_too_much_non_ascii_data(flw->rData))  //too much non-ascii data in query or rdata
        {
            ret = true;
            ++thresh_stats[IPFIX_THRESH_TOO_MUCH_ASCII_POS];
        }
    }

    return ret;
}

/**
 * Detection method for one IPFIX bin.
 */
void ipfix_detector2::detect_in_bin(ip_storage *bin_to_inspect)
{
    if (bin_to_inspect)
    {
        vector<ipfix_flow> *flows = bin_to_inspect->get_ipfix_flows_vector();

        //iterate through all flows in bin
        for (unsigned int i=0; i<flows->size(); i++)
        {
            //detection based on selected detection schema
            if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_ALL)
            {
                //aggregate all flows in bin
                put_ip_data_in_map(&((*flows)[i]));
            }
            else if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_SINGLE)
            {
                //aggregate only certain flows based on thresholds
                if (detect(&((*flows)[i])))
                    put_ip_data_in_map(&((*flows)[i]));
            }
            else if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES)
            {
                //aggregate only suspicious DNS query types commonly used for DNS tunneling
                u_int16_t qtype = (*flows)[i].qType;
                if (qtype == QTYPE_MX || qtype == QTYPE_NS || qtype == QTYPE_TXT || qtype == QTYPE_NULL || qtype == QTYPE_CNAME)
                    put_ip_data_in_map(&((*flows)[i]));
            }
            else if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES_THRESHOLDS)
            {
                //aggregate only suspicious DNS query types where thresholds are met
                u_int16_t qtype = (*flows)[i].qType;
                if ((qtype == QTYPE_MX || qtype == QTYPE_NS || qtype == QTYPE_TXT || qtype == QTYPE_NULL || qtype == QTYPE_CNAME)
                        && detect(&((*flows)[i])))
                    put_ip_data_in_map(&((*flows)[i]));
            }

            if (stop)
                break;
        } //for

        total_flows_processed += flows->size();
    }
}

/**
 * Inspect aggregated data.
 */
void ipfix_detector2::inspect_aggregated_bins(ofstream *file)
{
    unordered_map<flow_key, storage_data, flow_key_hasher> *table = ipmap.get_map();

    const int TOTAL_CRITERIAS = 8;  //total number of detection criterias

    //iterate through aggregated records
    for(unordered_map<flow_key, storage_data, flow_key_hasher>::iterator iter = table->begin(); iter != table->end(); iter++)
    {
        in_addr srcIP, dstIP;
        in6_addr srcIPv6, dstIPv6;
        flow_key fk = (flow_key)((*iter).first);
        if (fk.ipv6)
        {
            srcIPv6 = fk.srcIP.v6;
            dstIPv6 = fk.dstIP.v6;
        }
        else
        {
            srcIP.s_addr = fk.srcIP.v4.s_addr;
            dstIP.s_addr = fk.dstIP.v4.s_addr;
        }

        storage_data *data;
        data = (storage_data *) &((*iter).second);

        ostringstream out_stream;
        unsigned int conditions_matched = 0;

        //detection
        if (data->get_avg_packet_size() > parameters->avg_size_map_thresh && data->get_packets_count() > parameters->map_minimum_packets) //avg packet size threshold (needs more packets)
        {
            out_stream << "\t" << "avg packet size = " << data->get_avg_packet_size() << " B (detection threshold = " << parameters->avg_size_map_thresh << "), packets: " << data->get_packets_count() << endl;
            ++conditions_matched;
        }
        if (data->get_bytes_total() > parameters->total_bytes_map_thresh) //total bytes threshold
        {
            out_stream << "\t" << "total bytes = " << data->get_bytes_total() << " B (detection threshold = " << parameters->total_bytes_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_occurences_count() > parameters->occurences_in_map_thresh) //occurences in map threshold
        {
            out_stream << "\t" << "occurences in map = " << data->get_occurences_count() << " (detection threshold = " << parameters->occurences_in_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_queries_total() > parameters->total_queries_map_thresh) //number of queries threshold
        {
            out_stream << "\t" << "total number of queries = " << data->get_queries_total() << " (detection threshold = " << parameters->total_queries_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_bytes_only_queries() > parameters->total_bytes_queries_only_map_thresh) //total bytes only on queries
        {
            out_stream << "\t" << "total bytes only on queries = " << data->get_bytes_only_queries() << " B (detection threshold = " << parameters->total_bytes_queries_only_map_thresh << "), queries: " << data->get_queries_total() << endl;
            ++conditions_matched;
        }
        if (data->get_avg_namelen_size() > parameters->avg_namelen_map_thresh && data->get_packets_count() > parameters->map_minimum_packets) //average DNS query length threshold
        {
            out_stream << "\t" << "avg DNS query len = " << data->get_avg_namelen_size() << " (detection threshold = " << parameters->avg_namelen_map_thresh << "), packets: " << data->get_packets_count() << endl;
            ++conditions_matched;
        }
        if (data->get_most_used_domain_count() > parameters->most_used_domain_count_thresh)
        {
            out_stream << "\t" << "most used domain count = " << data->get_most_used_domain_count() << " (detection threshold = " << parameters->most_used_domain_count_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_most_used_qtype_count() > parameters->most_used_qtype_count_thresh)
        {
            out_stream << "\t" << "most used qType count = " << data->get_most_used_qtype_count() << " (detection threshold = " << parameters->most_used_qtype_count_thresh << ")" << endl;
            ++conditions_matched;
        }

        //print detected IP info
        if (conditions_matched >= parameters->map_minimum_conditions && data->get_most_used_domain_count() >= parameters->map_minimum_most_used_domain_count)
        {
            u_int16_t srcPort = (data->get_srcports_count() > 1) ? 0 : data->get_srcports_map()->begin()->first;
            u_int16_t dstPort = (data->get_dstports_count() > 1) ? 0 : data->get_dstports_map()->begin()->first;
            string srcIP_str = (fk.ipv6) ? get_ipv6_str(srcIPv6) : get_ipv4_str(srcIP);
            string dstIP_str = (fk.ipv6) ? get_ipv6_str(dstIPv6) : get_ipv4_str(dstIP);

            //detect only communication from tunnel client to tunnel server
            if (parameters->detect_only_client_communication && srcPort == DNS_PORT)
                continue;

            //print flow time and IPs/ports
            (*file) << data->get_first_time().get_date_time_str() << " - " << data->get_last_time().get_date_time_str() << endl;
            (*file) << srcIP_str << ":";
            if (srcPort == 0)
                (*file) << "XXXXX";
            else
                (*file) << srcPort;
            (*file) << " -> " << dstIP_str << ":";
            if (dstPort == 0)
                (*file) << "XXXXX" << endl;
            else
                (*file) << dstPort << endl;

            //print domains
            (*file) << "Used domains:" << endl;
            for (map<string, u_int32_t>::iterator iter = data->get_domains_map()->begin(); iter != data->get_domains_map()->end(); iter++)
            {
                (*file) << "\t" << iter->first << "\t\t" << iter->second << "x" << endl;
            }

            //print DNS query types
            (*file) << "Used query types:" << endl;
            for (map<u_int16_t, u_int32_t>::iterator iter = data->get_qtypes_map()->begin(); iter != data->get_qtypes_map()->end(); iter++)
            {
                (*file) << "\t" << dns_type_to_text((u_int16_t)(iter->first)) << "\t\t" << iter->second << "x" << endl;
            }

            (*file) << "Detection criteria (matched " << conditions_matched << "/" << TOTAL_CRITERIAS << "):" << endl;
            (*file) << out_stream.str();

            total_detections++;
            (*file) << "----------------------------" << endl;
        }

        if (stop)
            break;
    }

    ipmap.clear_all();
    stored_bins = 0;
}

/**
 * Put flow data into internal aggregation map for further analysis.
 */
void ipfix_detector2::put_ip_data_in_map(ipfix_flow *flw)
{
    if (flw)
    {
        aggreg_data data(flw->srcPort, flw->dstPort, flw->qName, flw->packets, flw->bytes, flw->flowTime, flw->qType, flw->rdLen, (flw->msgType == DNS_TYPE_QUERY) ? true : false);

        bool added = false;

        if (flw->ipv6)
            added = ipmap.add_flow_data(flw->srcIP.v6, flw->dstIP.v6, data);
        else
            added = ipmap.add_flow_data(flw->srcIP.v4.s_addr, flw->dstIP.v4.s_addr, data);

        total_flows_analyzed++;

        if (! added)    //if flow was not added, it means it had whitelist domain
            total_flows_whitelisted++;
    }
}

/**
 * Free finished bin data.
 */
void ipfix_detector2::delete_finished_bin()
{
    if (bin)
    {
        delete bin;
        bin = NULL;
    }
}

/**
 * Save summary msg to shared summary.
 */
void ipfix_detector2::save_summary()
{
    ostringstream str;
    str << "Total bins processed:    " << total_bins << endl;
    str << "Total flows processed:   " << total_flows_processed << endl;
    str << "Total flows analyzed:    " << total_flows_analyzed << endl;
    str << "Total flows whitelisted: " << total_flows_whitelisted << endl;
    str << "Total flows detected:    " << total_detections << endl;
    str << endl;
    str << "Single flow thresholds usage statistics:" << endl;

    //print thresholds usage info
    str << "Bytes per flow:                " << thresh_stats[IPFIX_THRESH_BPF_POS] << endl;
    str << "Packets per flow:              " << thresh_stats[IPFIX_THRESH_PPF_POS] << endl;
    str << "Single packet size:            " << thresh_stats[IPFIX_THRESH_PACK_SIZE_POS] << endl;
    str << "DNS TTL:                       " << thresh_stats[IPFIX_THRESH_TTL_POS] << endl;
    str << "DNS RDATA size:                " << thresh_stats[IPFIX_THRESH_RDLEN_POS] << endl;
    str << "DNS Query size:                " << thresh_stats[IPFIX_THRESH_QUERY_SIZE_POS] << endl;
    str << "DNS domain len:                " << thresh_stats[IPFIX_THRESH_QNAME_LEN_POS] << endl;
    str << "Same length subdomains:        " << thresh_stats[IPFIX_THRESH_SUBDOMAIN_LEN_POS] << endl;
    str << "Too much non-ASCII characters: " << thresh_stats[IPFIX_THRESH_TOO_MUCH_ASCII_POS] << endl;

    shared_summary.set_detector2_msg(str.str());
}

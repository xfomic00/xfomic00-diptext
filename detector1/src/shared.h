/**
 * shared.h
 * This file defines classes for sharing data between threads.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    11.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <condition_variable>
#include <thread>
#include <mutex>
#include <chrono>
#include <queue>
#include <pcap.h>
#include <fstream>

#include "ip_storage.h"
#include "common.h"
#include "libnfdump.h"

using namespace std;

#ifndef _SHARED_H_
#define _SHARED_H_

/////////////////////////////////////////////// CONSTANTS

extern class shared_queue_ip_storage shared_bin_pointers;               //shared bins between parser and detector1

extern class shared_queue_ip_storage shared_bin_pointers_filtered_by_d1;//shared bins between detector1 and detector2

extern struct opened_stuff opened_stuff_shared; //temporary pointers to opened files etc.

extern class shared_detectors_summary shared_summary;   //processing summary

extern params shared_parameters;    //program parameters

extern mutex mutex_cout;            //global mutex for log information

extern bool stop;                   //stop all processing after signal catch

/////////////////////////////////////////////// CLASSES & STRUCTS


/**
 * Struct with pointer to opened files, which needs to be freed before program exit.
 * Used with signal catching in main.cpp
 */
struct opened_stuff
{
    opened_stuff() : handle(NULL), pcap_opened_file(NULL), opened_outfile(NULL), opened_infile(NULL), opened_nfdump_file(NULL) {}

    pcap_t *handle;
    pcap_t *pcap_opened_file;
    ofstream *opened_outfile;
    ifstream *opened_infile;
    nfdump_iter_t *opened_nfdump_file;
};

/**
 * Shared class to hold summary msgs from detectors...
 * Summary msgs are written at the end of outfile.
 */
class shared_detectors_summary
{
public:
    shared_detectors_summary() {}
    void set_parser_msg(string str) { parser_msg = str; }
    void set_detector1_msg(string str) { detector1_msg = str; }
    void set_detector2_msg(string str) { detector2_msg = str; }

    void write_msgs_to_file(ofstream *file);

private:
    string parser_msg;
    string detector1_msg;
    string detector2_msg;
};

/**
 * Shared queue with ip_storage pointers.
 * Work with queue is thread safe.
 */
class shared_queue_ip_storage
{
public:
    shared_queue_ip_storage();
    ~shared_queue_ip_storage();

    void set_finished(bool fin);    //set finished variable

    void add_data(ip_storage *data);    //add pointer to queue
    ip_storage *get_data();             //get pointer from queue (or wait for it)

private:
    queue<ip_storage *> pointers;   //queue of pointers
    bool finished;                  //if adding is finished
    mutex _m;                       //mutex for locking CS
    condition_variable _cv;         //for synchronizing threads
};

#endif

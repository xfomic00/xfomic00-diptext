/**
 * netflowstatisticsmaker.cpp
 * This file implements methods of class netflow_statistics_maker.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    25.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>


#include "netflowstatisticsmaker.h"
#include "netflowparser.h"
#include "ipfixparser.h"
#include "common.h"
#include "shared.h"

using namespace std;

////////////////////////////////////////////////////////////// NETFLOW_STATISTICS_MAKER METHODS

/**
 * Constructor
 */
netflow_statistics_maker::netflow_statistics_maker(params *par)
{
    parameters = par;
    last_bin_index = 0;
    last_period_index = 0;
    actual_period = NULL;
    //wait_for_period_start = false;
}

/**
 * Destructor
 */
netflow_statistics_maker::~netflow_statistics_maker()
{
}

/**
 * Create statistics from NetFlow input files.
 */
void netflow_statistics_maker::create_netflow_statistics()
{
    int retcode;
    netflow_parser parser(parameters);
    retcode = parser.open_infile(); //open first file, rest is managed by parser itself
    if (retcode != RET_OK)
        err_exit();

    flow *flw;   //flow record

    do  //read flow records
    {
        flw = parser.get_next_flow(&retcode, true); //parse only statistical data
        if (flw && retcode == RET_OK)   //DNS flow
        {
            collect_statistics(flw, NULL);
        }
        else if (retcode == RET_ERROR)
        {
            err_exit();
        }

        if (stop)
            break;

        //automatically skip all non-DNS flows
    } while (retcode != RET_ERROR && retcode != RET_EMPTY);

    //aggregate remaining bins in vector
    aggregate_remaining_bins();

    //export statistics
    save_statistics();
}

/**
 * Create statistics from IPFIX csv input files.
 */
void netflow_statistics_maker::create_ipfix_statistics()
{
    int retcode;
    ipfix_parser parser(parameters);
    retcode = parser.open_infile(); //open first file, rest is managed by parser itself
    if (retcode != RET_OK)
        err_exit();

    ipfix_flow *flw;   //flow record

    do  //read flow records
    {
        flw = parser.get_next_flow(&retcode, true); //parse only statistical data
        if (flw && retcode == RET_OK)   //DNS flow
        {
            collect_statistics(NULL, flw);
        }
        else if (retcode == RET_ERROR)
        {
            err_exit();
        }

        if (stop)
            break;

        //automatically skip all non-DNS flows
    } while (retcode != RET_ERROR && retcode != RET_EMPTY);

    //aggregate remaining bins in vector
    aggregate_remaining_bins();

    //export statistics
    save_statistics();
}

/**
 * Save statisticall data.
 * Only one parameter can be set, other must be NULL!
 */
void netflow_statistics_maker::collect_statistics(flow *flw, ipfix_flow *iflw)
{
    if (flw || iflw)
    {
        bool is_ipfix = (iflw) ? true : false;
        //unsigned long flownum = (is_ipfix) ? iflw->flowNum : flw->flowNum;
        flow_time ftime = (is_ipfix) ? iflw->flowTime : flw->flowTime;

        static bool was_first = false;

        if (! was_first)  //first flow, determine first bin and period end time
        {
            create_new_bin(ftime);

            create_all_periods();
            actual_period = get_period(ftime);

            was_first = true;

            if (parameters->debug_level > DEBUG_LEVEL_NONE)
                cout << "set period start = " << actual_period->start.get_time_str() << ", period end = " << actual_period->end.get_time_str() << endl;
        }

        //aggregate bin data
        if (is_ipfix)
            aggregate_into_correct_bin(ftime, 1, iflw->packets, iflw->bytes);
        else
            aggregate_into_correct_bin(ftime, 1, flw->packets, flw->bytes);
    }
}

/**
 * Export computed statistics into file.
 */
void netflow_statistics_maker::save_statistics()
{
    ofstream outfile(parameters->outfile);  //open output file
    if (! outfile.is_open())
    {
        cerr << "Couldn't create output file " << parameters->outfile << "!" << endl;
        err_exit();
    }

    opened_stuff_shared.opened_outfile = &outfile;

    //CSV format header
    outfile << "start,end,avg bpb,avg fpb,avg bpppb,max bpb,max fpb" << endl;

    //data
    for(unsigned int i=0; i<periods.size(); i++)
    {
        if (periods[i].data.get_bins() > 0)
        {
            outfile << periods[i].start.get_time_str() << "," << periods[i].end.get_time_str() << "," << fixed << setprecision(4) << periods[i].data.get_avg_bytes_per_bin() << ",";
            outfile << periods[i].data.get_avg_flows_per_bin() << "," << periods[i].data.get_avg_bytes_per_packet_per_bin() << ",";
            outfile << periods[i].data.get_max_bytes_per_bin() << "," << periods[i].data.get_max_flows_per_bin() << endl;
        }
    }

    outfile.close();
    opened_stuff_shared.opened_outfile = NULL;
}

/**
 * Aggregate flow data into correct bin.
 */
void netflow_statistics_maker::aggregate_into_correct_bin(flow_time flwtime, unsigned int flows, unsigned int packets, unsigned long bytes)
{
    bool found = false;
    bool dont_check = false;
    unsigned int curr_index = last_bin_index;

    while (! found)
    {
        if (curr_index >= bins.size())
            curr_index = 0;

        //check last index first
        if (bins[curr_index].bin_start.compare_with(flwtime) <= 0 && bins[curr_index].bin_end.compare_with(flwtime) > 0)
        {
            found = true;
        }
        else    //we must search
        {
            //flow is too old, put it in first bin
            if (! dont_check && bins[0].bin_start.compare_with(flwtime) >= 0)
            {
                curr_index = 0;
                found = true;
            }
            //bin for this flow don't exist yet
            else if (! dont_check && bins[bins.size() - 1].bin_end.compare_with(flwtime) <= 0)
            {
                //end time of last existing bin
                flow_time last_bin_time = bins[bins.size() - 1].bin_end;

                while (last_bin_time.compare_with(flwtime) <= 0)
                {
                    //aggregate oldest bin data into period
                    if (bins.size() == parameters->num_of_sorting_bins)
                    {
                        bin_data first_bin = bins[0];
                        bins.erase(bins.begin());
                        if (first_bin.flows > 0)
                            get_period(first_bin.bin_start)->data.add(first_bin.bytes, first_bin.flows, first_bin.get_bytes_per_packet_per_bin());
                    }

                    //create new bin & push it into local vector
                    create_new_bin(last_bin_time);
                    last_bin_time = bins[bins.size() - 1].bin_end;
                }

                curr_index = bins.size() - 1;
                found = true;
            }
            //bin is somewhere in local vector
            else
            {
                if (bins[curr_index].bin_end.compare_with(flwtime) < 0)
                    curr_index++;   //bin with bigger time
                else
                    curr_index--;   //bin with smaller time

                dont_check = true;
            }
        }
    }

    bins[curr_index].add(flows, packets, bytes);
}

/**
 * Get period pointer to period to which to aggregate bin with bin_start_time start time.
 */
period *netflow_statistics_maker::get_period(flow_time bin_start_time)
{
    if (actual_period == NULL)
        actual_period = &periods[0];

    if (actual_period->end.compare_with(bin_start_time) > 0 && actual_period->start.compare_with(bin_start_time) <= 0)
        return actual_period;
    else
    {
        bool found = false;
        while (! found)
        {
            if (actual_period->end.compare_with(bin_start_time) <= 0)   //search previous period
            {
                last_period_index = (last_period_index + 1) % periods.size();
                actual_period = &(periods[last_period_index]);
            }
            else if (actual_period->start.compare_with(bin_start_time) > 0) //search next period
            {
                if (last_period_index == 0)
                    last_period_index = periods.size() - 1;
                else
                    --last_period_index;

                actual_period = &(periods[last_period_index]);
            }
            else
                found = true;
        }
    }

    return actual_period;
}

/**
 * Create new bin and put it into vector.
 */
void netflow_statistics_maker::create_new_bin(flow_time flwtime)
{
    bin_data b;
    b.bin_start = flwtime;
    flwtime.add_time(parameters->bin_size_hrs, parameters->bin_size_mins, parameters->bin_size_secs);
    b.bin_end = flwtime;

    bins.push_back(b);
    last_bin_index = (bins.size() - 1);

    if (parameters->debug_level > DEBUG_LEVEL_NONE)
        cout << "new bin, end = " << flwtime.get_time_str() << endl;
}

/**
 * Create all daily periods.
 */
void netflow_statistics_maker::create_all_periods()
{
    flow_time per;
    per.set_time(0,0,0);    //periods from 00:00:00
    per.set_date(0,0,0);

    while (1)
    {
        period p;
        p.start = per;
        per.add_time(parameters->statistics_period_hrs, parameters->statistics_period_mins, parameters->statistics_period_secs);
        p.end = per;

        if (per.get_time_str() == "00:00:00")
        {
            per.set_time(23,59,59);
            p.end = per;
            periods.push_back(p);
            break;
        }
        else
            periods.push_back(p);
    }
}

/**
 * Aggregate all remaining bins into correct periods.
 */
void netflow_statistics_maker::aggregate_remaining_bins()
{
    for (unsigned int i=0; i<bins.size(); i++)
    {
        bin_data bin = bins[i];
        if (bin.flows > 0 && (i != (bins.size() - 1) || bins.size() == 1))    //do not aggregate last bin (it could decrease all statistics if bin was half empty)
            get_period(bin.bin_start)->data.add(bin.bytes, bin.flows, bin.get_bytes_per_packet_per_bin());
    }

    bins.clear();
}


/**
 * netflowstatistics.cpp
 * This file implements methods of class netflow_statistics.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    4.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>


#include "netflowstatistics.h"
#include "netflowparser.h"
#include "common.h"
#include "shared.h"

using namespace std;

/**
 * Compare two elements of nfstat (for std::sort).
 */
inline bool compare_nfstat(nfstat el1, nfstat el2)
{
    return (el1.periodstart.compare_with(el2.periodstart) < 0);
}

////////////////////////////////////////////////////////////// NETFLOW_STATISTICS METHODS

/**
 * Constructor
 */
netflow_statistics::netflow_statistics()
{
    parameters = NULL;
    last_index = 0;
}

/**
 * Extract information from statistics file.
 * Returns true if parsing was successful, false otherwise.
 */
bool netflow_statistics::parse_statistics_file()
{
    bool ret = true;

    ifstream infile;
    infile.open(parameters->stats_file);
    if (! infile.good())
    {
        cerr << "Couldn't open input file " << parameters->stats_file << "!" << endl;
        return false;
    }

    opened_stuff_shared.opened_infile = &infile;

    CSVRow row; //input data row
    nfstat stat;
    bool first_done = false;

    while (infile >> row)   //load next row
    {
        if (! first_done)   //skip first line (header)
        {
            first_done = true;
            continue;
        }

        //CSV format for stats file:
        //period start, period end, avg bpb, avg fpb, avg bpppb, max bpb, max fpb
        bool err = false;
        stat.periodstart.parse_time(row[NFSTATS_CSV_COL_PERSTART]);
        stat.periodend.parse_time(row[NFSTATS_CSV_COL_PEREND]);
        stat.avg_bpb = get_double_from_string(row[NFSTATS_CSV_COL_AVG_BPB], &err);
        stat.avg_fpb = get_double_from_string(row[NFSTATS_CSV_COL_AVG_FPB], &err);
        stat.avg_bpppb = get_double_from_string(row[NFSTATS_CSV_COL_AVG_BPPPB], &err);
        stat.max_bpb = get_double_from_string(row[NFSTATS_CSV_COL_MAX_BPB], &err);
        stat.max_fpb = get_double_from_string(row[NFSTATS_CSV_COL_MAX_FPB], &err);

        if (err)
        {
            ret = false;
            break;
        }

        stats_vect.push_back(stat); //save to vector

        if (stop)
            break;
    }

    infile.close();
    opened_stuff_shared.opened_infile = NULL;

    sort_statistic_data();      //sort data for faster access
    calc_avg_period_stats();    //calculate avg statistics
    fill_missing_stats();       //add missing statistics periods data

    return ret;
}

/**
 * Sort statistics data in vector by period start time.
 */
void netflow_statistics::sort_statistic_data()
{
    sort(stats_vect.begin(), stats_vect.end(), compare_nfstat);
}

/**
 * Calculate average period statistics.
 * Average period stats are used when there is no data for requested period...
 */
void netflow_statistics::calc_avg_period_stats()
{
    nfstat temp;
    temp.avg_bpb = 0;
    temp.avg_bpppb = 0;
    temp.avg_fpb = 0;
    temp.max_bpb = 0;
    temp.max_fpb = 0;

    unsigned int i=0;
    for (; i<stats_vect.size(); i++)
    {
        temp.avg_bpb += stats_vect[i].avg_bpb;
        temp.avg_bpppb += stats_vect[i].avg_bpppb;
        temp.avg_fpb += stats_vect[i].avg_fpb;
        temp.max_bpb += stats_vect[i].max_bpb;
        temp.max_fpb += stats_vect[i].max_fpb;
    }

    temp.avg_bpb /= i;
    temp.avg_bpppb /= i;
    temp.avg_fpb /= i;
    temp.max_bpb /= i;
    temp.max_fpb /= i;

    avg_period_stat = temp; //save as avg
}

/**
 * Determine missing statistics periods and fill them with average period data.
 */
void netflow_statistics::fill_missing_stats()
{
    flow_time start;
    start.set_time(0,0,0);  //00:00:00
    flow_time end = start;
    unsigned int lastindex = 0;

    //move through periods
    do
    {
        end.add_time(parameters->statistics_period_hrs, parameters->statistics_period_mins, parameters->statistics_period_secs);

        bool found = false;
        for (unsigned int i=lastindex; i<stats_vect.size(); i++)    //search for period in existing periods
        {
            if (((nfstat)stats_vect[i]).periodstart.compare_with(start) == 0)
            {
                found = true;
                lastindex = i;
            }
            else if (((nfstat)stats_vect[i]).periodstart.compare_with(start) > 0)
                break;
        }

        if (! found)    //add missing period with avg stats data
        {
            nfstat missing = avg_period_stat;
            missing.periodstart = start;
            missing.periodend = end;
            stats_vect.push_back(missing);
        }

        start = end;

    } while (start.minutes != 0 && start.hours != 0);   //end when we again get to 00:00:00

    sort_statistic_data();  //sort periods data
}

/**
 * Get statistical data of certain period based on flow time.
 * Returns pointer to statistical data struct (if data of requested
 * period are not known, average period data are returned).
 */
nfstat *netflow_statistics::get_stat_data_for_period(flow_time flwtime)
{
    nfstat *ret = &avg_period_stat;

    bool found = false;
    unsigned int j=last_index;  //from latest period to the end and then from start to last period
    for (unsigned int i = 0; i<stats_vect.size(); i++)
    {
        if (((nfstat)stats_vect[j]).periodstart.compare_with(flwtime) <= 0 && ((nfstat)stats_vect[j]).periodend.compare_with(flwtime) >= 0)
        {
            found = true;
            break;
        }

        j = (last_index + i) % (stats_vect.size());
    }

    if (found)
    {
        last_index = j;
        return &stats_vect[j];
    }

    return ret;
}

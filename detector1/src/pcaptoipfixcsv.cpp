/**
 * pcaptoipfixcsv.cpp
 * This file implements simple converter from pcap to IPFIX csv.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    14.4.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pcap.h>
#include <mutex>
#include <algorithm>

#include "common.h"
#include "pcapparser.h"
#include "shared.h"

using namespace std;

const char usage_msg[] =
    "pcaptoipfixcsv: Creates IPFIX csv file from pcap file (for detectors testing).\n\n"
    "Usage:\n"
    "./pcaptoipfixcsv params\n"
    "params:\n"
    "\t-i <input_file> - input file (pcap)\n"
    "\t-o <output_file> - output file (csv)\n"
    "\t-h - prints this help message\n";

////////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES (FOR COMPATIBILITY WITH INCLUDED FILES)

//shared var with pointers to opened files for signal handling
opened_stuff opened_stuff_shared;

//shared queue with pointers to bins (for inter-thread data sharing between parser and detector1)
shared_queue_ip_storage shared_bin_pointers;

//shared processing summary msgs
shared_detectors_summary shared_summary;

//shared program arguments
params shared_parameters;

//shared mutex for locking cout when printing logs
mutex mutex_cout;

//stop all actions after signal catch
bool stop = false;

////////////////////////////////////////////////////////////////////////// STRUCTS

//program parameters
struct small_params
{
    string infile;
    string outfile;
};

////////////////////////////////////////////////////////////////////////// HELPER FUNCTIONS

/**
 * Parse program arguments.
 */
small_params parse_params(int argc, char **argv)
{
    small_params p;

    int param = 0;
    opterr = 0;     //disable implicit error msgs

    if (argc == 2 && (strcmp(argv[1], "-h")==0 || strcmp(argv[1], "--help")==0))
    {
        cerr << usage_msg;
        exit(0);
    }

    //parse args
    while((param = getopt(argc, argv, "i:o:")) != EOF)
    {
        string s = "";
        if (optarg)
            s = string(optarg);


        switch(param)
        {
            case 'i':
                p.infile = s;
                break;
            case 'o':
                p.outfile = s;
                break;

            default:
                err_exit_with_msg("Error: Unknown parameter! Use \"-h\" for help.");
        }
    }

    if (optind < argc)
        err_exit_with_msg("Error: Unknown parameter! Use \"-h\" for help");

    if (p.infile.empty() || p.outfile.empty())
        err_exit_with_msg("Error: Input or output file not specified! Use -i, -o!");

    return p;
}

/**
 * Save packet as one IPFIX record into output file
 */
void print_packet_to_outfile(ofstream *outfile, pkt *packet)
{
    if (outfile && packet)
    {
        //DST_IP,SRC_IP,BYTES,TIME_FIRST,TIME_LAST,DNS_RR_TTL,PACKETS,DNS_ANSWERS,DNS_QTYPE,DNS_RLENGTH,DST_PORT,SRC_PORT,DNS_RCODE,DNS_NAME,DNS_RDATA
        (*outfile) << get_dstIP_str(packet) << "," << get_srcIP_str(packet) << "," << packet->ipLen << ",";
        (*outfile) << packet->pktTime.get_date_time_str_ipfix_version() << "," << packet->pktTime.get_date_time_str_ipfix_version() << ",";
        //DNS_RDATA
        (*outfile) << packet->ttl << "," << 1 << "," << packet->rCount << "," << packet->qType << "," << packet->rdLen << ",";
        (*outfile) << packet->dstPort << "," << packet->srcPort << "," << (int)(packet->rCode) << ",\"" << packet->qName << "\",";

        string rdata = packet->rData;
        if (packet->msgType == DNS_TYPE_QUERY)
            rdata = "(query)";
        else
        {
            //replace endline chars
            replace(rdata.begin(), rdata.end(), '\r', '#');
            replace(rdata.begin(), rdata.end(), '\n', '#');
        }


        (*outfile) << "\"" << rdata << "\"" << endl;
    }
}

/**
 * Loop for loading packets and saving them...
 */
void main_loop(small_params *parameters)
{
    struct pcap_pkthdr pktheader;       //packet pcap header
    pcap_t *pcapfile_opened;            //opened pcap struct
    char error_buff[PCAP_ERRBUF_SIZE];  //buffer for err msgs
    pkt pack;                           //captured DNS packet structure

    ///open input pcap file
    pcapfile_opened = pcap_open_offline(parameters->infile.c_str(), error_buff);
    if (pcapfile_opened == NULL)
    {
        cerr << "Couldn't open input file " << parameters->infile << ": " << error_buff << endl;
        err_exit();
    }

    // open output csv file
    ofstream outfile(parameters->outfile);  //open output file
    if (! outfile.is_open())
    {
        cerr << "Couldn't create output file " << parameters->outfile << "!" << endl;
        err_exit();
    }

    //insert header
    outfile << "DST_IP,SRC_IP,BYTES,TIME_FIRST,TIME_LAST,DNS_RR_TTL,PACKETS,DNS_ANSWERS,DNS_QTYPE,DNS_RLENGTH,DST_PORT,SRC_PORT,DNS_RCODE,DNS_NAME,DNS_RDATA" << endl;

    //loop
    int retcode;
    pcap_parser parser(NULL);
    while ((retcode = parser.get_next_packet(&pack, pcapfile_opened, &pktheader)) != RET_EMPTY)
    {
        if (retcode == RET_OK)
        {
            //print packet as one IPFIX record
            print_packet_to_outfile(&outfile, &pack);
        }
    }

    pcap_close(pcapfile_opened);        //close input file
    outfile.close();                    //close output file
}

////////////////////////////////////////////////////////////////////////// FUNCTIONS


////////////////////////////////////////////////////////////////////////// PROGRAM MAIN

/**
 * Main - parse args & stuff
 */
int main(int argc, char *argv[])
{
    small_params parameters = parse_params(argc, argv);

    main_loop(&parameters);

    return 0;
}

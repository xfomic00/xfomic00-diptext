/**
 * netflowdetector2.h
 * This file defines class netflow_detector2.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    22.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
#include "ip_storage.h"

using namespace std;

#ifndef _NETFLOWDETECTOR2_H_
#define _NETFLOWDETECTOR2_H_

/////////////////////////////////////////////// CONSTANTS

//total number of single flow thresholds
#define NETFLOW_SINGLE_THRESHOLDS 4

//indexes in thresh_stats array for thresholds usage statistics
#define NETFLOW_THRESH_BPF_POS 0
#define NETFLOW_THRESH_PPF_POS 1
#define NETFLOW_THRESH_PACK_SIZE_POS 2
#define NETFLOW_THRESH_FLOW_DURATION 3

/////////////////////////////////////////////// CLASSES

/**
 * netflow_detector2 - class with advanced netflow based detector of DNS tunneling.
 */
class netflow_detector2
{
  public:
    netflow_detector2(params *pars);    //constructor
    ~netflow_detector2();               //destructor

    void process_queue();                   //process bins
    bool detect(flow *flw); //inspect flow
    void detect_in_bin(ip_storage *bin_to_inspect);   //inspect bin data
    void inspect_aggregated_bins(ofstream *file);                     //inspect aggregated data

    void put_ip_data_in_map(flow *flw);     //store flow data for further analysis

    void delete_finished_bin(); //free allocated bin memory

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments
    unsigned int total_bins;//number of bins inspected
    unsigned int stored_bins;   //number of bins actually aggregated
    unsigned int total_detections;  //number of detected flows
    unsigned long total_flows_analyzed;  //number of analyzed flows
    unsigned long total_flows_processed; //number of ispected flows
    flow_time last_bin_end; //time of last bin end

    unsigned long thresh_stats[NETFLOW_SINGLE_THRESHOLDS];  //statistics for thresholds usage

    ip_storage *bin;        //active bin pointer
    ip_storage ipmap;       //internal aggregation map
};

#endif

/**
 * pcapdetector1.h
 * This file defines class pcap_detector1.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    13.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <pcap.h>
#include <stdlib.h>
#include <string>
#include <unordered_map>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include "common.h"
#include "ip_storage.h"

using namespace std;

#ifndef _PCAPDETECTOR1_H_
#define _PCAPDETECTOR1_H_

/////////////////////////////////////////////// CONSTANTS

//total number of single flow thresholds
#define PCAP_SINGLE_THRESHOLDS 7

//indexes in thresh_stats array for thresholds usage statistics
#define PCAP_THRESH_PACK_SIZE_POS 0
#define PCAP_THRESH_TTL_POS 1
#define PCAP_THRESH_RDLEN_POS 2
#define PCAP_THRESH_QUERY_SIZE_POS 3
#define PCAP_THRESH_QNAME_LEN_POS 4
#define PCAP_THRESH_SUBDOMAIN_LEN_POS 5
#define PCAP_THRESH_TOO_MUCH_ASCII_POS 6

/////////////////////////////////////////////// CLASSES & STRUCTS


/**
 * pcap_detector1 - class with simple pcap based detector of DNS tunneling.
 */
class pcap_detector1
{
  public:
    pcap_detector1(params *pars);   //constructor
    ~pcap_detector1();      //destructor

    void process_queue();    //process bins
    void detect_in_bin(ip_storage *bin_to_inspect);   //inspect bin data
    bool detect(struct pkt *packet);                                  //inspect single packet
    void inspect_aggregated_bins(ofstream *file);                     //inspect aggregated data

    void put_ip_data_in_map(pkt *packet);   //store packet data for further analysis
    void delete_finished_bin();             //delete used bin

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments

    ip_storage *bin;    //active bin pointer
    ip_storage ipmap;  //map for aggregating data

    unsigned int total_bins;    //number of bins processed
    unsigned int stored_bins;   //number of bins actually aggregated
    unsigned int total_detections;  //number of detections
    unsigned long total_pkts_analyzed;  //number of analyzed packets
    unsigned long total_pkts_processed; //number of ispected packets
    unsigned long total_pkts_whitelisted; //number of whitelisted packets

    unsigned long thresh_stats[PCAP_SINGLE_THRESHOLDS];  //statistics for thresholds usa
};

#endif

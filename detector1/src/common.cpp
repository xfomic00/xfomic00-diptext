/**
 * common.cpp
 * This file implements common functions.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    13.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include "common.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <stdio.h>
#include <string>
#include <cctype>

#include "shared.h"

using namespace std;

////////////////////////////////////////////////////////////// FLOW_TIME METHODS

/**
 * Konstruktor alternative
 */
void flow_time::init_flow_time(string t, string durat)
{
    bool err = false;
    err = ! parse_netflow_time(t);

    duration = get_double_from_string(durat, &err);
    if (err)
        cerr << "Warning: Unable to convert flow duration \"" << durat << "\"!" << endl;
}

/**
 * Parse time information from string (nfdump CSV string data).
 * Expected format: "2015-02-03 22:04:08"
 * Returns true if ok, false otherwise.
 */
bool flow_time::parse_netflow_time(string t)
{
    try
    {
        date = t.substr(0, DATE_LENGTH);
        t = t.erase(0, FLOW_TIME_START_POS);
    }
    catch (const std::out_of_range& e)
    {
        cerr << "in parse_netflow_time: " << t << endl;
    }

    return parse_time(t);  //error not expected
}

/**
 * Parse time information from string (ipfix CSV string data).
 * Expected format: "2015-03-17T16:16:50.215"
 * Returns true if ok, false otherwise.
 */
bool flow_time::parse_ipfix_time(string t)
{
    try
    {
        date = t.substr(0, DATE_LENGTH);
        t = t.erase(0, IPFIX_TIME_START_POS);   //erase date
        t = t.erase(TIME_LENGTH);   //msec
    }
    catch(const std::out_of_range& e)
    {
        cerr << "in parse_ipfix_time: " << t << endl;
    }

    duration = 0;

    return parse_time(t);
}

/**
 * Parse time information from string.
 * Expected format: "22:04:08"
 * returns true if ok, false if coversion failed
 */
bool flow_time::parse_time(string t)
{
    if (t.length() != TIME_LENGTH)
    {
        cerr << "Error: Unknown time format! Expected hh:mm:ss" << endl;
        return false;
    }

    bool err = false;

    hours = (u_int8_t) get_int_from_str(t.substr(0,2).c_str(), &err);
    minutes = (u_int8_t) get_int_from_str(t.substr(3,2).c_str(), &err);
    seconds = (u_int8_t) get_int_from_str(t.substr(6,2).c_str(), &err);

    if (err)
        cerr << "Error: Unable to convert time data \"" << t << "\"!" << endl;
    else
        is_valid = true;

    return (! err);
}

/**
 * Get string representation of time.
 */
string flow_time::get_time_str()
{
    ostringstream ret;
    ret << dec << setw(2) << setfill('0') << unsigned(hours);
    ret << ":";
    ret << setw(2) << unsigned(minutes);
    ret << ":";
    ret << setw(2) << unsigned(seconds);

    return ret.str();
}

/**
 * Get string representation of time and date.
 */
string flow_time::get_date_time_str()
{
    return (date + " " + get_time_str());
}

/**
 * Get string representation of time and date in IPFIX csv format.
 */
string flow_time::get_date_time_str_ipfix_version()
{
    //2015-03-17T16:16:50.215
    return (date + "T" + get_time_str() + ".000");
}

/**
 * Set date manually
 */
void flow_time::set_date(u_int16_t y, u_int8_t m, u_int8_t d)
{
    //make format "2015-02-03"

    //slow way
    /*
    ostringstream str;
    str << setw(4) << setfill('0') << y;
    str << "-";
    str << setw(2) << setfill('0') << unsigned(m);
    str << "-";
    str << setw(2) << setfill('0') << unsigned(d);

    date = str.str();
    */

    //faster way
    if (m > 12)
        m = 12;
    if (d > 31)
        d = 31;
    if (y > 9999)
        y = 9999;

    char strbuff[11];
    snprintf(strbuff, sizeof(strbuff), "%04d-%02d-%02d", y, m, d);
    strbuff[10] = 0;

    string str = string(strbuff);
    date = str;
}

/**
 * Set time manually
 */
void flow_time::set_time(u_int8_t h, u_int8_t m, u_int8_t s)
{
    hours = h;
    minutes = m;
    seconds = s;
    is_valid = true;
}

/**
 * Add to this time.
 */
void flow_time::add_time(u_int8_t h, u_int8_t m, u_int8_t s)
{
    if (hours + h >= 24)
    {
        add_hours_to_date(hours + h + 1 - 24);
        hours = (hours + h) % 24;
    }
    else
        hours = (hours + h) % 24;

    if (seconds + s >= 60)
    {
        u_int8_t mins = (seconds + s) / 60;
        minutes += mins;
        seconds = (seconds + s) % 60;
    }
    else
        seconds += s;

    if (minutes + m >= 60)
    {
        u_int8_t hrs = (minutes + m) / 60;
        if (hours + hrs >= 24)
        {
            add_hours_to_date(hours + hrs + 1 - 24);
            hours = (hours + hrs) % 24;
        }
        else
            hours = (hours + hrs) % 24;

        minutes = (minutes + m) % 60;
    }
    else
        minutes += m;
}

/**
 * Add saved duration to this time.
 */
void flow_time::add_duration_this()
{
    if (duration > 0)
    {
        double temp = duration;
        u_int8_t h, m, s;
        h = (int)temp / 3600;
        temp -= h*3600;
        m = (int)temp / 60;
        temp -= m*60;
        s = (int)temp;

        add_time(h, m, s);  //add duration to this time
    }
}

/**
 * Add hours to date (0-24 hours => +1 day)
 */
void flow_time::add_hours_to_date(unsigned int hrs)
{
    if (date.compare("0000-00-00") == 0)
        return;

    //"YYYY-MM-DD"
    bool err = false;
    try
    {
        int days = get_int_from_str(date.substr(DATE_LENGTH - 2, 2).c_str(), &err);
        int months = get_int_from_str(date.substr(DATE_LENGTH - 5, 2).c_str(), &err);
        int years = get_int_from_str(date.substr(0, 4).c_str(), &err);

        unsigned int total_days = hrs/24 + 1;

        if (days + total_days > 31)         //lame solution
            months += (days+total_days)/31;

        days = (days + total_days) % 31;

        if (months > 12)
        {
            years += months / 12;
            months = months % 12;
        }

        if (! err)
            set_date((u_int16_t) years, (u_int8_t) months, (u_int8_t) days);
    }
    catch (const std::out_of_range& e)
    {
        cerr << "in add_hours_to_date" << endl;
    }
}

/**
 * Compare with other time.
 * Returns val < 0 if this < t
 *         val > 0 if this > t
 *         val = 0 if this == t
 */
int flow_time::compare_with(flow_time t)
{
    bool check_date = true;
    if (date.compare("0000-00-00") == 0)
        check_date = false;

    //date
    if (check_date && t.date > date)
        return -1;
    else if (check_date && t.date < date)
        return 1;
    else //if (t.date == date)
    {   //time
        if (t.hours == hours)
        {
            if (t.minutes == minutes)
            {
                if (t.seconds == seconds)
                    return 0;
                else if (t.seconds > seconds)
                    return -1;
                else //if (t.seconds < seconds)
                    return 1;
            }
            else if (t.minutes > minutes)
                return -1;
            else //if (t.minutes < minutes)
                return 1;
        }
        else if (t.hours > hours)
            return -1;
        else //if (t.hours < hours)
            return 1;
    }
}

////////////////////////////////////////////////////////////// OTHER METHODS & FUNCTIONS

/**
 * Print error message
 */
void print_err(const char *error_str)
{
    cerr << error_str << endl;
}

/**
 * Exit app with err code.
 */
void err_exit()
{
    exit(ERR);
}

/**
 * Print error message and exit app with err code.
 */
void err_exit_with_msg(const char *error_str)
{
    print_err(error_str);
    err_exit();
}

/**
 * Extract int from char *.
 */
int get_int_from_str(const char *str, bool *err)
{
    //slow version
    /**
    stringstream ss;
    ss << str;

    int val = 0;
    ss >> val;

    if (ss.fail() || (! ss.eof()))
        *err = true;

    return val;
    */

    //fast version
    char *end;
    long  l;
    errno = 0;

    l = strtol(str, &end, 10);

    if ((errno == ERANGE) || *str == '\0' || *end != '\0')
        *err = true;

    return (int) l;
}

/**
 * Extract double from string.
 */
double get_double_from_string(string str, bool *err)
{
    double ret = 0;

    try
    {
        ret = stod(str, NULL);
    }
    catch (exception e)
    {
        *err = true;
    }

    return ret;
}

/**
 * Run system command and return its output.
 */
string system_cmd(const char *cmd)
{
    string output;

    if (cmd)
    {
        FILE *commandpipe = popen(cmd, "r");    //process pipe
        if (! commandpipe)
            return "ERR";

        char buff[CMD_BUFFER_SIZE];
        while(! feof(commandpipe))  //get output
        {
            if (fgets(buff, CMD_BUFFER_SIZE, commandpipe) != NULL)
                output.append(buff);
        }

        pclose(commandpipe);
    }

    return output;
}

/**
 * Parse detection parameters from configuration file.
 * Returns true if success, false otherwise
 */
bool parse_config_file(char *filename, params *p)
{
    bool err = false;
    bool bin_time = false;
    bool bin_size = false;
    int i=0;

    ifstream infile(filename);    //open input file
    if (! infile.good())
    {
        cerr << "Error: Couldn't open configuration file " << filename << "!" << endl;
        err_exit();
    }

    opened_stuff_shared.opened_infile = &infile;

    string line;
    while (getline(infile, line))
    {
        line.erase(remove_if(line.begin(), line.end(), remove_delim()), line.end());    //erase whitespace chars

        if (line.length() == 0 || line[0] == '#') //skip comments and blank lines
            continue;
        else
        {
            i++;
            config_parameter par = parse_config_name(line);

            if (par.name.compare("bin_time") == 0)
            {
                flow_time t;
                if (! t.parse_time(par.value))
                    err = true;

                p->bin_size_hrs = t.hours;
                p->bin_size_mins = t.minutes;
                p->bin_size_secs = t.seconds;

                bin_time = true;
            }
            else if (par.name.compare("statistics_period") == 0)
            {
                flow_time t;
                if (! t.parse_time(par.value))
                    err = true;

                if (t.hours == 0 && t.minutes == 0)
                {
                    err = true;
                    cerr << "Error: statistical period must be at least one minute!" << endl;
                }

                p->statistics_period_hrs = t.hours;
                p->statistics_period_mins = t.minutes;
                p->statistics_period_secs = t.seconds;
            }
            else if (par.name.compare("num_of_sorting_bins") == 0)
            {
                p->num_of_sorting_bins = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("dns_extract_domain_level") == 0)
            {
                p->dns_extract_domain_level = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("filter_common_qtypes") == 0)
            {
                p->filter_common_qtypes = (par.value.compare("1") == 0) ? true : false;
            }
            else if (par.name.compare("max_aggregated_bins") == 0)
            {
                p->max_aggregated_bins = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("bin_packets") == 0)
            {
                p->bin_packets = get_int_from_str(par.value.c_str(), &err);
                bin_size = true;
            }
            else if (par.name.compare("stat_bpb_thresh") == 0)
            {
                p->thresh_stat_avg_bpb = get_double_from_string(par.value, &err);
            }
            else if (par.name.compare("stat_fpb_thresh") == 0)
            {
                p->thresh_stat_avg_fpb = get_double_from_string(par.value, &err);
            }
            else if (par.name.compare("stat_bpppb_thresh") == 0)
            {
                p->thresh_stat_avg_bpppb = get_double_from_string(par.value, &err);
            }
            else if (par.name.compare("thresh_bpppb_thresh") == 0)
            {
                p->thresh_thresh_bpppb = get_double_from_string(par.value, &err);
            }
            else if (par.name.compare("packet_size_thresh") == 0)
            {
                p->packet_size_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("bpf_thresh") == 0)
            {
                p->bpf_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("ppf_thresh") == 0)
            {
                p->ppf_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("query_packet_size_thresh") == 0)
            {
                p->query_packet_size_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("packet_rdata_size_thresh") == 0)
            {
                p->packet_rdata_size_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("single_flow_duration") == 0)
            {
                p->single_flow_duration = get_double_from_string(par.value.c_str(), &err);
            }
            else if (par.name.compare("packet_ttl_thresh") == 0)
            {
                p->packet_ttl_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("detect_only_communication_from_client") == 0)
            {
                p->detect_only_client_communication = (get_int_from_str(par.value.c_str(), &err)) ? true : false;
            }
            else if (par.name.compare("map_minimum_conditions") == 0)
            {
                p->map_minimum_conditions = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("map_minimum_most_used_domain_count") == 0)
            {
                p->map_minimum_most_used_domain_count = get_int_from_str(par.value.c_str(), & err);
            }
            else if (par.name.compare("occurences_in_map_thresh") == 0)
            {
                p->occurences_in_map_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("map_minimum_packets") == 0)
            {
                p->map_minimum_packets = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("avg_size_map_thresh") == 0)
            {
                p->avg_size_map_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("total_bytes_map_thresh") == 0)
            {
                p->total_bytes_map_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("total_queries_map_thresh") == 0)
            {
                p->total_queries_map_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("total_bytes_queries_only_map_thresh") == 0)
            {
                p->total_bytes_queries_only_map_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("avg_namelen_map_thresh") == 0)
            {
                p->avg_namelen_map_thresh = get_int_from_str(par.value.c_str(), &err);
            }
            else if (par.name.compare("d1_detection_schema") == 0)
            {
                if (par.value.compare("statistics") == 0)
                    p->d1_detection_schema = D1_DETECTION_SCHEMA_STATISTICS;
                else if (par.value.compare("bin_thresholds") == 0)
                    p->d1_detection_schema = D1_DETECTION_SCHEMA_BIN_THRESHOLDS;
            }
            else if (par.name.compare("d2_detection_schema") == 0)
            {
                if (par.value.compare("aggregate_all") == 0)
                    p->d2_detection_schema = D2_DETECTION_SCHEMA_AGGREGATE_ALL;
                else if (par.value.compare("aggregate_single") == 0)
                    p->d2_detection_schema = D2_DETECTION_SCHEMA_AGGREGATE_SINGLE;
                else if (par.value.compare("aggregate_suspicious_qtypes") == 0)
                    p->d2_detection_schema = D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES;
                else if (par.value.compare("aggregate_suspicious_qtypes_thresholds") == 0)
                    p->d2_detection_schema = D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES_THRESHOLDS;
            }
        }

        if (err)
        {
            cerr << "Error in configuration file on line " << i << "!" << endl;
            break;
        }
        else if (bin_time && bin_size)
        {
            cerr << "Error: cant have both params bin_packets & bin_time active at same time!" << endl;
            err = true;
            break;
        }
    }

    infile.close();
    opened_stuff_shared.opened_infile = NULL;

    return (! err);
}

/**
 * Parse whitelist domains from whitelist file
 * Returns true if success, false otherwise
 */
bool parse_whitelist_file(char *filename, params *p)
{
    bool err = false;
    int i=0;

    ifstream infile(filename);    //open input file
    if (! infile.good())
    {
        cerr << "Error: Couldn't open whitelist file " << filename << "!" << endl;
        err_exit();
    }

    opened_stuff_shared.opened_infile = &infile;

    string line;
    while (getline(infile, line))
    {
        line.erase(remove_if(line.begin(), line.end(), remove_delim()), line.end());    //erase whitespace chars

        if (line.length() == 0 || line[0] == '#') //skip comments and blank lines
            continue;
        else
        {
            i++;
            p->whitelist_domains.push_back(line); //save domain
        }

        if (err)
        {
            cerr << "Error in whitelist file on line " << i << "!" << endl;
            break;
        }
    }

    infile.close();
    opened_stuff_shared.opened_infile = NULL;

    return (! err);
}

/**
 * Extract configuration parameter name from str.
 * Returns extracted config parameter name.
 */
config_parameter parse_config_name(string line)
{
    //expected format: name=val

    config_parameter ret;

    string::size_type pos = line.find('=');
    ret.name = line.substr(0, pos);
    ret.value = line.substr(pos+1);

    return ret;
}

/**
 * Create new config file from config file template.
 */
void create_config_file(char *filename)
{
    ofstream outfile(filename);  //open output file
    if (! outfile.is_open())
    {
        cerr << "Couldn't create config file " << filename << "!" << endl;
        err_exit();
    }

    opened_stuff_shared.opened_outfile = &outfile;

    outfile << def_config_file; //print default config file into file

    outfile.close();
    opened_stuff_shared.opened_outfile = NULL;
}

/**
 * Create new whitelist file from whitelist file template.
 */
void create_whitelist_file(char *filename)
{
    ofstream outfile(filename);  //open output file
    if (! outfile.is_open())
    {
        cerr << "Couldn't create whitelist file " << filename << "!" << endl;
        err_exit();
    }

    opened_stuff_shared.opened_outfile = &outfile;

    outfile << def_whitelist_file; //print default whitelist file into file

    outfile.close();
    opened_stuff_shared.opened_outfile = NULL;
}

/**
 * Extract domain name of nth level from full DNS query.
 * Returns extracted nth level domain name.
 */
string extract_domain_name_to_nth_level(string domain, unsigned int level)
{
    string::size_type pos = domain.length() - 1;
    unsigned int count = 0;

    for (unsigned int i=0; i<level; i++)
    {
        //search from end of str
        string::size_type findpos = domain.rfind('.', pos);
        if (findpos == string::npos)
            break;
        else
            count++;

        pos = findpos - 1;
    }

    if (count >= level)
    {
        try
        {
            domain.erase(0, pos+2); //end of cycle fix + skip last dot
        }
        catch(const std::out_of_range& e)
        {
            cerr << "in extract_domain_name_to_nth_level: " << domain << endl;
        }
    }

    return domain;
}

/**
 * Convert DNS query type from code (int) to text.
 */
const char *dns_type_to_text(int type)
{
    switch(type)
    {
        case QTYPE_A:
            return "A";
        case QTYPE_NS:
            return "NS";
        case QTYPE_CNAME:
            return "CNAME";
        case QTYPE_NULL:
            return "NULL";
        case QTYPE_PTR:
            return "PTR";
        case QTYPE_MX:
            return "MX";
        case QTYPE_TXT:
            return "TXT";
        case QTYPE_AAAA:
            return "AAAA";
        case QTYPE_SIG:
            return "SIG";
        case QTYPE_KEY:
            return "KEY";
        case QTYPE_RRSIG:
            return "RRSIG";
        case QTYPE_DNSKEY:
            return "DNSKEY";
        case QTYPE_SRV:
            return "SRV";
        case DNS_TYPE_MULTIPLE:
            return "Multiple types";
        default:   //uninteresting
            return "OTHER";
    }

    return "";
}

/**
 * Check if str contains too much non-ascii data (unprintable characters).
 * Threshold is specified in DNS_QUERIES_MAX_NON_ASCII_RATIO
 */
bool contains_too_much_non_ascii_data(string str)
{
    int nonascii = 0;
    for (unsigned int i=0; i<str.length(); i++)
    {
        if (! isprint(str[i]))
            nonascii++;
        else    //check forbidden chars array
        {
            char forbidden[] = "[]{}()<>$!':#*,%/&`´ˇ§¨-?^+=|@;;°~";
            if (strchr(forbidden, str[i]) != NULL)
                nonascii++;
        }
    }

    if (nonascii == 0)
        return false;

    double ratio = (double)nonascii/(double)str.length();

    return (ratio > DNS_QUERIES_MAX_NON_ASCII_RATIO) ? true : false;
}

/**
 * Check if domain name has subdomains of same length.
 */
bool subdomains_have_same_len(string domain)
{
    const unsigned int MIN_LEN = 5;  //minimum subdomain len

    size_t len1 = 0, len2 = 0, pos1 = 0;
    size_t pos2 = domain.find('.', 0);

    while(pos2 != string::npos)
    {
        len1 = pos2 - pos1;
        if (len1 == len2 && len1 > MIN_LEN)
            return true;

        pos1 = pos2 + 1;
        len2 = len1;
        pos2 = domain.find('.', pos2 + 1);
    }

    return false;
}

/**
 * Check if flow has common DNS query type (A, AAAA, PTR).
 */
bool has_common_qtype(ipfix_flow *flw)
{
    if (flw && (flw->qType == QTYPE_A || flw->qType == QTYPE_AAAA || flw->qType == QTYPE_PTR))
        return true;

    return false;
}

/**
 * Check if packet has common DNS query type (A, AAAA, PTR).
 */
bool has_common_qtype(pkt *pack)
{
    if (pack && (pack->qType == QTYPE_A || pack->qType == QTYPE_AAAA || pack->qType == QTYPE_PTR))
        return true;

    return false;
}

/**
 * Get string representation of IPv4 address.
 */
string get_ipv4_str(in_addr ip)
{
    string ret = string(inet_ntoa(ip));
    return ret;
}

/**
 * Get string representation of IPv6 address.
 */
string get_ipv6_str(in6_addr ip)
{
    string ret;
    char buffer[INET6_ADDRSTRLEN + 1];

    if (inet_ntop(AF_INET6, &ip, buffer, INET6_ADDRSTRLEN))
    {
        ret = string(buffer);
    }

    return ret;
}

/**
 * Get string with src IP address (IP/IPv6 selection).
 */
string get_srcIP_str(flow *flw)
{
    if (flw->ipv6)
        return get_ipv6_str(flw->srcIP.v6);
    else
        return get_ipv4_str(flw->srcIP.v4);
}

/**
 * Get string with src IP address (IP/IPv6 selection).
 */
string get_srcIP_str(ipfix_flow *flw)
{
    if (flw->ipv6)
        return get_ipv6_str(flw->srcIP.v6);
    else
        return get_ipv4_str(flw->srcIP.v4);
}

/**
 * Get string with src IP address (IP/IPv6 selection).
 */
string get_srcIP_str(pkt *pack)
{
    if (pack->ipv6)
        return get_ipv6_str(pack->srcIP.v6);
    else
        return get_ipv4_str(pack->srcIP.v4);
}

/**
 * Get string with dst IP address (IP/IPv6 selection).
 */
string get_dstIP_str(flow *flw)
{
    if (flw->ipv6)
        return get_ipv6_str(flw->dstIP.v6);
    else
        return get_ipv4_str(flw->dstIP.v4);
}

/**
 * Get string with dst IP address (IP/IPv6 selection).
 */
string get_dstIP_str(ipfix_flow *flw)
{
    if (flw->ipv6)
        return get_ipv6_str(flw->dstIP.v6);
    else
        return get_ipv4_str(flw->dstIP.v4);
}

/**
 * Get string with dst IP address (IP/IPv6 selection).
 */
string get_dstIP_str(pkt *pack)
{
    if (pack->ipv6)
        return get_ipv6_str(pack->dstIP.v6);
    else
        return get_ipv4_str(pack->dstIP.v4);
}


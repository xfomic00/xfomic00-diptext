/**
 * netflowparser.h
 * This file defines class netflow_parser.
 *
 * Nfdump binary files are parsed with "nfreader" library, which can be obtained here:
 * http://sourceforge.net/projects/hoststats/files/nfreader/
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    25.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
//#include "nfreader.h"
#include "libnfdump.h"
#include "ip_storage.h"
#include "netflowstatistics.h"

using namespace std;

#ifndef _NETFLOWPARSER_H_
#define _NETFLOWPARSER_H_

/////////////////////////////////////////////// CONSTANTS

//struct for saving positions of CSV columns
typedef struct
{
    unsigned int TIME_START;
    unsigned int TIME_END;
    unsigned int TIME_DURATION;
    unsigned int SRC_IP;
    unsigned int DST_IP;
    unsigned int SRC_PORT;
    unsigned int DST_PORT;
    unsigned int PROTOCOL;
    unsigned int IN_PKTS;
    unsigned int IN_BYTES;
    unsigned int OUT_PKTS;
    unsigned int OUT_BYTES;
} netflow_csv_header;

/////////////////////////////////////////////// CLASSES

/**
 * netflow_parser - class for parsing netflow file(s), automatically handles opening of files, parsing records, etc.
 * Supports csv and binary formats.
 */
class netflow_parser
{
  public:
    netflow_parser(params *par);  //constructor
    ~netflow_parser();

    void get_file_list();   //get list of all input files

    flow *get_next_flow(int *err, bool parse_only_statistics);  //read and parse next flow record

    void fill_header(CSVRow *row);  //fill header struct

    bool parse_flow_csv_fields(CSVRow *flowdata, flow *flwdest, bool parse_only_statistics);    //parse readed flow data into flow struct
    bool parse_flow_binary_fields(master_record_t *flowdata, flow *flwdest, bool parse_only_statistics);    //extract only useful data from master_record_t

    int open_infile();
    int open_infile_csv();
    int open_infile_binary();

    void close_infile();

  private:
    params *parameters; //program arguments
    bool first_done;    //if first line of file has been skipped
    unsigned long long counter;    //flow counter

    flow flw;   //flow record

    netflow_csv_header header;  //struct for saving CSV column indexes

    vector<string> infiles; //list of all input files
    ifstream infile_csv;    //input file (csv format)
    nfdump_iter_t infile_binary;    //binary input file handle
    bool infile_binary_opened;  //if infile binary is opened
};

/**
 * netflow_bin_maker - class for organising netflow data into bins.
 */
class netflow_bin_maker
{
  public:
    netflow_bin_maker(params *pars);   //constructor
    ~netflow_bin_maker();   //destructor

    void parse_infile();    //process input file

    static void print_flow(flow *flw);       //print flow info (debug function)
    static void save_to_file(ofstream *file, flow *flw);   //save flow info into file

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments

    unsigned long total_bins;               //total bins created
    unsigned long long total_flws_processed;//total packets processed
    unsigned long long total_flws_filtered; //total DNS packets
};

#endif

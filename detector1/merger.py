#!/usr/bin/env python

########################################################################################
# PROJECT: Effective detection of network anomaly using DNS data (Master thesis).
# THIS: Merger for merging attack data converted to CSV with normal data in other CSV.
# DATE: 2015-04-15
#
# AUTHOR: Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
########################################################################################

import sys
import re
import argparse
import os.path
import csv
import time
from datetime import datetime
from datetime import timedelta

######################################################################################## CHECKS

#check if file exists, open it
def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return open(arg, 'r')  # return an open file handle

#check if time string is valid    
def is_valid_time(parser, arg):
    try:
        time.strptime(arg, '%H:%M:%S')
        return arg
    except ValueError:
        parser.error("Time is not in valid format!")

######################################################################################## HELPER FUNCTIONS

#get time string (hh:mm:ss) from ipfix-style date+time str
def get_time_ipfix(timedateStr):
    #format: 2015-03-17T16:16:49.068
    return timedateStr[11:19]

#get date string (yyyy-mm-dd) from ipfix-style date+time str
def get_date_ipfix(timedateStr):
    #format: 2015-03-17T16:16:49.068
    return timedateStr[:10]

#get time string (hh:mm:ss) from netflow-style date+time str
def get_time_netflow(timedateStr):
    #format: 2015-02-21 23:59:41
    return timedateStr[11:19]

#get date string (yyyy-mm-dd) from netflow-style date+time str
def get_date_netflow(timedateStr):
    #format: 2015-02-21 23:59:41
    return timedateStr[:10]


#replace original timedate with new specified time and date (IPFIX version)
#date = new date
#newtimestart = attack time start specified by program parameter
#firsttime = original time of first merge record
#oldtime = original time from current merge record
def replace_time_ipfix(date, newtimestart, firsttime, oldtime):
    formatstr = '%H:%M:%S'
    difft = datetime.strptime(oldtime, formatstr) - datetime.strptime(firsttime, formatstr)
    
    newtime = datetime.strptime(newtimestart, formatstr) + difft
    
    return date + "T" + newtime.strftime(formatstr) + ".000";

#replace original timedate with new specified time and date (NetFlow version)
def replace_time_netflow(date, newtimestart, firsttime, oldtime):
    formatstr = '%H:%M:%S'
    difft = datetime.strptime(oldtime, formatstr) - datetime.strptime(firsttime, formatstr)
    
    newtime = datetime.strptime(newtimestart, formatstr) + difft
    
    return date + " " + newtime.strftime(formatstr);

######################################################################################## MERGE LOOP

#merge files
def merge(filetype, startTime, infile, mergefile, outfile):
    #create readers
    inreader = csv.reader(infile, quoting=csv.QUOTE_NONE)
    mergereader = csv.reader(mergefile, quoting=csv.QUOTE_NONE)
    
    #create writer
    ofile  = open(outfile, "w")
    outwriter = csv.writer(ofile, delimiter=',', quoting=csv.QUOTE_NONE, quotechar='')
    
    #initialization
    header = inreader.next()
    headercols = len(header)
    inrecord = inreader.next()
    mergerecord = mergereader.next()
    outwriter.writerow(header)
    
    if (filetype == "ipfix"):
        time_first_index = header.index('TIME_FIRST')
        time_last_index = header.index('TIME_LAST')
        get_time = get_time_ipfix
        get_date = get_date_ipfix
        replace_time = replace_time_ipfix
        mergerecord = mergereader.next()
    elif (filetype == "netflow"):
        time_first_index = header.index('ts')
        time_last_index = header.index('te')
        get_time = get_time_netflow
        get_date = get_date_netflow
        replace_time = replace_time_netflow
        mergerecord = mergereader.next()
        
    firstTime = get_time(mergerecord[time_first_index]) #time of first record from merge file
    firstDate = get_date(inrecord[time_last_index])    #date of first record from input file
    mergerecord[time_first_index] = replace_time(firstDate, startTime, firstTime, get_time(mergerecord[time_first_index]))
    mergerecord[time_last_index] = replace_time(firstDate, startTime, firstTime, get_time(mergerecord[time_last_index]))
    
    #print replace_time_ipfix(firstDate, startTime, "14:40:11", "14:40:52")
    
    #merge loop
    while(1):
        #outwriter.writerow(inrecord)
        #outwriter.writerow(mergerecord)
        
        #get date+time for actual records
        intime = inrecord[time_last_index]
        mergetime = mergerecord[time_last_index]
        
        #select record from infile or mergefile based on record time    
        if (intime <= mergetime):   #in record first
            outwriter.writerow(inrecord)
            
            try: 
                inrecord = inreader.next() #get next record
                
                if (filetype == "netflow") and (len(inrecord) < headercols): #skip summary
                    raise StopIteration
                    
            except csv.Error:
                sys.stderr.write("CSV input file format error!")
            except StopIteration:
                outwriter.writerow(mergerecord)
                for rec in mergereader:
                    if (len(rec) < headercols):
                        break
                    #replace original time+date with new time+date
                    rec[time_first_index] = replace_time(firstDate, startTime, firstTime, get_time(rec[time_first_index]))
                    rec[time_last_index] = replace_time(firstDate, startTime, firstTime, get_time(rec[time_last_index]))
    
                    outwriter.writerow(rec) #add rest of mergefile records
                
                break
            
        elif (intime > mergetime):  #merge record first
            outwriter.writerow(mergerecord)
        
            try: 
                mergerecord = mergereader.next()
                
                if (filetype == "netflow") and (len(mergerecord) < headercols): #skip summary
                    raise StopIteration  
                
                #replace original time+date with new time+date
                mergerecord[time_first_index] = replace_time(firstDate, startTime, firstTime, get_time(mergerecord[time_first_index]))
                mergerecord[time_last_index] = replace_time(firstDate, startTime, firstTime, get_time(mergerecord[time_last_index]))
                    
            except csv.Error:
                sys.stderr.write("CSV merge file format error!")
            except StopIteration:
                outwriter.writerow(inrecord)
                for rec in inreader:
                    if (len(rec) < headercols):
                        break
                    outwriter.writerow(rec) #add rest of infile records
                
                break
    
    #close files
    infile.close()
    mergefile.close()
    ofile.close()

######################################################################################## MAIN

# MAIN
if __name__ == "__main__":
    #parse program parameters
    argparser = argparse.ArgumentParser(description='Merge IPFIX/NetFlow CSV files together.')
    group = argparser.add_mutually_exclusive_group()
    group.add_argument("-x", "--ipfix", action="store_true")
    group.add_argument("-n", "--netflow", action="store_true")
    argparser.add_argument("-i", dest="infile", required=True, help="Input file with normal traffic", metavar="FILE", type=lambda x: is_valid_file(argparser, x))
    argparser.add_argument("-m", dest="mergefile", required=True, help="Input file with attack traffic (to be merged with normal file)", metavar="FILE", type=lambda x: is_valid_file(argparser, x))
    argparser.add_argument("-o", dest="outfile", required=True, help="Output merged file", metavar="FILE")
    argparser.add_argument("-t", dest="time", required=True, help="New starting time of mergefile records (attack) in hh:mm:ss format", metavar="hh:mm:ss", type=lambda x: is_valid_time(argparser, x))
    args = argparser.parse_args()
    
    if (args.ipfix):
        merge("ipfix", args.time, args.infile, args.mergefile, args.outfile)
    elif (args.netflow):
        merge("netflow", args.time, args.infile, args.mergefile, args.outfile)
    else:
        sys.stderr.write("You must select format: IPFIX/NetFlow!\n")
        sys.exit(1)
    

/**
 * netflowdetector1.h
 * This file defines class netflow_detector1.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    18.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
#include "ip_storage.h"
#include "netflowstatistics.h"

using namespace std;

#ifndef _NETFLOWDETECTOR1_H_
#define _NETFLOWDETECTOR1_H_

/////////////////////////////////////////////// CONSTANTS

//total number of single flow thresholds
#define NETFLOW1_SINGLE_THRESHOLDS 3

//indexes in thresh_stats array for thresholds usage statistics
#define NETFLOW1_THRESH_BPB_POS 0
#define NETFLOW1_THRESH_FPB_POS 1
#define NETFLOW1_THRESH_BPPPB_POS 2

/////////////////////////////////////////////// CLASSES

/**
 * netflow_detector1 - class with simple netflow based detector of DNS tunneling.
 */
class netflow_detector1
{
  public:
    netflow_detector1(params *pars);   //constructor
    ~netflow_detector1();   //destructor

    void process_queue();    //process bins
    bool detect_in_bin(ip_storage *bin_to_inspect);   //inspect bin data
    bool detect_in_bin_statistics_schema(ip_storage *bin_to_inspect);   //statistical detection schema
    bool detect_in_bin_threshold_schema(ip_storage *bin_to_inspect);    //bin thresholds detection schema

    void delete_finished_bin(); //free allocated bin memory

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments

    netflow_statistics nf_stats;    //daily statistics data
    nfstat *bin_period_stat;        //stats for actual bin

    unsigned int total_bins;            //total number of inspected bins
    unsigned int total_bins_detected;   //total number of detected bins

    unsigned long thresh_stats[NETFLOW1_SINGLE_THRESHOLDS];  //statistics for thresholds usage

    ip_storage *bin;        //active bin pointer
};

#endif

/**
 * main.cpp
 * This file implements main function.
 *
 * Based on example_module.c by Vaclav Bartos <ibartosv@fit.vutbr.cz>
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    20.4.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */


//information if sigaction is available for nemea signal macro registration
//(TRAP_DEFAULT_INITIALIZATION)
#ifdef HAVE_CONFIG_H
#include <config.h> 
#endif

#ifdef __cplusplus
extern "C" {
#endif
#include <libtrap/trap.h>
#ifdef __cplusplus
}
#endif
#include <unirec/unirec.h>

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <thread>
#include <mutex>

#include "common.h"
#include "netflowdetector1.h"
#include "netflowdetector2.h"
#include "netflowparser.h"
#include "netflowstatisticsmaker.h"
#include "ipfixparser.h"
#include "ipfixdetector2.h"
#include "shared.h"

using namespace std;

// Struct with information about module
trap_module_info_t module_info = {
   (char *)"DNS Anomaly Detector (DNS tunneling)", // Module name
   // Module description
   (char *)"This module provides effective detection of DNS tunneling.\n"
   "It receives UniRec containing flow information and DNS data (IPFIX).\n"
   "Detected anomalies are printed to output file.\n"
   "Interfaces:\n"
   "   Inputs: 1 (UniRec: <BASIC_FLOW><DNS>)\n"
    "Usage:\n"
    "./detector params\n"
    "params:\n"
    "\t-i <UniRec input> - input interface with UniRec IPFIX data.\n"
    "\t-o <output_file> - output file\n"
    "\t-n - run NetFlow detection.\n"
    "\t-x - run IPFIX detection.\n"
    "\t-t <stats_file> - daily NetFlow/IPFIX statistics file created with -s.\n"
    "\t-s - create daily statistics for NetFlow/IPFIX detector (from input data). Flow type (-n/-x), input (-i) and output (-o) must be specified!\n"
    "\t-c <config_file> - configuration file with parameters for detection. If not specified, default configuration is used (see -m parameter).\n"
    "\t-m <config_file> - create new config file with default configuration.\n"
    "\t-w <whitelist_file> - whitelist file with domains which will not be detected as tunnels.\n"
    "\t-l <whitelist_file> - create new whitelist file with default configuration.\n"
    "\t-D <integer> - debug level (prints packet info to stdout). Higher level means more info.\n"
    "\t\t-D 1 prints information about bins (creation, detection, ...)\n"
    "\t\t-D 2 prints packet/flow information - IPs, ports, ...\n"
    "\t\t-D 3 prints packet/flow extended info - DNS queries etc.\n"
    "\t\t-D 4 prints extended info & bin data just before bin switch\n"
    "\t-h - prints this help message\n",
   1, // Number of input interfaces
   0, // Number of output interfaces
};

////////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES

//variable for handling signals and stopping main loop
int stop = 0;

//shared queue with pointers to bins (for inter-thread data sharing between parser and detector1)
shared_queue_ip_storage shared_bin_pointers;

//bins shared between detector1 and detector2
shared_queue_ip_storage shared_bin_pointers_filtered_by_d1;

//shared processing summary msgs
shared_detectors_summary shared_summary;

//shared program arguments
params shared_parameters;

//shared mutex for locking cout when printing logs
mutex mutex_cout;

////////////////////////////////////////////////////////////////////////// FUNCTIONS

// Function to handle SIGTERM and SIGINT signals (used to stop the module)
//TRAP_DEFAULT_SIGNAL_HANDLER(stop = 1)
void signal_handler(int signal)
{
   if (trap_get_verbose_level() > 0)
      printf("Signal received\n");
   if (signal == SIGTERM || signal == SIGINT) {
      stop = 1; // this breaks the main loop
      shared_bin_pointers.set_finished(true);
      shared_bin_pointers_filtered_by_d1.set_finished(true);
      trap_terminate(); // this interrupts a possible waiting in recv/send functions
   }
}


/**
 * Append summary information from modules into output file.
 */
void write_summary()
{
    ofstream outfile(shared_parameters.outfile, ios_base::app | ios_base::out);  //open output file for append
    if (! outfile.is_open())
    {
        cerr << "Couldn't open output file " << shared_parameters.outfile << "!" << endl;
        err_exit();
    }
    else
    {
        shared_summary.write_msgs_to_file(&outfile);
        outfile.close();
    }
}

/**
 * Parse program arguments.
 */
params parse_params(int argc, char **argv)
{
    params p;

    int param = 0;
    opterr = 0;     //disable implicit error msgs
    bool err = false;
    bool was_m = false;

    //parse args
    while((param = getopt(argc, argv, "o:c:D:m:t:w:l:snx")) != EOF)
    {
        string s = "";
        if (optarg)
            s = string(optarg);

        switch(param)
        {
            case 'o':
                p.outfile = s;
                break;
            case 't':
                p.stats_file = s;
                break;
            case 'c':
                if (! parse_config_file(optarg, &p))
                    err_exit();
                break;
            case 'w':
                if (! parse_whitelist_file(optarg, &p))
                    err_exit();
                break;
            case 'D':
                p.debug_level = get_int_from_str((const char *)optarg, &err);
                if (err || p.debug_level < 0)
                    err_exit_with_msg("Error: Expected positive int value after param \"-D\".");
                break;
            case 'n':
                if (p.filetype != '-')
                    err_exit_with_msg("Error: You can select only one file format (-n, -x, but not combination of these)!");
                p.filetype = 'n';
                break;
            case 'x':
                if (p.filetype != '-')
                    err_exit_with_msg("Error: You can select only one file format (-n, -x, but not combination of these)!");
                p.filetype = 'x';
                break;
            case 'm':
                create_config_file(optarg);
                was_m = true;
                break;
            case 'l':
                create_whitelist_file(optarg);
                was_m = true;
            case 's':
                p.s_param = true;
                break;

            //default:
            //    err_exit_with_msg("Error: Unknown parameter! Use \"-h\" for help.");
        }
    }

    //if (optind < argc)
    //    err_exit_with_msg("Error: Unknown parameter! Use \"-h\" for help");

    if (! was_m && p.filetype == '-')
        err_exit_with_msg("Error: Detection file type not specified! Use -n or -x!");

    if (! was_m && p.outfile.empty())
        err_exit_with_msg("Error: Output file not specified! Use -o!");

    if (p.s_param && p.outfile.empty())
        err_exit_with_msg("Error: Missing needed parameters!");

    if (p.bin_packets > 0 && p.d1_detection_schema != D1_DETECTION_SCHEMA_BIN_THRESHOLDS)
        err_exit_with_msg("Error: Can't use bin_packets settings with statistics-based detection schema!");

    return p;
}

////////////////////////////////////////////////////////////////////////// THREAD MAINS


/**
 * Main for netflow parsing and bin creating thread.
 * This thread will parse input netflow files into bins and
 * save them into shared queue.
 */
void thread_run_netflow_loading()
{
    netflow_bin_maker bin_maker(&shared_parameters);
    bin_maker.parse_input();
    bin_maker.save_summary();

    mutex_cout.lock();
    cerr << "PARSER DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for netflow detector 1 (DNS tunnel detection).
 * This thread will act as detector 1 for netflow based DNS tunnel detection (simple detection).
 * Detector reads bins created by loading thread from shared queue.
 */
void thread_run_netflow_detector1()
{
    netflow_detector1 detector1(&shared_parameters);
    detector1.process_queue();
    detector1.save_summary();

    mutex_cout.lock();
    cerr << "DETECTOR1 DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for netflow detector 2 (DNS tunnel detection).
 * This thread will act as detector 2 for netflow based DNS tunnel detection (advanced detection).
 * Detector reads bins created by netflow detector 1 from shared queue.
 */
void thread_run_netflow_detector2()
{
    netflow_detector2 detector2(&shared_parameters);
    detector2.process_queue();
    detector2.save_summary();

    mutex_cout.lock();
    cerr << "DETECTOR2 DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for IPFIX parsing and bin creating thread (from csv file).
 * This thread will read and parse packets into bins and
 * save them into shared queue.
 */
void thread_run_ipfix_loading()
{
    ipfix_bin_maker bin_maker(&shared_parameters);
    bin_maker.parse_input();
    bin_maker.save_summary();

    mutex_cout.lock();
    cerr << "PARSER DONE" << endl;
    mutex_cout.unlock();
}

/**
 * Main for IPFIX detector 1 (DNS tunnel detection).
 * This thread will act as IPFIX detector 2 for DNS tunnel detection (advanced detection methods).
 * Detector reads bins created by detector 1 from shared queue.
 */
void thread_run_ipfix_detector2()
{
    ipfix_detector2 detector2(&shared_parameters);
    detector2.process_queue();
    detector2.save_summary();

    mutex_cout.lock();
    cerr << "DETECTOR2 DONE" << endl;
    mutex_cout.unlock();
}

////////////////////////////////////////////////////////////////////////// PROGRAM MAIN

/**
 * Main - parse args & stuff
 */
int main(int argc, char **argv)
{
   // ***** TRAP initialization *****
   TRAP_DEFAULT_INITIALIZATION(argc, argv, module_info);

   //check params
   shared_parameters = parse_params(argc, argv);
   
   // Register signal handler.
   //TRAP_REGISTER_DEFAULT_SIGNAL_HANDLER();
   signal(SIGTERM, signal_handler);
   signal(SIGINT, signal_handler);
   
   // ***** Create UniRec templates *****
   ur_template_t *unirec_template = ur_create_template("<COLLECTOR_FLOW>,<DNS>");
   shared_parameters.ur_template = unirec_template;
   
   // ***** Main processing loop *****
   if (shared_parameters.s_param)    //create daily statistic data for NetFlow/IPFIX detector
   {
       netflow_statistics_maker stats(&shared_parameters);
       stats.create_netflow_statistics();

       return 0;
   }
   else if (shared_parameters.filetype == 'n')   //netflow based detection
   {
       thread t_loading(thread_run_netflow_loading);       //create netflow loading thread
       thread t_detector1(thread_run_netflow_detector1);   //create netflow detector 1 thread
       thread t_detector2(thread_run_netflow_detector2);   //create netflow detector 2 thread
       t_loading.join();
       t_detector1.join();
       t_detector2.join();

       //write summary to outfile and close it
       write_summary();
   }
   else if (shared_parameters.filetype == 'x') //IPFIX based detection
   {
       thread t_loading(thread_run_ipfix_loading);         //create ipfix loading thread
       thread t_detector1(thread_run_netflow_detector1);   //detector 1 thread - we can use netflow detector
       thread t_detector2(thread_run_ipfix_detector2);     //detector 2 thread
       t_loading.join();
       t_detector1.join();
       t_detector2.join();

       //write summary to outfile and close it
       write_summary();
   }
   
   // ***** Cleanup *****
   
   // Do all necessary cleanup before exiting
   TRAP_DEFAULT_FINALIZATION();

   ur_free_template(unirec_template);
   
   return 0;
}


/**
 * binsorter.cpp
 * This file implements methods of class bin_sorter.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    27.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <sys/types.h>

#include "common.h"
#include "shared.h"
#include "binsorter.h"

using namespace std;

////////////////////////////////////////////////////////////// BIN_SORTER METHODS

/**
 * Constructor
 */
bin_sorter::bin_sorter(params *pars)
{
    parameters = pars;
    total_bins = 0;
    bin_flows = 0;
    last_index = 0;
}

/**
 * Destructor
 */
bin_sorter::~bin_sorter()
{
    for(unsigned int i=0; i<bins.size(); i++)
        delete bins[i];
}

/**
 * Put NetFlow flow into right bin based on flow end time.
 */
void bin_sorter::sort_into_bin(flow *flw)
{
    if (flw)
    {
        flow_time flwtime = flw->flowTime;

        unsigned int n = get_bin_number_by_time(&flwtime);
        bins[n]->add_netflow_flow(*flw);
        bins[n]->add_statistics_data(1, flw->packets, flw->bytes);

        last_index = n;
    }
}

/**
 * Put IPFIX flow into right bin based on flow end time.
 */
void bin_sorter::sort_into_bin(ipfix_flow *flw)
{
    if (flw)
    {
        flow_time flwtime = flw->flowTime;

        unsigned int n = get_bin_number_by_time(&flwtime);

        //filter common query types (if configured)
        if (! (parameters->filter_common_qtypes && has_common_qtype(flw)))
            bins[n]->add_ipfix_flow(*flw);

        bins[n]->add_statistics_data(1, flw->packets, flw->bytes);

        last_index = n;
    }
}

/**
 * Allocate new bin and push it on end of queue.
 */
ip_storage *bin_sorter::alloc_new_bin(flow_time start_time)
{
    ip_storage *new_bin = new ip_storage;

    if (parameters->bin_packets == 0)  //time based bins, set bin start and end time
    {
        new_bin->set_start_time(start_time);
        start_time.add_time(parameters->bin_size_hrs, parameters->bin_size_mins, parameters->bin_size_secs);
        new_bin->set_end_time(start_time);

        if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
        {
            mutex_cout.lock();
            cout << "New bin: end = " << start_time.get_date_time_str() << endl;
            mutex_cout.unlock();
        }
    }
    else    //limited number of flows per bin...
        bin_flows = 0;

    bins.push_back(new_bin);
    return new_bin;
}

/**
 * Push pointer to finished bin into shared queue.
 */
void bin_sorter::push_finished_bin_to_shared(ip_storage *finished_bin)
{
    //bin is not empty
    if (finished_bin && finished_bin->get_stats_data()->flows > 0
            && (finished_bin->get_ipfix_flows_vector()->size() > 0 || finished_bin->get_netflow_flows_vector()->size() > 0))
    {
        shared_bin_pointers.add_data(finished_bin);

        //print finished bin data if needed
        if (parameters->debug_level > DEBUG_LEVEL_EXTENDED)
        {
            mutex_cout.lock();
            finished_bin->print_data();
            mutex_cout.unlock();
        }

        total_bins++;
    }
    else    //delete empty bin
        delete finished_bin;
}

/**
 * Push all unfinished bins into shared queue.
 */
void bin_sorter::push_all_to_shared()
{
    for (unsigned int i=0; i<bins.size(); i++)
    {
        if (bins[i]->get_stats_data()->flows > 0 && (bins[i]->get_ipfix_flows_vector()->size() > 0 || bins[i]->get_netflow_flows_vector()->size() > 0))
        {
            shared_bin_pointers.add_data(bins[i]);

            //print finished bin data if needed
            if (parameters->debug_level > DEBUG_LEVEL_EXTENDED)
            {
                mutex_cout.lock();
                bins[i]->print_data();
                mutex_cout.unlock();
            }

            total_bins++;
        }
    }

    bins.clear();
}

/**
 * Until bin for specified flwtime is created, do:
 * -push oldest bin into shared
 * -insert new bin with time interval after newest bin in vector
 */
void bin_sorter::move_window_until(flow_time *flwtime)
{
    //end time of last existing bin
    flow_time last_bin_time = get_nth_bin_end_time(bins.size() - 1);

    while (last_bin_time.compare_with(*flwtime) < 0)
    {
        //push oldest bin into share queue
        if (bins.size() == parameters->num_of_sorting_bins)
        {
            ip_storage *first_bin = bins[0];
            bins.erase(bins.begin());
            push_finished_bin_to_shared(first_bin);
        }

        //start new bin & push it into local queue
        ip_storage *latest_bin = alloc_new_bin(last_bin_time);
        last_bin_time = latest_bin->get_end_time();
    }
}

/**
 * Get start time of nth bin.
 */
flow_time bin_sorter::get_nth_bin_start_time(unsigned int n)
{
    if (n > bins.size())
        return flow_time();

    return bins[n]->get_start_time();
}

/**
 * Get bin number (index in bins vect) based on bin time.
 * If desired bin doesnt exist, it will be created.
 */
unsigned int bin_sorter::get_bin_number_by_time(flow_time *flwtime)
{
    int bin_number = 0;

    //alloc first bin
    if (bins.empty())
    {
        alloc_new_bin(*flwtime);
        bin_number = 0;
    }
    else //there are existing bins
    {
        if (parameters->bin_packets == 0)   //time based bins
        {
            //flow is too old, put it in first bin
            if (get_nth_bin_start_time(0).compare_with(*flwtime) >= 0)
                return 0;
            //bin for this flow don't exist yet
            else if (get_nth_bin_end_time(bins.size() - 1).compare_with(*flwtime) < 0)
            {
                //move window of bins to destined time
                //(creates needed bin and stores finished bins)
                move_window_until(flwtime);
                return (bins.size() - 1);
            }
            //bin is somewhere in local vector
            else
            {
                return search_in_vector(flwtime);
            }
        }
        else    //limited number of flows per bin
        {
            bin_flows++;
            //bin full
            if (bin_flows > parameters->bin_packets)
            {
                ip_storage *first_bin = bins[0];
                first_bin->set_end_time(*flwtime);
                bins.erase(bins.begin());
                push_finished_bin_to_shared(first_bin);

                //start new bin & push it into local queue
                alloc_new_bin(*flwtime);
            }

            return 0;   //first bin in queue is now active (we need only one bin)
        }
    }

    return bin_number;
}

/**
 * Search for bin in local vector.
 * Search starts on last used position in vector.
 */
unsigned int bin_sorter::search_in_vector(flow_time *flwtime)
{
    unsigned int j=last_index;  //start from last position

    for (unsigned int i = 0; i<bins.size(); i++)
    {
        int cmp1 = get_nth_bin_start_time(j).compare_with(*flwtime);
        int cmp2 = get_nth_bin_end_time(j).compare_with(*flwtime);

        if (cmp1 <= 0 && cmp2 > 0)
            return j;   //found
        else if (cmp2 <= 0)  //search right
            j = (last_index + i) % (bins.size());
        else if (cmp1 > 0)  //search left
            j = (last_index - i) % (bins.size());
    }

    return j;
}

/**
 * Get end time of nth bin.
 */
flow_time bin_sorter::get_nth_bin_end_time(unsigned int n)
{
    if (n > bins.size())
        return flow_time();

    return bins[n]->get_end_time();
}

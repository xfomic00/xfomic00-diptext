README for "detector_nemea"
----------------------
Author: Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
Date: 21.4.2015

"detector_nemea" is tool providing detection of DNS tunneling using various techniques.
This is Nemea version of detector, which is implemented as Nemea module (uses UniRec input
records and Libtrap interface).

--------- INSTALLATION

To run this program, you need to match following criteria:
- compiler (g++) with "--std=c++0x" option
- installed packages: 
  - nemea (needs libtrap, libunirec)

Nemea can be downloaded here: https://www.liberouter.org/technologies/nemea/

For making program, use "make" (program is not installed).

If you have problems with compiling (many weird errors like "size_t was not declared"), than
you must use autoconf. To do this, you must copy all files to a new directory in folder with
nemea modules (modules/) in nemea installation files. Next, you must update list of makefiles to do
in "modules/configure.ac", "AC_CONFIG_FILES" section (line 201). Add line with "created_folder/Makefile".
After that, you can use "bootstrap.sh" followed by "./configure" and "make" to make this module 
(Makefile.ac is used and new Makefile is automatically generated). 

---------- IMPORTANT !!!

  When working with netflow statistics files (-t <file>), statistic period time duration specified in config file must match 
period duration with which were statistic data captured (with -s param)!

---------- TESTING

  For testing purposes, use nemea modules for reading NetFlow or pcap inputs merged with normal traffic.
Otherwise, you can use "standalone version" of this program (included merger for merging attack with normal traffic at
specified time & date).

---------- SPECIFICS

  You can use IPFIX data as input (UniRec template is "<BASIC_FLOW>,<DNS>").
  IPFIX input data can be created using "DNS plugin" for INVEA-TECH's Flowmon Exporter, which you can get from
https://www.liberouter.org/technologies/dns-plugin/

---------- PARAMETERS

  Available options for detection are specified in configuration file. If configuration file
is missing, you can generate it by "./detector_nemea -m <filename>".
  Configuration file is provided to program by parameter "-c <filename>". If no configuration
file is specified, program uses default detection setting, which can be seen in generated
config file...

Program options:
	-i <UniRec input> - input interface with UniRec IPFIX data
	-o <output_file> - output file
    -n - run NetFlow detection.
    -x - run IPFIX detection.
	-t <stats_file> - daily NetFlow/IPFIX statistics file created with -s.
	-s - create daily statistics for NetFlow/IPFIX detector (from input data). Flow type (-n/-d/-x), inputs (-i/-r) and output (-o) must be specified!
	-c <config_file> - configuration file with parameters for detection. If not specified, default configuration is used (see -m parameter).
	-m <config_file> - create new config file with default configuration.
    -w <whitelist_file> - whitelist file with domains which will not be detected as tunnels.
    -l <whitelist_file> - create new whitelist file with default configuration.
	-D <integer> - debug level (prints packet info to stdout). Higher level means more info.
		-D 1 prints information about bins (creation, detection, ...)
		-D 2 prints packet/flow information - IPs, ports, ...
		-D 3 prints packet/flow extended info - DNS queries etc.
		-D 4 prints extended info & bin data just before bin switch
	-h - prints help message

---------- TYPICAL USAGE

Firstly, you need configuration file. If configuration file (config.txt) is missing, you can create it with:
	./detector_nemea -m config.txt
Next, you should change configuration for your specific environment (based on volume of traffic which flows through your detection point).

Case 1: IPFIX detection
	./detector_nemea -x -i "t;collector-nemea,7608" -o detections.txt -c config.txt -w whitelist.txt -t statistics.txt

Case 2: NetFlow detection
	./detector_nemea -n -i "t;collector-nemea,7608" -o detections.txt -c config.txt -w whitelist.txt -t statistics.txt

 

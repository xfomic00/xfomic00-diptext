/**
 * bin_sorter.h
 * This file defines class bin_sorter.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    27.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
#include "ip_storage.h"

using namespace std;

#ifndef _BINSORTER_H_
#define _BINSORTER_H_

/////////////////////////////////////////////// CONSTANTS



/////////////////////////////////////////////// CLASSES

/**
 * bin_sorter - class for sorting flows into multiple bins by flow time.
 */
class bin_sorter
{
  public:
    bin_sorter(params *pars);   //constructor
    ~bin_sorter();   //destructor

    void sort_into_bin(flow *flw);      //put flow into right bin based on flow time
    void sort_into_bin(ipfix_flow *flw);//same, but different flow type

    ip_storage *alloc_new_bin(flow_time start_time);       //allocate new bin and push it into bins vector
    void push_finished_bin_to_shared(ip_storage *finished_bin); //pass pointer to finished bin to shared queue
    void push_all_to_shared();  //push all bins into shared
    void move_window_until(flow_time *flwtime); //create new bins and save old ones until bin for current flow is created

    flow_time get_nth_bin_start_time(unsigned int n);  //start time of nth bin
    flow_time get_nth_bin_end_time(unsigned int n);    //end time of nth bin

    unsigned int get_bin_number_by_time(flow_time *flwtime);    //get bin number (index in bins vect) based on flow time
    unsigned int search_in_vector(flow_time *flwtime);          //search bin in vector by time

    unsigned long get_total_bins() { return total_bins; }   //get number of created bins

  private:
    params *parameters; //program arguments

    vector<ip_storage *> bins;  //bins window

    unsigned int last_index;    //index of last used bin
    unsigned long bin_flows;    //number of flows saved in current bin
    unsigned long total_bins;   //number of created bins
};

#endif

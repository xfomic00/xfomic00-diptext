/**
 * netflowdetector2.cpp
 * This file implements methods of class netflow_detector2.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    22.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>


#include "netflowdetector2.h"
#include "netflowparser.h"
#include "common.h"
#include "shared.h"

using namespace std;

////////////////////////////////////////////////////////////// NETFLOW_DETECTOR2 METHODS

/**
 * Constructor
 */
netflow_detector2::netflow_detector2(params *pars)
{
    parameters = pars;
    bin = NULL;
    total_bins = 0;
    stored_bins = 0;
    total_detections = 0;
    total_flows_processed = 0;
    total_flows_analyzed = 0;
    last_bin_end.is_valid = false;

    for(int i=0; i<NETFLOW_SINGLE_THRESHOLDS; i++)
        thresh_stats[i] = 0;

    //check if supported aggregation schema was selected
    if (parameters->d2_detection_schema != D2_DETECTION_SCHEMA_AGGREGATE_ALL && parameters->d2_detection_schema != D2_DETECTION_SCHEMA_AGGREGATE_SINGLE)
        parameters->d2_detection_schema = D2_DETECTION_SCHEMA_AGGREGATE_SINGLE;
}

/**
 * Destructor
 */
netflow_detector2::~netflow_detector2()
{
    delete_finished_bin();
}


/**
 * Process shared queue of bins filtered by detector 1.
 */
void netflow_detector2::process_queue()
{
    ofstream outfile(parameters->outfile);  //open output file
    if (! outfile.is_open())
    {
        cerr << "Couldn't create output file " << parameters->outfile << "!" << endl;
        err_exit();
    }

    //process all bins in shared queue
    //(NULL is returned when queue is empty and producer process has finished)
    while ((bin = shared_bin_pointers_filtered_by_d1.get_data()) != NULL)
    {
        //bin(s) were detected in succession, but now sequence ends -> inspect aggregated data
        if ((last_bin_end.is_valid && last_bin_end.compare_with(bin->get_start_time()) != 0) || (stored_bins == parameters->max_aggregated_bins))
            inspect_aggregated_bins(&outfile);

        //detection
        detect_in_bin(bin);

        total_bins++;
        stored_bins++;
        last_bin_end = bin->get_end_time();
        last_bin_end.is_valid = true;

        delete_finished_bin();  //delete finished bin

        if (stop)
            break;
    }

    //inspect final aggregated data
    inspect_aggregated_bins(&outfile);

    outfile.close();
}


/**
 * Detection method for single NetFlow flow.
 * Returns true if detection was positive, false otherwise.
 */
bool netflow_detector2::detect(flow *flw)
{
    bool ret = false;

    if (flw)
    {
        if (flw->bytes > parameters->bpf_thresh)    //bytes per flow threshold
        {
            ret = true;
            ++thresh_stats[NETFLOW_THRESH_BPF_POS];
        }
        else if (flw->packets > parameters->ppf_thresh) //packets per flow threshold
        {
            ret = true;
            ++thresh_stats[NETFLOW_THRESH_PPF_POS];
        }
        else if (flw->packets == 1 && flw->bytes > parameters->packet_size_thresh)  //packet size thresh
        {
            ret = true;
            ++thresh_stats[NETFLOW_THRESH_PACK_SIZE_POS];
        }
        else if (flw->flowTime.duration > parameters->single_flow_duration) //flow duration
        {
            ret = true;
            ++thresh_stats[NETFLOW_THRESH_FLOW_DURATION];
        }
    }

    return ret;
}

/**
 * Detection method for one NetFlow bin.
 * Picks detection method based on selected detection schema.
 */
void netflow_detector2::detect_in_bin(ip_storage *bin_to_inspect)
{
    if (bin_to_inspect)
    {
        vector<flow> *flows = bin_to_inspect->get_netflow_flows_vector();

        //iterate through all flows in bin
        for (unsigned int i=0; i<flows->size(); i++)
        {
            //detection based on selected detection schema
            if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_ALL)
            {
                //aggregate all flows in bin
                put_ip_data_in_map(&((*flows)[i]));
            }
            else if (parameters->d2_detection_schema == D2_DETECTION_SCHEMA_AGGREGATE_SINGLE)
            {
                //aggregate only certain flows
                if (detect(&((*flows)[i])))
                    put_ip_data_in_map(&((*flows)[i]));
            }

            if (stop)
                break;
        } //for

        total_flows_processed += flows->size();
    }
}

/**
 * Inspect aggregated data from multiple bins.
 */
void netflow_detector2::inspect_aggregated_bins(ofstream *file)
{
    unordered_map<flow_key, storage_data, flow_key_hasher> *table = ipmap.get_map();

    const int TOTAL_CRITERIAS = 4;  //total number of detection criterias

    //iterate through aggregated records
    for(unordered_map<flow_key, storage_data, flow_key_hasher>::iterator iter = table->begin(); iter != table->end(); iter++)
    {
        in_addr srcIP, dstIP;
        in6_addr srcIPv6, dstIPv6;
        flow_key fk = (flow_key)((*iter).first);
        if (fk.ipv6)
        {
            srcIPv6 = fk.srcIP.v6;
            dstIPv6 = fk.dstIP.v6;
        }
        else
        {
            srcIP.s_addr = fk.srcIP.v4.s_addr;
            dstIP.s_addr = fk.dstIP.v4.s_addr;
        }

        storage_data *data;
        data = (storage_data *) &((*iter).second);

        ostringstream out_stream;
        unsigned int conditions_matched = 0;

        if (data->get_avg_packet_size() > parameters->avg_size_map_thresh && data->get_packets_count() > parameters->map_minimum_packets) //avg packet size threshold (must be more packets)
        {
            out_stream << "\t" << "avg packet size = " << data->get_avg_packet_size() << " B (detection threshold = " << parameters->avg_size_map_thresh << "), packets: " << data->get_packets_count() << endl;
            ++conditions_matched;
        }
        if (data->get_bytes_total() > parameters->total_bytes_map_thresh) //total bytes threshold
        {
            out_stream << "\t" << "total bytes = " << data->get_bytes_total() << " B (detection threshold = " << parameters->total_bytes_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_occurences_count() > parameters->occurences_in_map_thresh) //occurences in map threshold
        {
            out_stream << "\t" << "occurences in map = " << data->get_occurences_count() << " (detection threshold = " << parameters->occurences_in_map_thresh << ")" << endl;
            ++conditions_matched;
        }
        if (data->get_max_duration() > parameters->single_flow_duration)    //single flow duration
        {
            out_stream << "\t" << "single flow duration = " << data->get_max_duration() << " s (detection threshold = " << parameters->single_flow_duration << ")" << endl;
            ++conditions_matched;
        }

        //print detected IP info
        if (conditions_matched >= parameters->map_minimum_conditions)
        {
            u_int16_t srcPort = (data->get_srcports_count() > 1) ? 0 : data->get_srcports_map()->begin()->first;
            u_int16_t dstPort = (data->get_dstports_count() > 1) ? 0 : data->get_dstports_map()->begin()->first;
            string srcIP_str = (fk.ipv6) ? get_ipv6_str(srcIPv6) : get_ipv4_str(srcIP);
            string dstIP_str = (fk.ipv6) ? get_ipv6_str(dstIPv6) : get_ipv4_str(dstIP);

            //detect only communication from tunnel client to tunnel server
            if (parameters->detect_only_client_communication && srcPort == 53)
                continue;

            //print flow time and IPs/ports
            (*file) << data->get_first_time().get_date_time_str() << " - " << data->get_last_time().get_date_time_str() << endl;
            (*file) << srcIP_str << ":";
            if (srcPort == 0)
                (*file) << "XXXXX";
            else
                (*file) << srcPort;
            (*file) << " -> " << dstIP_str << ":";
            if (dstPort == 0)
                (*file) << "XXXXX" << endl;
            else
                (*file) << dstPort << endl;

            (*file) << "Detection criteria (matched " << conditions_matched << "/" << TOTAL_CRITERIAS << "):" << endl;
            (*file) << out_stream.str();

            total_detections++;
            (*file) << "----------------------------" << endl;
        }

        if (stop)
            break;
    }

    //clear aggregation map
    stored_bins=0;
    ipmap.clear_all();
}

/**
 * Put flow data into internal aggregation map for further analysis.
 */
void netflow_detector2::put_ip_data_in_map(flow *flw)
{
    if (flw)
    {
        aggreg_data data(flw->srcPort, flw->dstPort, "", flw->packets, flw->bytes, flw->flowTime, 0, 0, false);

        if (flw->ipv6)
            ipmap.add_flow_data(flw->srcIP.v6, flw->dstIP.v6, data);
        else
            ipmap.add_flow_data(flw->srcIP.v4.s_addr, flw->dstIP.v4.s_addr, data);

        total_flows_analyzed++;
    }
}

/**
 * Free allocated bin data (of finished bin).
 */
void netflow_detector2::delete_finished_bin()
{
    if (bin)
    {
        delete bin;
        bin = NULL;
    }
}

/**
 * Save summary msg to shared summary.
 */
void netflow_detector2::save_summary()
{
    ostringstream str;
    str << "Total bins processed:  " << total_bins << endl;
    str << "Total flows processed: " << total_flows_processed << endl;
    str << "Total flows analyzed:  " << total_flows_analyzed << endl;
    str << "Total flows detected:  " << total_detections << endl;
    str << endl;
    str << "Single flow thresholds usage statistics:" << endl;

    //print thresholds usage info
    str << "Bytes per flow:       " << thresh_stats[NETFLOW_THRESH_BPF_POS] << endl;
    str << "Packets per flow:     " << thresh_stats[NETFLOW_THRESH_PPF_POS] << endl;
    str << "Single packet size:   " << thresh_stats[NETFLOW_THRESH_PACK_SIZE_POS] << endl;
    str << "Single flow duration: " << thresh_stats[NETFLOW_THRESH_FLOW_DURATION] << endl;

    shared_summary.set_detector2_msg(str.str());
}

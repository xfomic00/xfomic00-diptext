/**
 * netflowdetector1.cpp
 * This file implements methods of class netflow_detector1.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    18.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>


#include "netflowdetector1.h"
#include "netflowparser.h"
#include "common.h"
#include "netflowstatisticsmaker.h"
#include "shared.h"

using namespace std;

////////////////////////////////////////////////////////////// NETFLOW_DETECTOR1 METHODS

/**
 * Konstruktor
 */
netflow_detector1::netflow_detector1(params *pars)
{
    parameters = pars;
    bin = NULL;
    nf_stats.set_params(pars);
    bin_period_stat = NULL;
    total_bins = 0;
    total_bins_detected = 0;

    for(int i=0; i<NETFLOW1_SINGLE_THRESHOLDS; i++)
        thresh_stats[i] = 0;
}

/**
 * Destruktor
 */
netflow_detector1::~netflow_detector1()
{
    delete_finished_bin();
}

/**
 * Process shared queue of bins (detection loop).
 */
void netflow_detector1::process_queue()
{
    if (! parameters->stats_file.empty()) //statistics file was selected, parse it first
    {
        if (! nf_stats.parse_statistics_file())
            err_exit();
    }
    else    //statistics file not specified
    {
        //switch detection schema
        parameters->d1_detection_schema = D1_DETECTION_SCHEMA_BIN_THRESHOLDS;
    }

    //do not print double with scientific notation
    cout << fixed << setprecision(4);

    //process all bins in shared queue
    //(NULL is returned when queue is empty and producer process has finished)
    while ((bin = shared_bin_pointers.get_data()) != NULL)
    {
        //determine daily statistics for new bin (based on bin start time)
        if (! parameters->stats_file.empty())
            bin_period_stat = nf_stats.get_stat_data_for_period(bin->get_start_time());

        //print bin info
        if (parameters->debug_level > DEBUG_LEVEL_NONE)
        {
            unsigned int total_flows = bin->get_stats_data()->flows;
            mutex_cout.lock();
            cout << "D1 received bin: start=" << bin->get_start_time().get_date_time_str() << ", end=" << bin->get_end_time().get_date_time_str() << ", flows=" << total_flows << endl;
            mutex_cout.unlock();
        }

        //detection
        if (detect_in_bin(bin))
        {
            shared_bin_pointers_filtered_by_d1.add_data(bin);   //pass detected bins to detector 2
            bin = NULL;
            total_bins_detected++;
        }
        else
            delete_finished_bin();  //delete finished bin which isnt interesting enough for further detection

        total_bins++;

        if (stop)
            break;
    }

    shared_bin_pointers_filtered_by_d1.set_finished(true);  //filtering done
}

/**
 * Provide detection method using bin data.
 * Found tunnels info prints into file.
 */
bool netflow_detector1::detect_in_bin(ip_storage *bin_to_inspect)   //ofstream *file,
{
    bool detected = false;

    //select detection function based on configured detection schema
    if (parameters->d1_detection_schema == D1_DETECTION_SCHEMA_STATISTICS)
        detected = detect_in_bin_statistics_schema(bin_to_inspect);
    else if (parameters->d1_detection_schema == D1_DETECTION_SCHEMA_BIN_THRESHOLDS)
        detected = detect_in_bin_threshold_schema(bin_to_inspect);

    return detected;
}

/**
 * Provide statistics based detection schema for bin inspection.
 * returns true if bin satisfies detection condition, false otherwise.
 */
bool netflow_detector1::detect_in_bin_statistics_schema(ip_storage *bin_to_inspect)
{
    bool detected = false;

    if (! parameters->stats_file.empty()) //statistics based detection (only summary data)
    {
        bin_data *bin_summary = bin_to_inspect->get_stats_data();

        //statistical threshold based detection
        //avg bytes per bin
        if (bin_summary->bytes > (bin_period_stat->avg_bpb + parameters->thresh_stat_avg_bpb))
        {
            if (parameters->debug_level > DEBUG_LEVEL_NONE)
            {
                mutex_cout.lock();
                cout << "D1 DETECTED: bin " << bin_to_inspect->get_start_time().get_date_time_str() << " with bytes per bin: " << bin_summary->bytes << " > statistical threshold: ";
                cout << (bin_period_stat->avg_bpb + parameters->thresh_stat_avg_bpb) << endl;
                mutex_cout.unlock();
            }

            detected = true;
            ++thresh_stats[NETFLOW1_THRESH_BPB_POS]; 
        }
        //avg flows per bin
        else if (bin_summary->flows > (bin_period_stat->avg_fpb + parameters->thresh_stat_avg_fpb))
        {
            if (parameters->debug_level > DEBUG_LEVEL_NONE)
            {
                mutex_cout.lock();
                cout << "D1 DETECTED: bin " << bin_to_inspect->get_start_time().get_date_time_str() << " with flows per bin: " << bin_summary->flows << " > statistical threshold: ";
                cout << (bin_period_stat->avg_fpb + parameters->thresh_stat_avg_fpb) << endl;
                mutex_cout.unlock();
            }

            detected = true;
            ++thresh_stats[NETFLOW1_THRESH_FPB_POS]; 
        }
        //avg bytes per packet per bin
        else if (bin_summary->get_bytes_per_packet_per_bin() > (bin_period_stat->avg_bpppb + parameters->thresh_stat_avg_bpppb))
        {
            if (parameters->debug_level > DEBUG_LEVEL_NONE)
            {
                mutex_cout.lock();
                cout << "D1 DETECTED: bin " << bin_to_inspect->get_start_time().get_date_time_str() << " with bytes per packet per bin: " << bin_summary->get_bytes_per_packet_per_bin() << " > statistical threshold: ";
                cout << (bin_period_stat->avg_bpppb + parameters->thresh_stat_avg_bpppb) << endl;
                mutex_cout.unlock();
            }

            detected = true;
            ++thresh_stats[NETFLOW1_THRESH_BPPPB_POS]; 
        }
    }

    return detected;
}

/**
 * Provide bin threshold based detection schema for bin inspection.
 * returns true if bin satisfies detection condition, false otherwise.
 */
bool netflow_detector1::detect_in_bin_threshold_schema(ip_storage *bin_to_inspect)
{
    bool detected = false;

    if (bin_to_inspect)
    {
        bin_data *bin_summary = bin_to_inspect->get_stats_data();

        if (bin_summary->get_bytes_per_packet_per_bin() > parameters->thresh_thresh_bpppb)
        {
            detected = true;
            if (parameters->debug_level > DEBUG_LEVEL_NONE)
            {
                mutex_cout.lock();
                cout << "D1 DETECTED: bin " << bin_to_inspect->get_start_time().get_date_time_str() << "with bytes per packet per bin: " << bin_summary->get_bytes_per_packet_per_bin() << " > threshold: ";
                cout << (parameters->thresh_thresh_bpppb) << endl;
                mutex_cout.unlock();
            }

            ++thresh_stats[NETFLOW1_THRESH_BPPPB_POS];
        }
        //TODO other thresholds
    }

    return detected;
}

/**
 * Free allocated bin memory.
 */
void netflow_detector1::delete_finished_bin()
{
    if (bin)
    {
        delete bin;
        bin = NULL;
    }
}

/**
 * Save summary msg to shared summary.
 */
void netflow_detector1::save_summary()
{
    ostringstream str;
    str << "Total bins processed: " << total_bins << endl;
    str << "Total bins detected:  " << total_bins_detected << endl;
    str << endl;
    str << "Bin statistical thresholds usage statistics:" << endl;

    //print thresholds usage info
    str << "Average bytes per bin:            " << thresh_stats[NETFLOW1_THRESH_BPB_POS] << endl;
    str << "Average flows per bin:            " << thresh_stats[NETFLOW1_THRESH_FPB_POS] << endl;
    str << "Average bytes per packet per bin: " << thresh_stats[NETFLOW1_THRESH_BPPPB_POS] << endl;

    shared_summary.set_detector1_msg(str.str());
}

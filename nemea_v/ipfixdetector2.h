/**
 * ipfixdetector2.h
 * This file defines class ipfix_detector2.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    22.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
#include "ip_storage.h"

using namespace std;

#ifndef _IPFIXDETECTOR2_H_
#define _IPFIXDETECTOR2_H_

/////////////////////////////////////////////// CONSTANTS

//total number of single flow thresholds
#define IPFIX_SINGLE_THRESHOLDS 9

//indexes in thresh_stats array for thresholds usage statistics
#define IPFIX_THRESH_BPF_POS 0
#define IPFIX_THRESH_PPF_POS 1
#define IPFIX_THRESH_PACK_SIZE_POS 2
#define IPFIX_THRESH_TTL_POS 3
#define IPFIX_THRESH_RDLEN_POS 4
#define IPFIX_THRESH_QUERY_SIZE_POS 5
#define IPFIX_THRESH_QNAME_LEN_POS 6
#define IPFIX_THRESH_SUBDOMAIN_LEN_POS 7
#define IPFIX_THRESH_TOO_MUCH_ASCII_POS 8

/////////////////////////////////////////////// CLASSES

/**
 * ipfix_detector2 - class with advanced IPFIX based detector of DNS tunneling.
 */
class ipfix_detector2
{
  public:
    ipfix_detector2(params *pars);  //constructor
    ~ipfix_detector2();             //destructor

    void process_queue();                                               //process bins
    bool detect(ipfix_flow *flw);                       //inspect single flow
    void detect_in_bin(ip_storage *bin_to_inspect);     //inspect bin data
    void inspect_aggregated_bins(ofstream *file);                       //inspect aggregated data

    void put_ip_data_in_map(ipfix_flow *flw);   //aggregate flow data into bin map

    void delete_finished_bin();                 //free allocated bin memory

    void save_summary();            //create summary msg

  private:
    params *parameters;     //program arguments
    unsigned int total_bins;//number of bins inspected
    unsigned int stored_bins;   //number of bins actually aggregated
    unsigned int total_detections;  //number of detected flows
    unsigned long total_flows_processed; //number of ispected flows
    unsigned long total_flows_analyzed;  //number of analyzed flows
    unsigned long total_flows_whitelisted; //number of whitelisted flows
    flow_time last_bin_end; //time of last bin end

    unsigned long thresh_stats[IPFIX_SINGLE_THRESHOLDS];  //statistics for thresholds usage

    ip_storage *bin;        //active bin pointer
    ip_storage ipmap;       //internal aggregation map
};

#endif

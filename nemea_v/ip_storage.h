/**
 * ip_storage.h
 * This file defines class ip_storage, which is used for storing IP addresses and their data.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    18.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <cstring>
#include <string>
#include <iterator>
#include <vector>
#include <unordered_map>
#include <map>

#include "common.h"
#include "netflowstatisticsmaker.h"

using namespace std;

#ifndef _IPSTORAGE_H_
#define _IPSTORAGE_H_

/////////////////////////////////////////////// CONSTANTS & STRUCTS

#define SORT_EVERY_NTH_ELEMENT 5    //after N elements are added, sort is done to make find faster...


//struct with data for specific flow, which will be aggregated
struct aggreg_data
{
    aggreg_data(u_int16_t sprt, u_int16_t dprt, string domn, u_int16_t pkts, unsigned int byts)
        : SRCport(sprt), DSTport(dprt), domain(domn), packets(pkts), bytes(byts) //constructor
    {
        ftime.is_valid = false;
        qType = 0;
        rdLen = 0;
        is_query = false;
    }

    aggreg_data(u_int16_t sprt, u_int16_t dprt, string domn, u_int16_t pkts, unsigned int byts, flow_time time, u_int16_t qt, u_int16_t rlen, bool isQuery)
        : SRCport(sprt), DSTport(dprt), domain(domn), packets(pkts), bytes(byts), ftime(time), qType(qt), rdLen(rlen), is_query(isQuery) {}  //constructor

    u_int16_t SRCport;  //src port
    u_int16_t DSTport;  //dst port
    string domain;      //used domain name
    u_int16_t packets;  //total packets
    u_int32_t bytes;    //total bytes
    flow_time ftime;    //time
    u_int16_t qType;    //type of query (A, NS, MX, ...)
    u_int16_t rdLen;    //length of RDATA
    bool is_query;      //this was query packet
};

/**
 * flow_key - helper struct acting as key into flows hash table.
 * (joins src and dst addresses)
 */
struct flow_key
{
    //constructors
    flow_key(u_int32_t src, u_int32_t dst)
    {
        srcIP.v4.s_addr = src;
        dstIP.v4.s_addr = dst;
        ipv6 = false;
    }

    flow_key(in6_addr src, in6_addr dst)
    {
        srcIP.v6 = src;
        dstIP.v6 = dst;
        ipv6 = true;
    }

    //compare operator
    bool operator == (flow_key const& other) const
    {
        if (ipv6 != other.ipv6)
            return false;
        else if (ipv6)
        {
            //compare in6_addrs (memcmp is not working...)
            return (srcIP.v6.__in6_u.__u6_addr32[0] == other.srcIP.v6.__in6_u.__u6_addr32[0] &&
                srcIP.v6.__in6_u.__u6_addr32[1] == other.srcIP.v6.__in6_u.__u6_addr32[1] &&
                srcIP.v6.__in6_u.__u6_addr32[2] == other.srcIP.v6.__in6_u.__u6_addr32[2] &&
                srcIP.v6.__in6_u.__u6_addr32[3] == other.srcIP.v6.__in6_u.__u6_addr32[3] &&
                dstIP.v6.__in6_u.__u6_addr32[0] == other.dstIP.v6.__in6_u.__u6_addr32[0] &&
                dstIP.v6.__in6_u.__u6_addr32[1] == other.dstIP.v6.__in6_u.__u6_addr32[1] &&
                dstIP.v6.__in6_u.__u6_addr32[2] == other.dstIP.v6.__in6_u.__u6_addr32[2] &&
                dstIP.v6.__in6_u.__u6_addr32[3] == other.dstIP.v6.__in6_u.__u6_addr32[3]);
        }
        else
            return (srcIP.v4.s_addr == other.srcIP.v4.s_addr && dstIP.v4.s_addr == other.dstIP.v4.s_addr);
    }

    ip46_union srcIP;
    ip46_union dstIP;

    /*
    u_int32_t srcIP;
    u_int32_t dstIP;
    in6_addr srcIPv6;
    in6_addr dstIPv6;
    */
    bool ipv6;
};

/**
 * flow_key_hasher - helper struct for hashing two IPs together.
 */
struct flow_key_hasher
{
    size_t operator()(flow_key const& fk) const
    {
        size_t seed = 0;
        if (fk.ipv6)
        {
            hash_combine(seed, fk.srcIP.v6.__in6_u.__u6_addr32[0]);
            hash_combine(seed, fk.srcIP.v6.__in6_u.__u6_addr32[1]);
            hash_combine(seed, fk.srcIP.v6.__in6_u.__u6_addr32[2]);
            hash_combine(seed, fk.srcIP.v6.__in6_u.__u6_addr32[3]);
            hash_combine(seed, fk.dstIP.v6.__in6_u.__u6_addr32[0]);
            hash_combine(seed, fk.dstIP.v6.__in6_u.__u6_addr32[1]);
            hash_combine(seed, fk.dstIP.v6.__in6_u.__u6_addr32[2]);
            hash_combine(seed, fk.dstIP.v6.__in6_u.__u6_addr32[3]);
        }
        else
        {
            hash_combine(seed, fk.srcIP.v4.s_addr);
            hash_combine(seed, fk.dstIP.v4.s_addr);
        }

        return seed;
    }
};

/////////////////////////////////////////////// CLASSES


/**
 * storage_data - stores useful aggregated data for each IP.
 * Data are added dynamically (use function add).
 */
class storage_data
{
  public:
    storage_data();     //constructor
    ~storage_data();    //destructor

    bool add(const aggreg_data& data); //add aggregation data

    //search functions...
    int get_domains_count() { return domains_map.size(); }
    int get_srcports_count() { return srcports_map.size(); }
    int get_dstports_count() { return dstports_map.size(); }
    unsigned int get_occurences_count() { return occurences; }
    unsigned int get_packets_count() { return packets; }
    unsigned long get_bytes_total() { return bytes; }
    unsigned long get_rdlen_total() { return rdlen_total; }
    unsigned int get_queries_total() { return queries; }
    unsigned long get_bytes_only_queries() { return bytes_only_queries; }
    map<u_int16_t, u_int32_t> *get_srcports_map() { return &srcports_map; }
    map<u_int16_t, u_int32_t> *get_dstports_map() { return &dstports_map; }
    map<string, u_int32_t> *get_domains_map() { return &domains_map; }
    map<u_int16_t, u_int32_t> *get_qtypes_map() { return &qtypes_map; }
    flow_time get_first_time() { return firstTime; }
    flow_time get_last_time() { return lastTime; }
    double get_max_duration() { return maxDuration; }

    u_int32_t get_domain_counter(string domain);
    u_int32_t get_qtype_counter(u_int16_t qtype);
    u_int32_t get_most_used_domain_count() { return most_used_domain_counter; }
    u_int32_t get_most_used_qtype_count() { return most_used_qtype_counter; }
    double get_avg_packet_size();
    double get_avg_rdlen_size();
    double get_avg_namelen_size();

  private:
    map<u_int16_t, u_int32_t> srcports_map; //src ports used with src IP
    map<u_int16_t, u_int32_t> dstports_map; //dst ports used with dst IP
    map<string, u_int32_t> domains_map;     //domains used with this IP
    map<u_int16_t, u_int32_t> qtypes_map;   //DNS query types used with this IP
    unsigned int occurences;                //total number of occurences
    unsigned int packets;                   //total packets count for this IP
    unsigned long bytes;                    //total bytes count for this IP
    unsigned long rdlen_total;              //total size of DNS RDATA received
    unsigned long namelen_total;            //total length of DNS domain names
    unsigned int queries;                   //total number of queries only
    unsigned long bytes_only_queries;       //total number of bytes on only queries
    u_int32_t most_used_domain_counter;     //counter for most used domain
    u_int32_t most_used_qtype_counter;      //counter for most used qType

    flow_time firstTime;    //time of first detection
    flow_time lastTime;     //time of last detection
    double maxDuration;     //maximal flow duration (NetFlow only)
};


/**
 * ip_storage - class used for storing IP addresses and other informations about bin.
 * For each IP address there exists some attached data (used ports, domains...).
 * IP addresses are stored in hash array - relatively fast & memory effective.
 */
class ip_storage
{
  public:
    ip_storage();   //constructor
    ~ip_storage();  //destructor

    bool add_flow_data(u_int32_t srcIP, u_int32_t dstIP, aggreg_data data);  //add data to specific flow (IPv4)
    bool add_flow_data(in6_addr srcIP, in6_addr dstIP, aggreg_data data);    //add data to specific flow (IPv6)
    void add_statistics_data(u_int32_t flows, u_int16_t packets, u_int32_t bytes);   //add bin statistic data
    void add_pkt(pkt p);                //add pcap packet parsed data into private vector...
    void add_ipfix_flow(ipfix_flow f);  //add IPFIX flow parsed data into private vector
    void add_netflow_flow(flow f);      //add NetFlow flow parsed data ...

    bin_data *get_stats_data();         //get summary bin data

    void set_start_time(flow_time t) { start_time = t; }
    void set_end_time(flow_time t) { end_time = t; }
    flow_time get_start_time() { return start_time; }
    flow_time get_end_time() { return end_time; }

    unordered_map<flow_key, storage_data, flow_key_hasher> *get_map();    //get map pointer
    vector<pkt> *get_pkts_vector();
    vector<ipfix_flow> *get_ipfix_flows_vector();
    vector<flow> *get_netflow_flows_vector();

    int get_addr_count();   //return count of stored addresses
    void clear_all();       //delete actual content of table

    void print_data();                          //print stored data
    void print_data_to_file(ofstream *file);    //save data to file (as formatted output)

  private:
    unordered_map<flow_key, storage_data, flow_key_hasher> table; //hash storage table for flow data
    bin_data summary_data;      //summary data used for statistics based detection
    flow_time start_time;       //bin start time
    flow_time end_time;         //bin end time
    vector<pkt> pkts;           //vector of parsed packets (pcap)
    vector<ipfix_flow> xflows;  //vector of parsed IPFIX flows
    vector<flow> nflows;        //vector of parsed NetFlow flows
};

#endif

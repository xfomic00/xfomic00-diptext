/**
 * netflowstatisticsmaker.h
 * This file defines class netflow_statistics_maker.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    25.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>

#include "common.h"

using namespace std;

#ifndef _NETFLOWSTATISTICSMAKER_H_
#define _NETFLOWSTATISTICSMAKER_H_

/////////////////////////////////////////////// CONSTANTS


/////////////////////////////////////////////// CLASSES & STRUCTS

/**
 * bin_data - stores data aggregated over one bin
 */
struct bin_data
{
    bin_data() : flows(0), packets(0), bytes(0) {}  //constructor

    void add(unsigned int f, unsigned int p, unsigned int b)    //add data
    {
        flows += f;
        packets += p;
        bytes += b;
    }

    void clear()    //clear content
    {
        flows = 0;
        packets = 0;
        bytes = 0;
        bin_start.is_valid = false;
        bin_end.is_valid = false;
    }

    double get_bytes_per_packet_per_bin() { return (double)bytes/(double)packets; }

    unsigned int flows;
    unsigned int packets;
    unsigned long bytes;

    flow_time bin_start;
    flow_time bin_end;
};

/**
 * period_data - stores useful aggregated data for each statistical period.
 * Data are added dynamically (use function add).
 */
class period_data
{
  public:
    period_data() : max_bytes_per_bin(0), max_flows_per_bin(0), bytes_per_bin(0), flows_per_bin(0), bytes_per_packet_per_bin(0), bins(0) {} //constructor

    inline void add(double bpb, double fpb, double bpppb) //add aggregation data
    {
        bytes_per_bin += bpb;
        flows_per_bin += fpb;
        bytes_per_packet_per_bin += bpppb;
        bins++;

        if (bpb > max_bytes_per_bin)
            max_bytes_per_bin = bpb;
        if (fpb > max_flows_per_bin)
            max_flows_per_bin = fpb;
    }

    void merge_with(period_data *d)
    {
        if (! d)
            return;

        bytes_per_bin += d->get_bytes_per_bin();
        flows_per_bin += d->get_flows_per_bin();
        bytes_per_packet_per_bin += d->get_bytes_per_packet_per_bin();
        bins += d->get_bins();
    }

    double get_avg_bytes_per_bin() { return bytes_per_bin/(double)bins; }
    double get_avg_flows_per_bin() { return flows_per_bin/(double)bins; }
    double get_avg_bytes_per_packet_per_bin() { return bytes_per_packet_per_bin/(double)bins; }
    unsigned long get_max_bytes_per_bin() { return max_bytes_per_bin; }
    unsigned long get_max_flows_per_bin() { return max_flows_per_bin; }

    double get_bytes_per_bin() { return bytes_per_bin; }
    double get_flows_per_bin() { return flows_per_bin; }
    double get_bytes_per_packet_per_bin() { return bytes_per_packet_per_bin; }
    unsigned int get_bins() { return bins; }

  private:
    unsigned long max_bytes_per_bin;
    unsigned long max_flows_per_bin;
    double bytes_per_bin;
    double flows_per_bin;
    double bytes_per_packet_per_bin;
    unsigned int bins;
};

/**
 * period - struct with period information and aggregated data.
 */
struct period
{
    period_data data;   //aggregated data for this period
    flow_time start;    //period time start
    flow_time end;      //period time end
};

/**
 * netflow_statistics_maker - class for creating netflow statistics from input files.
 * Statistics are saved into file and can be used by netflow detector.
 */
class netflow_statistics_maker
{
  public:
    netflow_statistics_maker(params *par);  //constructor
    ~netflow_statistics_maker();            //destructor

    void create_netflow_statistics();       //create statistics from input records in NetFlow format
    void collect_statistics(flow *flw);     //save statistical data
    void save_statistics();                 //export computed statistics into file

    //aggregate flow data into correct bin (sorts flows into bins)
    void aggregate_into_correct_bin(flow_time flwtime, unsigned int flows, unsigned int packets, unsigned long bytes);
    period *get_period(flow_time bin_start_time);  //get period to which to store bin with specified start time
    void create_new_bin(flow_time flwtime); //create new bin with specified start time
    void create_all_periods();              //create all statistics periods and save them into vector

    void aggregate_remaining_bins();        //aggregate remaining bins in vector

  private:
    params *parameters; //program arguments

    unsigned int last_bin_index;    //index of last used bin
    unsigned int last_period_index; //index of last used period
    vector <period> periods; //all recorded periods
    vector <bin_data> bins;  //all bins
    period *actual_period;   //pointer to active period
};

#endif

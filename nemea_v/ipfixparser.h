/**
 * ipfixparser.h
 * This file defines class ipfix_parser.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    18.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
#include "ip_storage.h"


using namespace std;

#ifndef _IPFIXPARSER_H_
#define _IPFIXPARSER_H_

/////////////////////////////////////////////// CONSTANTS



/////////////////////////////////////////////// CLASSES

/**
 * ipfix_bin_maker - class for organising IPFIX data into bins.
 */
class ipfix_bin_maker
{
  public:
    ipfix_bin_maker(params *pars);   //constructor
    ~ipfix_bin_maker();   //destructor

    void parse_input();    //process input files
    bool parse_ipfix_flow(const void *in_rec, ipfix_flow *flwdest, bool parse_only_statistics);    //parse readed flow data into flow struct

    static void print_flow(ipfix_flow *flw);       //print flow info (debug function)
    static void save_to_file(ofstream *file, ipfix_flow *flw);   //save flow info into file

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments

    unsigned long total_bins;               //total bins created
    unsigned long long total_flws_processed;//total packets processed
    unsigned long long total_flws_filtered; //total DNS packets
};

#endif

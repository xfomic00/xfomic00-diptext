/**
 * netflowparser.h
 * This file defines class netflow_parser.
 *
 * Nfdump binary files are parsed with "nfreader" library, which can be obtained here:
 * http://sourceforge.net/projects/hoststats/files/nfreader/
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    25.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "common.h"
#include "ip_storage.h"
#include "netflowstatistics.h"

using namespace std;

#ifndef _NETFLOWPARSER_H_
#define _NETFLOWPARSER_H_

/////////////////////////////////////////////// OTHER

//parse readed flow data into flow struct
bool parse_netflow_flow(const void *in_rec, flow *flwdest, ur_template_t *templ, unsigned long long flownum, bool parse_only_statistics);

/////////////////////////////////////////////// CLASSES

/**
 * netflow_bin_maker - class for organising netflow data into bins.
 */
class netflow_bin_maker
{
  public:
    netflow_bin_maker(params *pars);   //constructor
    ~netflow_bin_maker();   //destructor

    void parse_input();    //process input file

    static void print_flow(flow *flw);       //print flow info (debug function)
    static void save_to_file(ofstream *file, flow *flw);   //save flow info into file

    void save_summary();    //save summary msg

  private:
    params *parameters; //program arguments

    unsigned long total_bins;               //total bins created
    unsigned long long total_flws_processed;//total packets processed
    unsigned long long total_flws_filtered; //total DNS packets
};

#endif

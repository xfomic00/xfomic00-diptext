#####
# THIS IS WHITELIST FILE FOR DETECTOR
#
# You can generate default whitelist file with "./detector -m <file>".
#
# Comments can be made by putting # as first character on line...
# Also blank lines are ignored.
#
# Whitelist contains domain names which will not be detected as tunnel.
# Every domain should match number of extracted subdomains specified in config file 
# (dns_extract_domain_level parameter, default = 3).
# Each line in this file should contain only one domain.
#####

############# WHITELIST START
url.zvelo.com
url.esoft.com
bl.spamcop.net
sa-trusted.bondedsender.org
query.bondedsender.org
sa-accredit.habeas.com
s.sophosxl.net
spamassassin.taint.org
zen.spamhaus.org
swl.spamhaus.org
bb.barracudacentral.org
combined.njabl.org
dnsbl.manitu.net
iadb.isipp.com
hul.habeas.com
emea.hpqcorp.net
asn.cymru.com
j.e5.sk


/**
 * netflowparser.cpp
 * This file implements methods of class netflow_parser.
 *
 * Nfdump binary files are parsed with "libnfdump" library, which can be obtained here:
 * http://sourceforge.net/projects/libnfdump/files/?source=navbar
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    25.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
#include <libtrap/trap.h>
#ifdef __cplusplus
}
#endif
#include <unirec/unirec.h>

#include "netflowparser.h"
#include "common.h"
#include "ip_storage.h"
#include "netflowstatistics.h"
#include "netflowstatisticsmaker.h"
#include "shared.h"
#include "binsorter.h"

using namespace std;

/**
 * Parse flow fields into "flwdest".
 * If parse_only_statistics switch is true, then only statistically important data will be parsed.
 * Returns true if it is DNS flow, false otherwise.
 */
bool parse_netflow_flow(const void *in_rec, flow *flwdest, ur_template_t *templ, unsigned long long flownum, bool parse_only_statistics)
{
    flwdest->flowNum = flownum;

    //time (flow end time)
    ur_time_t time = ur_get(templ, in_rec, UR_TIME_LAST);
    time_t endt = ur_time_get_sec(time);
    struct tm *loctime = localtime(&endt);
    flwdest->flowTime.set_date((u_int16_t)(loctime->tm_year + 1900), (u_int8_t)(loctime->tm_mon + 1), (u_int8_t)loctime->tm_mday);
    flwdest->flowTime.set_time((u_int8_t)loctime->tm_hour, (u_int8_t)loctime->tm_min, (u_int8_t)loctime->tm_sec);

    //IP header
    if (! parse_only_statistics)
    {
        ip_addr_t srcIP = ur_get(templ, in_rec, UR_SRC_IP);
        ip_addr_t dstIP = ur_get(templ, in_rec, UR_DST_IP);

        if (ip_is4(&srcIP))
        {
            memcpy(&(flwdest->srcIP.v4),ip_get_v4_as_bytes(&srcIP),sizeof(in_addr));
            flwdest->ipv6 = false;
        }
        else
        {
            memcpy(&(flwdest->srcIP.v6),&srcIP,sizeof(in6_addr));
            flwdest->ipv6 = true;
        }

        if (ip_is4(&dstIP))
            memcpy(&(flwdest->dstIP.v4),ip_get_v4_as_bytes(&dstIP),sizeof(in_addr));
        else
            memcpy(&(flwdest->dstIP.v6),&dstIP,sizeof(in6_addr));

        flwdest->ipProtocol = (u_int8_t) ur_get(templ, in_rec, UR_PROTOCOL);
    }

    //UDP header
    flwdest->srcPort = (u_int16_t) ur_get(templ, in_rec, UR_SRC_PORT);
    flwdest->dstPort = (u_int16_t) ur_get(templ, in_rec, UR_DST_PORT);

    if (flwdest->srcPort != DNS_PORT && flwdest->dstPort != DNS_PORT)   //not a DNS packet
        return false;

    //other
    flwdest->packets = (u_int16_t) ur_get(templ, in_rec, UR_PACKETS);
    flwdest->bytes = (u_int32_t) ur_get(templ, in_rec, UR_BYTES);

    return true;
}


////////////////////////////////////////////////////////////// NETFLOW_BIN_MAKER METHODS

/**
 * Constructor
 */
netflow_bin_maker::netflow_bin_maker(params *pars)
{
    parameters = pars;
    total_bins = 0;
    total_flws_processed = 0;
    total_flws_filtered = 0;
}

/**
 * Destructor
 */
netflow_bin_maker::~netflow_bin_maker()
{
}

/**
 * Parse input file.
 */
void netflow_bin_maker::parse_input()
{
    bin_sorter sorter(parameters);  //for sorting flows by end time

    flow flw;        //flow record
    const void *in_rec;     //input record
    uint16_t in_rec_size;   //input record size
    int ret;                //return code

    // Read data from input, process them and write to output
    while (!stop) {
       // Receive data from input interface 0.
       // Block if data are not available immediately (unless a timeout is set  using trap_ifcctl)
       ret = trap_recv(0, &in_rec, &in_rec_size);
       // Handle possible errors
       TRAP_DEFAULT_RECV_ERROR_HANDLING(ret, continue, break);

       // Check size of received data
       if (in_rec_size < ur_rec_static_size(parameters->ur_template)) {
          if (in_rec_size <= 1) {
             break; // End of data (used for testing purposes)
          } else {
             fprintf(stderr, "Error: data with wrong size received (expected size: >= %hu, received size: %hu)\n",
                     ur_rec_static_size(parameters->ur_template), in_rec_size);
             break;
          }
       }

       // PROCESS THE DATA
       ++total_flws_processed;
       if (parse_netflow_flow(in_rec, &flw, parameters->ur_template, total_flws_processed, false))
       {
           //put this flow into correct bin by flow end time
           sorter.sort_into_bin(&flw);
           ++total_flws_filtered;

           //print packet info if needed
           if (parameters->debug_level > DEBUG_LEVEL_STANDARD)
           {
               mutex_cout.lock();
               print_flow(&flw);
               mutex_cout.unlock();
           }
       }

    } // Main loop

    sorter.push_all_to_shared();            //put all waiting bins into shared queue
    shared_bin_pointers.set_finished(true); //all loading is finished

    total_bins = sorter.get_total_bins();
}

/**
 * Print flow information (debug function).
 */
void netflow_bin_maker::print_flow(flow *flw)
{
    if (flw)
    {
        cout << flw->flowNum << ": " << flw->flowTime.get_time_str() << ", bytes=" << flw->bytes << ", " << "pkts=" << flw->packets << ", " << get_srcIP_str(flw);
        cout << " -> " << get_dstIP_str(flw) << ", sp: " << flw->srcPort << ", dp: " << flw->dstPort << endl;
    }
}

/**
 * Write flow information into file.
 */
void netflow_bin_maker::save_to_file(ofstream *file, flow *flw)
{
    if (file && flw)
    {
        (*file) << flw->flowTime.get_time_str() << " " << get_srcIP_str(flw) << " -> ";
        (*file) << get_dstIP_str(flw) << ", sp=" << flw->srcPort << ", dp=";
        (*file) << flw->dstPort << ", pkts=" << flw->packets << ", bytes=" << flw->bytes << endl;
    }
}

/**
 * Save summary msg to shared obj.
 */
void netflow_bin_maker::save_summary()
{
    ostringstream str;
    str << "Total flows processed: " << total_flws_processed << endl;
    str << "Total flows filtered:  " << total_flws_filtered << endl;
    str << "Total bins created:    " << total_bins << endl;

    shared_summary.set_parser_msg(str.str());
}


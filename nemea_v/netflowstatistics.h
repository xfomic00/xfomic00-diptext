/**
 * netflowstatistics.h
 * This file defines class netflow_statistics.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    4.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdlib.h>
#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <unordered_map>

#include "common.h"

using namespace std;

#ifndef _NETFLOWSTATISTICS_H_
#define _NETFLOWSTATISTICS_H_

/////////////////////////////////////////////// CONSTANTS


/////////////////////////////////////////////// CLASSES & STRUCTS

/**
 * nfstat - struct with netflow statistic information.
 */
struct nfstat
{
    //CSV format for stats file:
    //period start, period end, avg bpb, avg fpb, avg bpppb, max bpb, max fpb

    flow_time periodstart;
    flow_time periodend;
    double avg_bpb;
    double avg_fpb;
    double avg_bpppb;
    double max_bpb;
    double max_fpb;
};

/**
 * netflow_statistics - class for parsing netflow statistics file and easy work with parsed information.
 */
class netflow_statistics
{
  public:
    netflow_statistics();  //constructor

    void set_params(params *p) { parameters = p; }  //set pointer to parameters

    bool parse_statistics_file();   //extract information from statistics file
    void sort_statistic_data();     //sort statistics in vector by period start time
    void calc_avg_period_stats();   //calculate average period statistics
    void fill_missing_stats();      //add missing periods data

    nfstat *get_stat_data_for_period(flow_time flwtime);    //get data by flow time

  private:
    params *parameters;         //program parameters
    vector<nfstat> stats_vect;  //statistic data
    nfstat avg_period_stat;     //default average stats by all periods

    unsigned int last_index;    //last used index in stats_vect (for faster period search)
};

#endif

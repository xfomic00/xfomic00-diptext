/**
 * detectionsettings.h
 * This file contains default detection settings.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    22.2.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */


#ifndef _DETECTIONSETTINGS_H_
#define _DETECTIONSETTINGS_H_

//macros for converting defines to text
#define XSTR(x) #x
#define STR(x) XSTR(x)


/////////////////////////////////////////////////////////////// DEFAULT SETTINGS FOR DETECTOR 1 STATISTICS BASED DETECTION

#define DEF_STATISTICS_THRESH_AVG_BPB 9000000
#define DEF_STATISTICS_THRESH_AVG_FPB 4000
#define DEF_STATISTICS_THRESH_AVG_BPPPB 5

/////////////////////////////////////////////////////////////// DEFAULT SETTINGS FOR DETECTOR 1 THRESHOLD BASED DETECTION

#define DEF_THRESHOLD_THRESH_BPPPB 250

/////////////////////////////////////////////////////////////// DEFAULT SETTINGS FOR DETECTION SCHEMAS

#define D1_DETECTION_SCHEMA_STATISTICS 1
#define D1_DETECTION_SCHEMA_BIN_THRESHOLDS 2

#define DEF_D1_DETECTION_SCHEMA D1_DETECTION_SCHEMA_STATISTICS

#define D2_DETECTION_SCHEMA_AGGREGATE_ALL 1
#define D2_DETECTION_SCHEMA_AGGREGATE_SINGLE 2
#define D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES 3
#define D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES_THRESHOLDS 4

#define DEF_D2_DETECTION_SCHEMA D2_DETECTION_SCHEMA_AGGREGATE_SUSPICIOUS_QTYPES_THRESHOLDS

/////////////////////////////////////////////////////////////// DEFAULT SETTINGS FOR DETECTOR 2 SINGLE FLOW/PACKET

#define DEF_BYTES_PER_FLOW_THRESH 800
#define DEF_PKTS_PER_FLOW_THRESH 20
#define DEF_PACKET_TTL_THRESH 5
#define DEF_PACKET_RDATA_SIZE_THRESH 200
#define DEF_QUERY_PACKET_SIZE_THRESH 150
#define DEF_PACKET_SIZE_THRESH 300
#define DEF_SINGLE_FLOW_DURATION 20

/////////////////////////////////////////////////////////////// DEFAULT SETTINGS FOR DETECTOR 2 AGGREGATION MAP

#define DEF_OCCURENCES_IN_MAP_THRESH 110
#define DEF_AVG_SIZE_MAP_THRESH 170
#define DEF_TOTAL_BYTES_MAP_THRESH 1048576
#define DEF_TOTAL_QUERIES_MAP_THRESH 200
#define DEF_TOTAL_BYTES_QUERIES_ONLY_MAP_THRESH 262144
#define DEF_AVG_NAMELEN_MAP_THRESH 50
#define DEF_MOST_USED_DOMAIN_COUNT_THRESH 110
#define DEF_MOST_USED_QTYPE_COUNT_THRESH 110

#define DEF_MAP_MINIMUM_PACKETS 50 //used together with AVG_SIZE_MAP_THRESH and AVG_NAMELEN_MAP_THRESH

#define DEF_MAP_MINIMUM_CONDITIONS 4 //number of conditions matched to be detected
#define DEF_MAP_MINIMUM_MOST_USED_DOMAIN_COUNT 100

#define DEF_DETECT_ONLY_CLIENT_COMMUNICATION 1

/////////////////////////////////////////////////////////////// DEFAULT SETTINGS DETECTION PERIODS

//default bin length
#define DEF_BIN_SIZE_HRS 0
#define DEF_BIN_SIZE_MINS 0
#define DEF_BIN_SIZE_SECS 20

//statistics period
#define DEF_STATISTICS_PERIOD_HRS 0
#define DEF_STATISTICS_PERIOD_MINS 30
#define DEF_STATISTICS_PERIOD_SECS 0

#define DEF_BIN_SIZE_PACKETS 0  //in default, we dont use packet size bin

//help macros - time must be saved in hh:mm:ss format
#define DEF_STATISTICS_PERIOD_TEXT "00:30:00"
#define DEF_BIN_SIZE_TEXT "00:00:20"

/////////////////////////////////////////////////////////////// DEFAULT MISC SETTINGS

#define DEF_NUM_OF_SORTING_BINS 5

#define DEF_FILTER_COMMON_QTYPES true
#define DEF_FILTER_COMMON_QTYPES_NUM 1  //0=false, 1=true

#define DEF_MAX_AGGREGATED_BINS 15

#define DEF_DNS_EXTRACT_DOMAIN_LEVEL 3

/////////////////////////////////////////////////////////////// DEFAULT CONFIG FILE

const char def_config_file[] =
"#####\n"
"# THIS IS CONFIG FILE TEMPLATE FOR DETECTOR\n"
"#\n"
"# You can generate default configuration file with \"./detector -m <file>\".\n"
"#\n"
"# Comments can be made by putting # as first character on line...\n"
"# Also blank lines are ignored.\n"
"#\n"
"# All params have format: param_name = value\n"
"#####\n"
"\n"
"## Here comes params section:\n"
"# Uncomment/comment each param as you like.\n"
"\n"
"##################### DETECTION PERIODS\n"
"# Netflow statistics time range (one statistical period) in hh:mm:ss format.\n"
"# For your own good, please use period which can fill exactly 24 hrs (hour of half hour periods are preferred).\n"
"statistics_period = " DEF_STATISTICS_PERIOD_TEXT "\n"
"\n"
"# Bin time length in hh:mm:ss format. Cant be used together with bin_packets!\n"
"bin_time = " DEF_BIN_SIZE_TEXT "\n"
"\n"
"# Maximum packets/flows per bin. Cant be used together with bin_time and cannot be used with statistics file!\n"
"# Also cannot be used with NetFlow (-d, -n) or IPFIX (-x) input when statistics-based detection schema is \n"
"# selected (because flows wouldn't be sorted by time)...\n"
"#bin_packets = " STR(DEF_BIN_SIZE_PACKETS) "\n"
"\n"        
"##################### MISC SETTINGS\n"
"# Number of bins, which are kept in waiting queue for \"sorting\" flows by end time.\n"
"# Choose this parameter carefully, because it has direct impact on performance\n"
"# (we need to find appropriate bin in this queue for each flow)...\n"
"num_of_sorting_bins = " STR(DEF_NUM_OF_SORTING_BINS) "\n"
"\n"
"# Do(0)/do not(1) inspect most common DNS query types (PTR, A, AAAA) - only IPFIX/pcap.\n"
"# Filtering common queries should make detection faster, but detector cannot detect A, AAAA based tunnels...\n"
"filter_common_qtypes = " STR(DEF_FILTER_COMMON_QTYPES_NUM) "\n"
"\n"
"# How many bins to maximally aggregate together before inspection of aggregated data.\n"
"max_aggregated_bins = " STR(DEF_MAX_AGGREGATED_BINS) "\n"
"\n"
"# Subdomain level of DNS domains which are stored in aggregation map (whitelist domains should match this!).\n"
"dns_extract_domain_level = " STR(DEF_DNS_EXTRACT_DOMAIN_LEVEL) "\n"
"\n"
"##################### DETECTOR 1 STATISTICS THRESHOLDS\n"
"# Average bytes per bin threshold for statistics based detection\n"
"stat_bpb_thresh = " STR(DEF_STATISTICS_THRESH_AVG_BPB) "\n"
"\n"
"# Average flows per bin threshold for statistics based detection\n"
"stat_fpb_thresh = " STR(DEF_STATISTICS_THRESH_AVG_FPB) "\n"
"\n"
"# Average bytes per packet per bin threshold for statistics based detection\n"
"stat_bpppb_thresh = " STR(DEF_STATISTICS_THRESH_AVG_BPPPB) "\n"
"\n"     
"##################### DETECTOR 1 BIN THRESHOLDS\n"
"# Bytes per packet per bin threshold.\n"
"thresh_bpppb_thresh = " STR(DEF_THRESHOLD_THRESH_BPPPB) "\n"
"\n"
"##################### DETECTOR 1 DETECTION SCHEMA\n"
"## Only one option can be selected (uncommented)!\n"
"# Use statistical data for detection.\n"
"d1_detection_schema = statistics\n"
"\n"
"# Use bin thresholds (not statistics based values).\n"
"#d1_detection_schema = bin_thresholds\n"
"\n"
"##################### DETECTOR 2 DETECTION SCHEMA\n"
"## Only one option can be selected (uncommented)!\n"
"## This detection schemas are also used in pcap detector 1\n"
"## Only aggregate_all and aggregate_simgle schemas are supported for NetFlow input\n"
"\n"
"# Aggregate all data from detected bins from detector 1\n"
"#d2_detection_schema = aggregate_all\n"
"\n"
"# Aggregate only sigle flows/packets based on SINGLE FLOW/PACKET THRESHOLDS\n"
"d2_detection_schema = aggregate_single\n"
"\n"
"# Aggregate only suspicious query types usually used for tunneling (MX, NS, TXT, NULL, CNAME).\n"
"#d2_detection_schema = aggregate_suspicious_qtypes\n"
"\n"
"# Aggregate suspicious query types based on SINGLE FLOW/PACKET THRESHOLDS (aggregate_suspicious_qtypes + aggregate_single)\n"
"#d2_detection_schema = aggregate_suspicious_qtypes_thresholds\n"
"\n"
"##################### DETECTOR 2 SINGLE FLOW/PACKET THRESHOLDS\n"
"# Packet size threshold [B].\n"
"packet_size_thresh = " STR(DEF_PACKET_SIZE_THRESH) "\n"
"\n"
"# Query only packet size threshold [B]. (pcap/IPFIX only)\n"
"query_packet_size_thresh = " STR(DEF_QUERY_PACKET_SIZE_THRESH) "\n"
"\n"
"# DNS RDATA size threshold [B] (pcap/IPFIX only)\n"
"packet_rdata_size_thresh = " STR(DEF_PACKET_RDATA_SIZE_THRESH) "\n"
"\n"
"# DNS response TTL threshold - less than [s] (pcap/IPFIX only)\n"
"packet_ttl_thresh = " STR(DEF_PACKET_TTL_THRESH) "\n"
"\n"
"# Bytes per flow threshold (netflow/IPFIX only).\n"
"bpf_thresh = " STR(DEF_BYTES_PER_FLOW_THRESH) "\n"
"\n"
"# Packets per flow threshold (netflow/IPFIX only).\n"
"ppf_thresh = " STR(DEF_PKTS_PER_FLOW_THRESH) "\n"
"\n"
"# Single flow duration [s] (NetFlow only).\n"
"single_flow_duration = " STR(DEF_SINGLE_FLOW_DURATION) "\n"
"\n"
"##################### DETECTOR 2 AGGREGATED MAP THRESHOLDS\n"
"## For NetFlow input, only these thresholds matter: occurences_in_map_thresh, avg_size_map_thresh, total_bytes_map_thresh\n"
"\n"
"# Detect only communication from tunnel client to tunnel server (IP:XXXXX -> IP:53) - useful for NetFlow-based detection (1=true, 0=false)\n"
"detect_only_communication_from_client = " STR(DEF_DETECT_ONLY_CLIENT_COMMUNICATION) "\n"
"\n"
"# Number of map conditions (following after this parameter) which must be matched to mark data as detected\n"
"map_minimum_conditions = " STR(DEF_MAP_MINIMUM_CONDITIONS) "\n"
"\n"
"# Minimum number of domain matches for most used domain (must be matched to mark data as detected)\n"
"map_minimum_most_used_domain_count = " STR(DEF_MAP_MINIMUM_MOST_USED_DOMAIN_COUNT) "\n"
"\n"
"# Number of occurences in map threshold (for single IP)\n"
"occurences_in_map_thresh = " STR(DEF_OCCURENCES_IN_MAP_THRESH) "\n"
"\n" 
"# Minimum number of packets to detect based on {avg_size_map_thresh, avg_namelen_map_thresh}\n"
"map_minimum_packets = " STR(DEF_MAP_MINIMUM_PACKETS) "\n"
"\n"
"# Avg size of packet threshold [B]\n"
"avg_size_map_thresh = " STR(DEF_AVG_SIZE_MAP_THRESH) "\n"
"\n"
"# Total aggregated bytes [B] = 1 MB\n"
"total_bytes_map_thresh = " STR(DEF_TOTAL_BYTES_MAP_THRESH) "\n"
"\n"
"# Total aggregated queries\n"
"total_queries_map_thresh = " STR(DEF_TOTAL_QUERIES_MAP_THRESH) "\n"
"\n"
"# Total bytes only on queries [B] = 256 kB\n"
"total_bytes_queries_only_map_thresh = " STR(DEF_TOTAL_BYTES_QUERIES_ONLY_MAP_THRESH) "\n"
"\n"
"# Avg len of DNS query domain name (including subdomains)\n"
"avg_namelen_map_thresh = " STR(DEF_AVG_NAMELEN_MAP_THRESH) "\n"
"\n"
"# Occurences of most used domain in map (for one map key) threshold\n"
"most_used_domain_count_thresh = " STR(DEF_MOST_USED_DOMAIN_COUNT_THRESH) "\n"
"\n"
"# Occurences of most used DNS query type in map (for one map key) threshold\n"
"most_used_qtype_count_thresh = " STR(DEF_MOST_USED_QTYPE_COUNT_THRESH) "\n"
"\n";

/////////////////////////////////////////////////////////////// DEFAULT WHITELIST FILE

const char def_whitelist_file[] =
"#####\n"
"# THIS IS WHITELIST FILE FOR DETECTOR\n"
"#\n"
"# You can generate default whitelist file with \"./detector -m <file>\".\n"
"#\n"
"# Comments can be made by putting # as first character on line...\n"
"# Also blank lines are ignored.\n"
"#\n"
"# Whitelist contains domain names which will not be detected as tunnel.\n"
"# Every domain should match number of extracted subdomains specified in config file \n"
"# (dns_extract_domain_level parameter, default = 3).\n"
"# Each line in this file should contain only one domain.\n"
"#####\n"
"\n"
"############# WHITELIST START\n"
"url.zvelo.com\n"
"url.esoft.com\n"
"bl.spamcop.net\n"
"sa-trusted.bondedsender.org\n"
"query.bondedsender.org\n"
"sa-accredit.habeas.com\n"
"s.sophosxl.net\n"
"spamassassin.taint.org\n"
"zen.spamhaus.org\n"
"swl.spamhaus.org\n"
"bb.barracudacentral.org\n"
"combined.njabl.org\n"
"dnsbl.manitu.net\n"
"iadb.isipp.com\n"
"hul.habeas.com\n"
"emea.hpqcorp.net\n"
"asn.cymru.com\n"
"j.e5.sk\n"
"\n";

#endif

/**
 * shared.cpp
 * This file implements methods of classes for sharing data between threads.
 *
 * Author:  Jiri Fomiczew <xfomic00@stud.fit.vutbr.cz>
 * Date:    11.3.2015
 * Project: Effective detection of network anomaly using DNS data (Master thesis).
 */

#include <stdio.h>
#include <iostream>
#include <string>
#include <stdlib.h>


#include "shared.h"

using namespace std;

////////////////////////////////////////////////////////////// SHARED_QUEUE_IP_STORAGE METHODS

/**
 * Constructor
 */
shared_queue_ip_storage::shared_queue_ip_storage()
{
    finished = false;
}

/**
 * Destructor
 */
shared_queue_ip_storage::~shared_queue_ip_storage()
{
    //delete all remaining pointers (should not happen, but...)
    while (! pointers.empty())
    {
        delete pointers.front();
        pointers.pop();
    }
}

/**
 * Set finished variable (producer thread will set finished to true, when
 * it will stop producing).
 */
void shared_queue_ip_storage::set_finished(bool fin)
{
    lock_guard<mutex> lck(_m);
    finished = fin;
    _cv.notify_all();
}

/**
 * Add pointer to queue.
 */
void shared_queue_ip_storage::add_data(ip_storage *data)
{
    if (data)
    {
        lock_guard<mutex> lck(_m);  //lock mutex
        pointers.push(data);        //CS
        _cv.notify_all();           //notify waiting threads that data is available
    }
}

/**
 * Get data.
 * If data is not available, wait for notifying by producer thread.
 * Returns NULL if queue is empty and producing is finished (consumer thread can stop too).
 */
ip_storage *shared_queue_ip_storage::get_data()
{
    ip_storage *ret = NULL;

    unique_lock<mutex> lck(_m); //lock mutex for condition variable

    if (! finished)
        _cv.wait(lck);          //wait for notify from producer thread (only if producing is not finished)

    if (pointers.size() > 0)    //this is CS
    {
        ret = pointers.front();
        pointers.pop();
    }

    return ret;
}

////////////////////////////////////////////////////////////// SHARED_DETECTORS_SUMMARY METHODS

/**
 * Write summary msgs to file.
 */
void shared_detectors_summary::write_msgs_to_file(ofstream *file)
{
    if (file)
    {
        (*file) << "######################### SUMMARY #############################" << endl;
        (*file) << "########## Parser summary: ##########" << endl;
        (*file) << parser_msg << endl;
        (*file) << "########## Detector 1 summary: ##########" << endl;
        (*file) << detector1_msg << endl;

        if (shared_parameters.filetype != 'p' && shared_parameters.filetype != 'e') //pcap doesnt have detector 2
        {
            (*file) << "########## Detector 2 summary: ##########" << endl;
            (*file) << detector2_msg << endl;
        }
    }
}


